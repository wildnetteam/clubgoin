<?php
session_start();
//include 'config.php';
require 'vendor/autoload.php';

# Replace these values. You probably want to start with your Sandbox credentials
# to start: https://docs.connect.squareup.com/articles/using-sandbox/

# The ID of the business location to associate processed payments with.
# If you're testing things out, use a sandbox location ID.
#
# See [Retrieve your business's locations](https://docs.connect.squareup.com/articles/getting-started/#retrievemerchantprofile)CBASEFUjI8G5RfloAxCIObwWhksgAQ
# for an easy way to get your business's location IDs.
$location_id = 'CBASEFUjI8G5RfloAxCIObwWhksgAQ';

# The access token to use in all Connect API requests. Use your *sandbox* access
# token if you're just testing things out.
$access_token = 'sandbox-sq0atb-fRh7r0K9gKOL5SK7yZblNQ';

# Helps ensure this code has been reached via form submission
if ($_SERVER['REQUEST_METHOD'] != 'POST') {
  error_log("Received a non-POST request");
 // echo "Request not allowed";
$data = array("status"=>0,"msg" => "Request not allowed");
echo json_encode($data);
  http_response_code(405);
  return;
}

# Fail if the card form didn't send a value for `nonce` to the server
$nonce = $_POST['nonce'];
if (is_null($nonce)) {
  //echo "Invalid card data";
$data = array("status"=>0,"msg" => "Invalid card data");
echo json_encode($data);
  http_response_code(422);
  return ;
}

# Fail if the Amount 

if (!isset($_SESSION['amt'])) {
  //echo "Invalid card data";
$data = array("status"=>0,"msg" => "Amount Not Set");
echo json_encode($data);
  http_response_code(422);
  return ;
}

$transaction_api = new \SquareConnect\Api\TransactionApi();
$imp_key = uniqid();
$amt = $_SESSION['amt'];
$d = date('Y-m-d H:i:s');
$request_body = array (

  "card_nonce" => $nonce,

  # Monetary amounts are specified in the smallest unit of the applicable currency.
  # This amount is in cents. It's also hard-coded for $1.00, which isn't very useful.
  "amount_money" => array (
    "amount" => (int)$amt,
    "currency" => "USD"
  ),

  # Every payment you process with the SDK must have a unique idempotency key.
  # If you're unsure whether a particular payment succeeded, you can reattempt
  # it with the same idempotency key without worrying about double charging
  # the buyer.
  "idempotency_key" => $imp_key
);

# The SDK throws an exception if a Connect endpoint responds with anything besides
# a 200-level HTTP code. This block catches any exceptions that occur from the request.
try {
  $result = $transaction_api->charge($access_token, $location_id, $request_body);
 //echo "<pre>";
//print_r($result);
 //echo "</pre>";
$transaction = $result->getTransaction();
$tenders = $transaction->getTenders();
$location_id = $transaction->getLocationId();
$transaction_id = $transaction->getId();
foreach ($tenders as $tender){
	//print_r($tender);
	$cardDetails = $tender->getCardDetails();		
	$status = $cardDetails->getStatus();
$card = $cardDetails->getCard();
$card_brand = $card->getCardBrand();

}

 
//$insert = mysqli_query($conn,"INSERT INTO `payment`(`transacation_id`, `location_id`, `idempotency_key`, `status`, `amount`,`card_brand`, `time`) VALUES ('$transaction_id','$location_id','$imp_key','$status','$amt','$card_brand','$d')");
		//$data = array("status"=>1,"msg" => "success");
//echo json_encode($data);
header('location:result.php?res=success');
} catch (\SquareConnect\ApiException $e) {

    $error =  $e->getmessage().'<br>';

   /* $insert = mysqli_query($conn,"INSERT INTO `error_payment`(`idempotency_key`, `amount`,`error`,`time`) VALUES ('$imp_key','$amt','$error','$d')");
if($insert)
{

  $data = array("status"=>0);
}
else
{

  $data = array("status"=>0);
}
*/

 //$data = array("status"=>0,"msg" => "failed");
header('location:result.php?res=fail');
//echo json_encode($data);
  //echo "Caught exception!<br/>";
 // print_r("<strong>Response body:</strong><br/>");
  //echo "<pre>"; var_dump($e->getResponseBody()); echo "</pre>";
  //echo "<br/><strong>Response headers:</strong><br/>";
 // echo "<pre>"; var_dump($e->getResponseHeaders()); echo "</pre>";
}



