<?php
include("config.php");
error_reporting(0);
ob_start();
session_start();

@$user=$_SESSION["name"];
@$userid=$_SESSION["userid"];
@$fbid=$_SESSION["fbid"];


?>

<!DOCTYPE html>
<html lang="en-gb" dir="ltr">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="keywords" content="" />
  <meta name="description" content="" />
  <?php
@$id=$_GET['id'];
            //@$date=$_GET['date'];

        @$vdetail=mysqli_query($conn,"SELECT i.item_title,i.item_lat,i.item_lng, i.item_img1,i.item_img2,i.item_img3,i.item_img4,i.venue_image,i.menu_image,i.bar_image,i.happy_hours,i.known_for,i.busy_nights,i.cuisines,i.highlights,i.cost,loc.loc_title,i.item_address from items i,location loc where i.item_location=loc.loc_id and i.item_id='$id'");
                        @$vfetch=mysqli_fetch_array($vdetail);


  ?>
        <title><?php echo $vfetch['item_title'];?></title>
        <link rel="shortcut icon" href="docs/images/clubgo-icon.png" type="image/x-icon">
        <link rel="apple-touch-icon-precomposed" href="docs/images/apple-touch-icon.png">
        <link id="data-uikit-theme" rel="stylesheet" href="docs/css/uikit.docs.min.css">
        <link rel="stylesheet" href="docs/css/docs.css">
        <link rel="stylesheet" href="docs/css/custom.css">
        <link rel="stylesheet" href="docs/css/makeweb.css">
        <link rel="stylesheet" href="docs/css/theme1.css">
        <link rel="stylesheet" href="docs/css/responsive.css">
        
        <link rel="stylesheet" href="docs/css/sitestyle_15-04.css">
                
        <link rel="stylesheet" href="vendor/highlight/highlight.css">
        <script src="vendor/jquery.js"></script>
        <script src="docs/js/uikit.min.js"></script>
        <script src="vendor/highlight/highlight.js"></script>
        <script src="docs/js/docs.js"></script>
        <script src="docs/js/slideshow.js"></script>
        <script src="docs/js/slideshow-fx.js"></script>
        <script src="docs/js/slideset.js"></script> 
        <script src="docs/js/lightbox.js"></script>
        <script src="docs/js/sticky.js"></script> 
        
        <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>  
        <script>
            $(document).ready(function(){
            
            
                //----------Select the first tab and div by default
            
                $('#vertical_tab_nav > ul > li > a').eq(0).addClass( "selected" );
                $('#vertical_tab_nav > div > article').eq(0).css('display','block');
            
            
                //---------- This assigns an onclick event to each tab link("a" tag) and passes a parameter to the showHideTab() function
            
                    $('#vertical_tab_nav > ul').click(function(e){
            
                  if($(e.target).is("a")){
            
                    /*Handle Tab Nav*/
                    $('#vertical_tab_nav > ul > li > a').removeClass( "selected");
                    $(e.target).addClass( "selected");
            
                    /*Handles Tab Content*/
                    var clicked_index = $("a",this).index(e.target);
                    $('#vertical_tab_nav > div > article').css('display','none');
                    $('#vertical_tab_nav > div > article').eq(clicked_index).fadeIn();
            
                  }
            
                    $(this).blur();
                    return false;
            
                    });
            
            
            });//end ready
        </script>
        
        <style>
        .uk-subnav-pill>.uk-active>*{background:#fff;box-shadow:none;color:#009dd8;border-bottom:4px solid #009dd8;border-radius:0px;}
        
        
        </style>   
        </head>

    <body class="tm-background">

        <nav class="tm-navbar uk-navbar uk-navbar-attached" data-uk-sticky="{boundary:'#define-an-offset'}">
            <div class="uk-container uk-container-center">

                <div class="uk-animation-hover"><a class="uk-navbar-brand uk-hidden-small uk-animation-reverse uk-animation-scale" href="index.php"><img class="uk-margin uk-margin-remove" src="docs/images/clugo.png" width="120" height="40" title="Clubgo" alt="Clubgo"></a></div>

                <ul class="uk-navbar-nav uk-navbar-flip uk-hidden-small">
                  <li><a href="about.php">About</a></li>
                    <li><a href="events.php">Events</a></li>
                    <li><a href="venues.php" class="acti">Venues</a></li>
<!--                     <li><a href="offer.php">Offers</a></li> -->
                    <li><a href="artist.php">Artists</a></li>
                    <li><a href="login.php" id="ulog">Login/Signup</a></li>
                    <li class="download"><a href="download.php">Download App</a></li>
                    <?php
if($fbid){
?>
<li><div class="uk-button-dropdown" data-uk-dropdown="{mode:'click'}" aria-haspopup="true" aria-expanded="false"><img src="https://graph.facebook.com/<?php echo  $fbid ?>/picture?type=small" width="40" height="40" class="fb-img"><div class="uk-dropdown uk-dropdown-bottom" aria-hidden="true" style="top: 30px; left: 0px;" tabindex="" >
                                        <ul class="uk-nav uk-nav-dropdown">
                                           <li><a href="logout.php">Logout</a></li> 
                                           <li><a href="history.php">Booked Tickets</a></li>
                                        </ul>
                                    </div>
                                </div></li>

<?php
}
if($userid and !$fbid){
?>
<li class="mail-img"><div class="uk-button-dropdown" data-uk-dropdown="{mode:'click'}" aria-haspopup="true" aria-expanded="false"><img src="docs/img/user-white.png" width="20" height="20"><div class="uk-dropdown uk-dropdown-bottom" aria-hidden="true" style="top: 30px; left: 0px;" tabindex="">
                                        <ul class="uk-nav uk-nav-dropdown" >
                                           <li id="uout" ><a href="logout.php">Logout</a></li> 
                                           <li><a href="history.php">Booked Tickets</a></li>
                                        </ul>
                                    </div>
                                </div></li>
<?php
}
  ?>
                </ul>

                <a href="#tm-offcanvas" class="uk-navbar-toggle uk-visible-small" data-uk-offcanvas></a>

                <div class="uk-navbar-brand uk-navbar-center uk-visible-small"><img src="docs/images/clugo.png" width="90" height="30" title="Clubgo" alt="Clubgo"></div>

            </div>
        </nav>


        <script type="text/javascript">


var us='<?php echo $userid ?>';

if(us){


$("#uout").show();
$("#ulog").hide();

}


        </script>

        <div class="tm-section-color-2 tm-section-colored top_a">
            <div class="uk-container uk-container-center uk-text-center top_a">


               <div class="uk-slidenav-position slider" data-uk-slideshow="{autoplay:true}">
                                     
                   <ul class="uk-slideshow uk-overlay-active slider-size" data-uk-slideshow>

<?php
        

                        
                        @$vcui=explode(",",$vfetch['cuisines']);
                        @$vhigh=explode(",",$vfetch['highlights']);
                        @$vcost=explode(",",$vfetch['cost']);

                        @$vhours=explode("@",$vfetch['happy_hours']);

                       @$venimage=$vfetch['item_img1'].",".$vfetch['item_img2'].",".$vfetch['item_img3'].",".$vfetch['item_img4'];
                  
                               @$venim=explode(",",$venimage);
                        

                               foreach ($venim as $value1) {

                   if(strlen($value1)>0){


                           ?>

                       <li class="ban">
                       <a href="#" class="curs">
                         <div>
                           <p><img src="https://www.clubgo.in/cgsquad.in/backend/<?php echo $value1?>" class="bansize" alt="image1"></p>
                           <div class="uk-overlay-panel uk-overlay-background uk-overlay-fade uk-flex uk-flex-center uk-flex-middle uk-text-center bannertextbox">
                               <!-- <div>
                                   <h3>Overlay</h3>
                                   <p>Lorem <a href="#">ipsum dolor</a> sit amet, consetetur sadipscing elitr,<br>sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam.</p>
                                   <button class="uk-button uk-button-primary">Button</button>
                               </div> -->
                           </div>
                         </div>
                       </a>
                       </li>

                      
                   
                     <?php } }

                           ?>



                       <!-- <li>
                                                  <img src="http://image.cdnllnwnl.xosnetwork.com/pics33/800/YY/YYWLTRCDPYHXXXL.20150209100050.jpg" width="800" height="400" alt="image1">
                                                  <div class="uk-overlay-panel uk-overlay-background uk-overlay-fade uk-flex uk-flex-center uk-flex-middle uk-text-center bannertextbox">
                                                      <div>
                                                          <h3>Overlay</h3>
                                                          <p>Lorem <a href="#">ipsum dolor</a> sit amet, consetetur sadipscing elitr,<br>sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam.</p>
                                                          <button class="uk-button uk-button-primary">Button</button>
                                                      </div>
                                                  </div>
                                              </li>
 -->                   </ul>
                   
                    <a href="#" class="uk-slidenav uk-slidenav-contrast uk-slidenav-previous pre_icon" data-uk-slideshow-item="previous" style="color: rgba(255,255,255,0.4)"></a>
                   <a href="#" class="uk-slidenav uk-slidenav-contrast uk-slidenav-next next_icon" data-uk-slideshow-item="next" style="color: rgba(255,255,255,0.4)"></a>
                   
                   <ul class="uk-dotnav uk-dotnav-contrast uk-position-bottom uk-flex-center nextbullate">
                       <?php 
                            $q=0;
                   foreach ($venim as $value1) {

                   if(strlen($value1)>0){?>

                  <li data-uk-slideshow-item="<?php echo $q; ?>" class=""><a href="#">Item 1</a></li>

                    <?php $q++;} }?>
                   </ul>
                   
               </div>
               
               
             
            </div>
        </div>

       

<div class="tm-section tm-section-color-white" style="background:#fff;">
    <div class="uk-container uk-container-center uk-text-center">

        <h1 class="tm-margin-large-bottom evntnmhed"><strong><?php echo $vfetch['item_title']?> </strong></h1>

<h2><img src="docs/img/loc.png" style="width:25px;">  <strong><?php echo $vfetch['loc_title']?></strong></h2>


            
           <!--  <p class="evntdate">Fusion music that rocks you.</p>
            <p class="tm-margin-small-bottom">Zimmerman produces a variety of styles within the house genre and sometimes other forms of electronic music. His tracks have been included in numerous compilation albums such as the 2007 In Search of Sunrise 6: Ibiza CD. The February 2008 issue of Mixmag's music magazine included a free CD which was titled MixMag Presents: The Hottest New Name In Dance! DEADMAU5 Tech-Trance-Electro-Madness, mixed by Zimmerman.</p><br> -->
<!-- <h1 class="tm-margin-large-bottom">Event Detail<br><span style="font-size:25px;"></h1>
       
<div class="uk-grid">
    <div class="uk-width-1-2 evetsdetailbox">
        <div class="eventnmbox"><span class="evtico"><img src="docs/img/calendar.png" height="20px" width="20px"></span><span class="eventnmhed">TIME</span></div>
        <div class="eventnmbox"><span class="eventnm">Wednesday 20July, 7:30 PM Close at 4:00 AM</span></div>
        
        <div class="eventnmbox"><span class="evtico"><img src="docs/img/artist.png" height="20px" width="20px"></span><span class="eventnmhed">TIME</span></div>
        <div class="eventnmbox"><div class="artistimg"><img src="docs/img/dj.jpg"></div></div>
        
        
        <div class="eventnmbox"><span class="evtico"><img src="docs/img/calendar.png" height="20px" width="20px"></span><span class="eventnmhed">TIME</span></div>
        <div class="eventnmbox"><span class="eventnm">Wednesday 20July, 7:30 PM Close at 4:00 AM</span></div>
        
        <div class="eventnmbox"><span class="evtico"><img src="docs/img/calendar.png" height="20px" width="20px"></span><span class="eventnmhed">TIME</span></div>
        <div class="eventnmbox"><span class="eventnm">Wednesday 20July, 7:30 PM Close at 4:00 AM</span></div>
        
    </div>
    <div class="uk-width-1-2 booktiktbox">Book Tickets</div>
</div> -->

    </div>
</div>


<div class="tm-section tm-section-color-white bgico">
 <div class="uk-container uk-container-center uk-text-center">
     
     <h3 class="tm-margin-medium-bottom text_left">Photos</h3>
     <ul class="uk-thumbnav">
      <?php

      @$vimag=strlen($vfetch['venue_image']);

      if($vimag==0){?>

<div>No Photos Found.</div>


     <?php }else{

      @$vimg=explode(",",$vfetch['venue_image']);
                        

for ($i=0; $i <count($vimg) ; $i++) { ?>

<li><a href="https://www.clubgo.in/cgsquad.in/backend/<?php echo $vimg[$i]?>" data-uk-lightbox="{group:'my-group'}"><img src="https://www.clubgo.in/cgsquad.in/backend/<?php echo $vimg[$i]?>" alt="" width="100" height="100" class="bgimg"></a></li>

  
<?php }}
       ?>
         
        <!--  <li><a href="http://static.topyaps.com/wp-content/uploads/2013/05/lap-nightclub.jpg"><img src="http://static.topyaps.com/wp-content/uploads/2013/05/lap-nightclub.jpg" data-uk-lightbox="{group:'my-group'}" alt="" width="100" height="100" class="bgimg"></a></li>
         <li><a href="http://resolume.com/forum/download/file.php?id=1593" data-uk-lightbox="{group:'my-group'}"><img src="http://resolume.com/forum/download/file.php?id=1593" alt="" width="100" height="100" class="bgimg"></a></li>
         <li><a href="https://mir-s3-cdn-cf.behance.net/project_modules/disp/fbd5eb24491003.563351475c495.jpg" data-uk-lightbox="{group:'my-group'}"><img src="https://mir-s3-cdn-cf.behance.net/project_modules/disp/fbd5eb24491003.563351475c495.jpg" alt="" width="100" height="100" class="bgimg"></a></li>
         <li><a href="http://static.topyaps.com/wp-content/uploads/2013/05/skooter-nightclub-delhi.jpg" data-uk-lightbox="{group:'my-group'}"><img src="http://static.topyaps.com/wp-content/uploads/2013/05/skooter-nightclub-delhi.jpg" alt="" width="100" height="100" class="bgimg"></a></li>
         <li><a href="https://mir-s3-cdn-cf.behance.net/project_modules/disp/14818424491003.563351478f510.jpg" data-uk-lightbox="{group:'my-group'}"><img src="https://mir-s3-cdn-cf.behance.net/project_modules/disp/14818424491003.563351478f510.jpg" alt="" width="100" height="100" class="bgimg"></a></li>
         <li><a href="https://mir-s3-cdn-cf.behance.net/project_modules/disp/ef8e1624491003.56335147539a3.jpg" data-uk-lightbox="{group:'my-group'}"><img src="https://mir-s3-cdn-cf.behance.net/project_modules/disp/ef8e1624491003.56335147539a3.jpg" alt="" width="100" height="100" class="bgimg"></a></li>
         <li><a href="http://miner8.com/en/wp-content/uploads/2016/03/shroom-nightclub.jpg" data-uk-lightbox="{group:'my-group'}"><img src="http://miner8.com/en/wp-content/uploads/2016/03/shroom-nightclub.jpg" alt="" width="100" height="100" class="bgimg"></a></li>
         <li><a href="http://www.partyowl.in/wp-content/uploads/2016/01/2-7.jpg" data-uk-lightbox="{group:'my-group'}"><img src="http://www.partyowl.in/wp-content/uploads/2016/01/2-7.jpg" alt="" width="100" height="100" class="bgimg"></a></li>
         <li><a href="http://static.topyaps.com/wp-content/uploads/2013/05/lap-nightclub.jpg"><img src="http://static.topyaps.com/wp-content/uploads/2013/05/lap-nightclub.jpg" data-uk-lightbox="{group:'my-group'}" alt="" width="100" height="100" class="bgimg"></a></li>
         <li><a href="http://resolume.com/forum/download/file.php?id=1593" data-uk-lightbox="{group:'my-group'}"><img src="http://resolume.com/forum/download/file.php?id=1593" alt="" width="100" height="100" class="bgimg"></a></li>
         <li><a href="https://mir-s3-cdn-cf.behance.net/project_modules/disp/fbd5eb24491003.563351475c495.jpg" data-uk-lightbox="{group:'my-group'}"><img src="https://mir-s3-cdn-cf.behance.net/project_modules/disp/fbd5eb24491003.563351475c495.jpg" alt="" width="100" height="100" class="bgimg"></a></li>
         <li><a href="http://static.topyaps.com/wp-content/uploads/2013/05/skooter-nightclub-delhi.jpg" data-uk-lightbox="{group:'my-group'}"><img src="http://static.topyaps.com/wp-content/uploads/2013/05/skooter-nightclub-delhi.jpg" alt="" width="100" height="100" class="bgimg"></a></li>
         <li><a href="https://mir-s3-cdn-cf.behance.net/project_modules/disp/14818424491003.563351478f510.jpg" data-uk-lightbox="{group:'my-group'}"><img src="https://mir-s3-cdn-cf.behance.net/project_modules/disp/14818424491003.563351478f510.jpg" alt="" width="100" height="100" class="bgimg"></a></li>
         <li><a href="https://mir-s3-cdn-cf.behance.net/project_modules/disp/ef8e1624491003.56335147539a3.jpg" data-uk-lightbox="{group:'my-group'}"><img src="https://mir-s3-cdn-cf.behance.net/project_modules/disp/ef8e1624491003.56335147539a3.jpg" alt="" width="100" height="100" class="bgimg"></a></li>
         <li><a href="http://miner8.com/en/wp-content/uploads/2016/03/shroom-nightclub.jpg" data-uk-lightbox="{group:'my-group'}"><img src="http://miner8.com/en/wp-content/uploads/2016/03/shroom-nightclub.jpg" alt="" width="100" height="100" class="bgimg"></a></li> -->
     </ul>
     
     <!-- <h3 class="tm-margin-medium-bottom text_left">Menu</h3> -->
     <h3 class="text_left">Food</h3>
     <ul class="uk-thumbnav">
            <?php
              @$vmen=strlen($vfetch['menu_image']);
              if($vmen==0){?>

<div class="width100"><img src="docs/img/no-image.png"></div>
     <!--  <div>No Menu Found.</div> -->

              <?php }else{

            @$vmenu=explode(",",$vfetch['menu_image']);

for ($j=0; $j <count($vmenu) ; $j++) { ?>

  <li><a href="https://www.clubgo.in/cgsquad.in/backend/<?php echo $vmenu[$j]?>" data-uk-lightbox="{group:'my-group'}"><img src="https://www.clubgo.in/cgsquad.in/backend/<?php echo $vmenu[$j]?>" alt="" width="100" height="100" class="bgimg"></a></li>
  
<?php } }
       ?>
       
     </ul>



      <h3 class="text_left">Bar</h3>
   <ul class="uk-thumbnav">
<?php





     @$vbar=strlen($vfetch['bar_image']);

              if($vbar==0){?>


             <div class="width100"><img src="docs/img/no-image.png"></div>

             <?php }else{

              @$vbarmenu=explode(",",$vfetch['bar_image']);

for ($p=0; $p <count($vbarmenu) ; $p++) { ?>


  <li><a href="https://www.clubgo.in/cgsquad.in/backend/<?php echo $vbarmenu[$p]?>" data-uk-lightbox="{group:'my-group'}"><img src="https://www.clubgo.in/cgsquad.in/backend/<?php echo $vbarmenu[$p]?>" alt="" width="100" height="100" class="bgimg"></a></li>
<?php } }
     ?>

   </ul>
     
  </div>
</div>     

   
     
     <div class="tm-section tm-section-color-white whitsec bgico">
         <div class="uk-container uk-container-center uk-text-center">
    
     
            
     
            <ul class="uk-subnav uk-subnav-pill contain_center" data-uk-switcher="{connect:'#switcher-content-a-fade', animation: 'fade'}" style="margin-left:0px;">
                <!-- <li class=""><a href="#">Nearby Metro</a></li> -->
                <li  class=""><a href="#" class="met">Opening Hours</a></li>
                <li  class=""><a href="#">Known For</a></li>
                <li  class=""><a href="#">Cuisines</a></li>
                <li  class=""><a href="#">Facilities</a></li>
                <li  class=""><a href="#">Cost</a></li>
                <li  class=""><a href="#">Busy Nights</a></li>
            </ul>
    
            <ul id="switcher-content-a-fade" class="uk-switcher uk-margin" style="background:#fff;">
                <!-- <li  class="">
                    <div class="uk-grid" style="margin-top:30px">
                        <div class="uk-width-1-2"><img src="http://findicons.com/files/icons/2787/beautiful_flat_icons/128/train.png" width="60px" height="60px"><p style="font-size:18px;font-weight:bold;">Rajiv Chowk<br><span class="" style="font-size:14px">Blue Line</span></p></div>
                        
                        <div class="uk-width-1-2"><img src="http://findicons.com/files/icons/2787/beautiful_flat_icons/128/train.png" width="60px" height="60px"><p style="font-size:18px;font-weight:bold;">Rajiv Chowk<br><span class="" style="font-size:14px">Yellow Line</span></p></div>
                    </div>
                </li> -->
                <li  class="open"><?php 


for($i=0;$i<count($vhours);$i++){


$day=explode("*",$vhours[$i]);
echo ucfirst($day[0])."&nbsp;"."-"."&nbsp;";



$time=explode("#",$day[1]);

if(strlen($time[1])==1){

  
 $to=explode("-", $time[0]);



  echo $to[0]."&nbsp;"."to"."&nbsp;".$to[1];
}
else{
echo ucfirst($time[1]);
}

echo "</br>";
} 
?>
</li>
                <li  class="knowntxt" style=""><?php echo $vfetch['known_for'] ?></li>
                <li  class="knowntxt"><?php


for ($h=0; $h <count($vcui) ; $h++) {

echo $vcui[$h]."<br>";           

}




                ?></li>
                <li  class="knowntxt"><?php


for ($m=0; $m <count($vhigh) ; $m++) {
  
echo '<img src="docs/img/tick.png" width="20" height="20"/>';
echo $vhigh[$m]."<br>";           

}




                ?></li>
                <li  class="knowntxt"><?php


for ($n=0; $n <count($vcost) ; $n++) {

echo $vcost[$n]."<br>";           

}




                ?></li>
                <li  class="knowntxt"><?php 


                $bsynights=explode(",",$vfetch['busy_nights']);

                for ($bn=0; $bn <count($bsynights) ; $bn++) { 
                  
                     echo $bsynights[$bn]."</br>";

                }



                 ?></li>
            </ul>
        
    
     
         </div>
         </div>
     
     
           
  
        <div class="tm-section tm-section-color-white bgico">
                    <div class="uk-container uk-container-center uk-text-center contain">
                        <script src='https://maps.googleapis.com/maps/api/js?v=3.exp'></script><div style='overflow:hidden;height:440px;width:100%;'><div id='gmap_canvas' style='height:440px;width:100%;'></div><div><small><a href="http://embedgooglemaps.com">                 embed google maps             </a></small></div><div><small><a href="http://www.autohuren.world/">auto huren</a></small></div><style>#gmap_canvas img{max-width:none!important;background:none!important}</style></div><script type='text/javascript'>

                      var lati='<?php echo $vfetch["item_lat"]?>';
                       var longi='<?php echo $vfetch["item_lng"]?>';
                       var ti='<?php echo $vfetch["item_title"]?>';
                       var loc='<?php echo $vfetch["item_address"]?>';
                       var viewlocation=ti+','+loc;


                        function init_map(){var myOptions = {zoom:13,scrollwheel: false,center:new google.maps.LatLng(lati,longi),mapTypeId: google.maps.MapTypeId.ROADMAP};map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(lati,longi)});infowindow = new google.maps.InfoWindow({content:'<strong>'+viewlocation+'</strong>'});google.maps.event.addListener(marker, 'click', function(){infowindow.open(map,marker);});infowindow.open(map,marker);}google.maps.event.addDomListener(window, 'load', init_map);</script>
        
                    </div>
             </div>
        
        
              
          
        
         <div class="tm-footer">
            <div class="uk-container uk-container-center uk-text-center">

                <ul class="uk-subnav uk-subnav-line uk-flex-center">
                    <li><a href="https://www.facebook.com/ClubGoApp/" target="_blank">Facebook</a></li>
                    <li><a href="https://www.instagram.com/clubgoapp/" target="_blank">Instagram</a></li>
                    <li><a href="https://twitter.com/ClubGoApp" target="_blank">Twitter</a></li>
                </ul>

                <div class="uk-panel">
                    <p>Copyright ClubGo - All rights reserved.</p>
                    <a href="index.php"><img src="docs/images/clugo.png" width="90" height="30" title="Clubgo" alt="Clubgo"></a>
                </div>

            </div>
        </div>

     <div id="tm-offcanvas" class="uk-offcanvas">

    <div class="uk-offcanvas-bar">

    <ul class="uk-nav uk-nav-offcanvas uk-nav-parent-icon" data-uk-nav="{ multiple: true }">
      <li><a href="index.php">Home</a></li>
    <li><a href="about.php">About</a></li>
      <li><a href="events.php"> Event </a></li>
      <li><a href="venues.php"> Venues </a></li>
<!--           <li><a href="offer.php"> Offers </a></li> -->
      <li><a href="artist.php"> Artists </a></li>
      <li><a href="login.php" id="ulog1"> Login/Signup </a></li>


     <li style="display:none" id="uout1"><a href="logout.php">Logout</a></li>
      <li><a href="download.php"> Download App </a></li>

    </ul>

    </div>

  </div>

  <script type="text/javascript">

var us1='<?php echo $userid ?>';

if(us1){


$("#uout1").show();
$("#ulog1").hide();

}


  </script>
     
        <script>

            $.ajax({
                dataType : "jsonp",
                url      : "https://api.github.com/repos/uikit/uikit?callback=ukghapi&nocache="+Math.random(),
                success  : function(data){

                    if(!data) return;

                    if(data.data.watchers){
                        $("[data-uikit-stargazers]").html(data.data.watchers);
                    }

                    if(data.data.forks){
                        $("[data-uikit-forks]").html(data.data.forks);
                    }
                }
            });
        </script>
    </body>
</html>
<?php ob_end_flush();
 ?>