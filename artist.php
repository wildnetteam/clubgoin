<?php
include("config.php");
error_reporting(0);
ob_start();
session_start();

@$user=$_SESSION["name"];
@$userid=$_SESSION["userid"];
@$fbid=$_SESSION["fbid"];


?>

<!DOCTYPE html>
<html lang="en-gb" dir="ltr">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="keywords" content="" />
	<meta name="description" content="" />
        <title>ClubGo - Your Nightlife Conceirge</title>
        <link rel="shortcut icon" href="docs/images/clubgo-icon.png" type="image/x-icon">
        <link rel="apple-touch-icon-precomposed" href="docs/images/apple-touch-icon.png">
        <link id="data-uikit-theme" rel="stylesheet" href="docs/css/uikit.docs.min.css">
        <link rel="stylesheet" href="docs/css/docs.css">
        <link rel="stylesheet" href="docs/css/custom.css">
        <link rel="stylesheet" href="docs/css/makeweb.css">
        <link rel="stylesheet" href="docs/css/responsive.css">
        <link rel="stylesheet" href="docs/css/theme1.css">
        <link rel="stylesheet" href="vendor/highlight/highlight.css">
        <script src="vendor/jquery.js"></script>
        <script src="docs/js/uikit.min.js"></script>
        <script src="vendor/highlight/highlight.js"></script>
        <script src="docs/js/docs.js"></script>
        <script src="docs/js/slideshow.js"></script>
        <script src="docs/js/slideshow-fx.js"></script>
        <script src="docs/js/slideset.js"></script>
        <script src="docs/js/lightbox.js"></script>
        <script src="docs/js/sticky.js"></script>    
        </head>

    <body class="tm-background">

        <nav class="tm-navbar uk-navbar uk-navbar-attached" data-uk-sticky="{boundary:'#define-an-offset'}">
            <div class="uk-container uk-container-center">

                <div class="uk-animation-hover"><a class="uk-navbar-brand uk-hidden-small uk-animation-reverse uk-animation-scale" href="index.php"><img class="uk-margin uk-margin-remove" src="docs/images/clugo.png" width="120" height="40" title="Clubgo" alt="Clubgo"></a></div>

                <ul class="uk-navbar-nav uk-navbar-flip uk-hidden-small">
	                <li><a href="about.php">About</a></li>
                    <li><a href="events.php">Events</a></li>
                    <li><a href="venues.php">Venues</a></li>
<!--                     <li><a href="offer.php">Offers</a></li> -->
                    <li><a href="artist.php" class="acti">Artists</a></li>
                    <li><a href="login.php" id="ulog">Login/Signup</a></li>
                    <li class="download"><a href="download.php">Download App</a></li>
                    <?php
if($fbid){
?>
<li><div class="uk-button-dropdown" data-uk-dropdown="{mode:'click'}" aria-haspopup="true" aria-expanded="false"><img src="https://graph.facebook.com/<?php echo  $fbid ?>/picture?type=small" width="40" height="40" class="fb-img"><div class="uk-dropdown uk-dropdown-bottom" aria-hidden="true" style="top: 30px; left: 0px;" tabindex="" >
                                        <ul class="uk-nav uk-nav-dropdown">
                                           <li><a href="logout.php">Logout</a></li> 
                                           <li><a href="history.php">Booked Tickets</a></li>
                                        </ul>
                                    </div>
                                </div></li>

<?php
}
if($userid and !$fbid){
?>
<li class="mail-img"><div class="uk-button-dropdown" data-uk-dropdown="{mode:'click'}" aria-haspopup="true" aria-expanded="false"><img src="docs/img/user-white.png" width="20" height="20"><div class="uk-dropdown uk-dropdown-bottom" aria-hidden="true" style="top: 30px; left: 0px;" tabindex="">
                                        <ul class="uk-nav uk-nav-dropdown" >
                                           <li id="uout" ><a href="logout.php">Logout</a></li> 
                                           <li><a href="history.php">Booked Tickets</a></li>
                                        </ul>
                                    </div>
                                </div></li>
<?php
}
  ?>
                </ul>

                <a href="#tm-offcanvas" class="uk-navbar-toggle uk-visible-small" data-uk-offcanvas></a>

                <div class="uk-navbar-brand uk-navbar-center uk-visible-small"><img src="docs/images/clugo.png" width="90" height="30" title="Clubgo" alt="Clubgo"></div>

            </div>
        </nav>

                <script type="text/javascript">


var us='<?php echo $userid ?>';

if(us){


$("#uout").show();
$("#ulog").hide();

}


        </script>

        <div class="tm-section-color-2 tm-section-colored top_a" style="display: none;">
            <div class="uk-container uk-container-center uk-text-center top_a">


               <div class="uk-slidenav-position slider" data-uk-slideshow="">
                                     
                   <ul class="uk-slideshow uk-overlay-active slider-size" data-uk-slideshow>

    <?php

                        
                        @$slide1=mysqli_query($conn,"SELECT banner_img1  from banner where banner_type='Artist'");
                        @$bann=mysqli_fetch_array($slide1);

                       //  @$slname=$bann['banner_name'];

                       // // echo $slname;
                       //   @$slna=explode(",", $slname);

                       //   @$sldesc=$bann['banner_desc'];
                       //  // echo $sldesc;
                       //  @$slndesc=explode(";", $sldesc);

                         @$slimg=$bann['banner_img1'];
                        
                        @$slnimg=explode(",", $slimg);
                        
                    


                       // @$a=count($fetch);
                       // echo $a;
                         for ($i=0; $i <count($slnimg); $i++) { 
                           
                                   
                         

                          ?>






                       <li class="ban">
                       <a href="#" class="curs">
                       <div>    
<p class="banmax"><img src="https://www.clubgo.in/cgsquad.in/backend/<?php echo $slnimg[$i]?>" class="bansize" alt="image1"></p>


<div class="uk-overlay-panel uk-overlay-background uk-overlay-fade  uk-flex-center uk-flex-middle uk-text-center bannertextbox dead_txt">
                               <div class="dead_top">
                                   <h2 style="line-height:10px;"><?php

                                  
                              
                                     


                                   ?></h2>
                                     <h3 style="line-height:0px;">
<?php  


 


?>
                                  
                                    



                                   </h3> 
                                   <!--<h4 style="line-height:0px;margin-top:0px;"><span><img src="http://neith.googlecode.com/hg/externals/icon-set/metrostation_by_yankoa-d312tty/PNG/Communications/White/MB_0021_clock2.png" class="clockimg"></span>&nbsp; 9 pm onwards, Tuesday, 15th march</h4>
                                    --><!-- <button class="uk-button uk-button-primary">Button</button> -->
                               </div>
                           </div>
                       </div>
                       </a>
                       </li>


                           <?php }?>
                   <!--     <li>
                                                  <img src="http://s1.img.yan.vn/YanThumbNews/2167221/201511/352x176_a8d3ca89-ca7a-4697-8baa-32419ec02dbd.jpg" width="800" height="400" alt="image1">
                                                  <div class="uk-overlay-panel uk-overlay-background uk-overlay-fade uk-flex uk-flex-center uk-flex-middle uk-text-center bannertextbox">
                                                      <div>
                                                          <h1>DJ Axwell</h1>
                                                          <h2>@Vietnam</h2> -->
                                                          <!-- <button class="uk-button uk-button-primary">Button</button> -->
                                              <!--         </div>
                                                  </div>
                                              </li> -->
                   </ul>
                   
                    <a href="#" class="uk-slidenav uk-slidenav-contrast uk-slidenav-previous pre_icon" data-uk-slideshow-item="previous" style="color: rgba(255,255,255,0.4)"></a>
                   <a href="#" class="uk-slidenav uk-slidenav-contrast uk-slidenav-next next_icon" data-uk-slideshow-item="next" style="color: rgba(255,255,255,0.4)"></a>
                   
                   <ul class="uk-dotnav uk-dotnav-contrast uk-position-bottom uk-flex-center nextbullate">

                     <?php 

                    for ($i=0; $i <count($slnimg); $i++) {?>

                  <li data-uk-slideshow-item="<?php echo $i ?>" class=""><a href="#">Item 1</a></li>

                    <?php }?>



                       <!-- <li data-uk-slideshow-item="0" class=""><a href="#">Item 1</a></li>
                       <li data-uk-slideshow-item="1" class=""><a href="#">Item 2</a></li>
                       <li data-uk-slideshow-item="2" class=""><a href="#">Item 3</a></li>
                       <li data-uk-slideshow-item="3" class=""><a href="#">Item 4</a></li>
                       <li data-uk-slideshow-item="4" class=""><a href="#">Item 5</a></li> -->
                   </ul>
                   
                   
               </div>
               
               
             
            </div>
        </div>

       

<div class="tm-section tm-section-color-white bgico">
    <div class="uk-container uk-container-center uk-text-center">

        <h1 class="tm-margin-large-bottom"><strong>ARTISTS</strong></h1>

        <div class="uk-grid vetop">



                   <?php

                          @$sel6=mysqli_query($conn,"SELECT * from djtable where d_published='1'");
                         while(@$fet6=mysqli_fetch_array($sel6)){


                            ?>
            


   <div class="uk-width-small-1-1 uk-width-medium-1-2 uk-width-large-1-3" style="padding-top:30px;">
                <figure class="uk-overlay uk-overlay-hover"><div class="uk-panel uk-panel-box"  data-uk-modal="{target:'#my-id'}" onclick="show(<?php echo $fet6['d_id'];?>)"><img src="https://www.clubgo.in/cgsquad.in/backend/<?php echo $fet6['d_img']?>" style="width:300px; height:200px;" alt=""><div class="artist_txt"><?php echo $fet6['d_name']?></div></div><!-- <div class="uk-overlay-panel uk-overlay-fade uk-overlay-background"><h3></h3><p style="text-align:justify;line-spacing:5px;"><?php echo $fet6['d_desc']?></p></div><span class="uk-position-cover"></span>--></figure>
            </div> 
<div id="my-id" class="uk-modal modal-pop">
    <div class="uk-modal-dialog" >
        <a class="uk-modal-close uk-close"></a>
        <p id="show"></p>
    </div>
</div>

                         <?php }



                           ?>




          <!--   <div class="uk-width-1-3">
                <figure class="uk-overlay uk-overlay-hover hgt_crd"><div class="uk-panel uk-panel-box hgt_crd"><img src="http://www.radioandmusic.com/sites/www.radioandmusic.com/files/images/entertainment/2015/01/23/Cocktail---cd-Packshot.jpg" alt=""><div class="artist_txt">DJ Savo</div></div><div class="uk-overlay-panel uk-overlay-fade uk-overlay-background"><h3>Anchor</h3><p style="text-align:justify;line-spacing:5px;">"DJ Savo" is one of the esteem DJ of today’s entertainment. imty been creative with his talent in all music genres to fit the right occasions for all his clients. DJ imty’s passion towards his work has always kept him notable DJ over many years.</p></div><span class="uk-position-cover"></span></figure>
            </div>
            <div class="uk-width-1-3">
                <figure class="uk-overlay uk-overlay-hover hgt_crd"><div class="uk-panel uk-panel-box hgt_crd"><img src="http://townista.com/static/files/item/zdoko9t658bz.jpg"><div class="artist_txt">DJ Shirren</div></div><div class="uk-overlay-panel uk-overlay-fade uk-overlay-background"><h3>Anchor</h3><p style="text-align:justify;line-spacing:5px;">"DJ Savo" is one of the esteem DJ of today’s entertainment. imty been creative with his talent in all music genres to fit the right occasions for all his clients. DJ imty’s passion towards his work has always kept him notable DJ over many years.</p></div><span class="uk-position-cover"></span></figure>
            </div>
            <div class="uk-width-1-3">
                <figure class="uk-overlay uk-overlay-hover hgt_crd"><div class="uk-panel uk-panel-box hgt_crd"><img src="http://4.bp.blogspot.com/-9vYPej7jx80/VcRkq6kcnqI/AAAAAAAACHM/fTkOb9cFm3E/s640/badsha.jpg"><div class="artist_txt">DJ Mayur</div></div><div class="uk-overlay-panel uk-overlay-fade uk-overlay-background"><h3>Anchor</h3><p style="text-align:justify;line-spacing:5px;">"DJ Savo" is one of the esteem DJ of today’s entertainment. imty been creative with his talent in all music genres to fit the right occasions for all his clients. DJ imty’s passion towards his work has always kept him notable DJ over many years.</p></div><span class="uk-position-cover"></span></figure>
            </div> -->
        </div>


        <script type="text/javascript">

function show(e){


var data1='id='+e;


                            $.ajax({

                                url:'artistajax.php',
                                type:'POST',
                                data:data1,
                                cache:false,

                                success:function(rex){

                                
                                        document.getElementById('show').textContent=rex;
                                      

                                 
                             }
                         });





}





</script>

<?php

// @$id=$_GET['id'];


// @$select=mysqli_query($conn,"SELECT d_desc from djtable where d_id='$id' ");
// @$fetch=mysqli_fetch_array($select);

// echo $fetch['d_desc']; 


?>
        
       <!--  <div class="uk-grid">
                    <div class="uk-width-1-3">
                        <figure class="uk-overlay uk-overlay-hover hgt_crd"><div class="uk-panel uk-panel-box hgt_crd"><img src="http://www.radioandmusic.com/sites/www.radioandmusic.com/files/images/entertainment/2016/02/08/dj-(4).jpg"><div class="artist_txt">DJ Kygo</div></div><div class="uk-overlay-panel uk-overlay-fade uk-overlay-background"><h3>Anchor</h3><p style="text-align:justify;line-spacing:5px;">"DJ Savo" is one of the esteem DJ of today’s entertainment. imty been creative with his talent in all music genres to fit the right occasions for all his clients. DJ imty’s passion towards his work has always kept him notable DJ over many years.</p></div><span class="uk-position-cover"></span></figure>
                    </div>
                    <div class="uk-width-1-3">
                        <figure class="uk-overlay uk-overlay-hover hgt_crd"><div class="uk-panel uk-panel-box hgt_crd"><img src="http://youngsterchoice.com/wp-content/uploads/2015/07/Aastha-Gill-Wiki.jpg"><div class="artist_txt">Aastha Gill</div></div><div class="uk-overlay-panel uk-overlay-fade uk-overlay-background"><h3>Anchor</h3><p style="text-align:justify;line-spacing:5px;">"DJ Savo" is one of the esteem DJ of today’s entertainment. imty been creative with his talent in all music genres to fit the right occasions for all his clients. DJ imty’s passion towards his work has always kept him notable DJ over many years.</p></div><span class="uk-position-cover"></span></figure>
                    </div>
                    <div class="uk-width-1-3">
                        <figure class="uk-overlay uk-overlay-hover hgt_crd"><div class="uk-panel uk-panel-box hgt_crd"><img src="http://timesofindia.indiatimes.com/photo/50837143.cms"><div class="artist_txt">DJ Ana</div></div><div class="uk-overlay-panel uk-overlay-fade uk-overlay-background"><h3>Anchor</h3><p style="text-align:justify;line-spacing:5px;">"DJ Savo" is one of the esteem DJ of today’s entertainment. imty been creative with his talent in all music genres to fit the right occasions for all his clients. DJ imty’s passion towards his work has always kept him notable DJ over many years.</p></div><span class="uk-position-cover"></span></figure>
                    </div>
                </div> -->
                
             <!--    <div class="uk-grid">
                    <div class="uk-width-1-3">
                            <figure class="uk-overlay uk-overlay-hover hgt_crd"><div class="uk-panel uk-panel-box hgt_crd"><img src="http://musicinafrica.net/sites/default/files/styles/zoom/public/images/mia-articles/201603/djmawi-africa.jpg?itok=OSnmfBJv"><div class="artist_txt">Algerian Band</div></div><div class="uk-overlay-panel uk-overlay-fade uk-overlay-background"><h3>Anchor</h3><p style="text-align:justify;line-spacing:5px;">"DJ Savo" is one of the esteem DJ of today’s entertainment. imty been creative with his talent in all music genres to fit the right occasions for all his clients. DJ imty’s passion towards his work has always kept him notable DJ over many years.</p></div><span class="uk-position-cover"></span></figure>
                            </div>
                            <div class="uk-width-1-3">
                                <figure class="uk-overlay uk-overlay-hover hgt_crd"><div class="uk-panel uk-panel-box hgt_crd"><img src="http://img2.ymlp347.com/plexipr_VerbotenSTAGEONE08232015SelectsWATERMARK33Matadorsm.jpg"><div class="artist_txt">Verbotene</div></div><div class="uk-overlay-panel uk-overlay-fade uk-overlay-background"><h3>Anchor</h3><p style="text-align:justify;line-spacing:5px;">"DJ Savo" is one of the esteem DJ of today’s entertainment. imty been creative with his talent in all music genres to fit the right occasions for all his clients. DJ imty’s passion towards his work has always kept him notable DJ over many years.</p></div><span class="uk-position-cover"></span></figure>
                            </div>
                            <div class="uk-width-1-3">
                                <figure class="uk-overlay uk-overlay-hover hgt_crd"><div class="uk-panel uk-panel-box hgt_crd"><img src="http://www.radioandmusic.com/sites/www.radioandmusic.com/files/images/entertainment/2015/07/28/skrillex.jpg"><div class="artist_txt">Skrillex</div></div><div class="uk-overlay-panel uk-overlay-fade uk-overlay-background"><h3>Anchor</h3><p style="text-align:justify;line-spacing:5px;">"DJ Savo" is one of the esteem DJ of today’s entertainment. imty been creative with his talent in all music genres to fit the right occasions for all his clients. DJ imty’s passion towards his work has always kept him notable DJ over many years.</p></div><span class="uk-position-cover"></span></figure>
                            </div>
                        </div>   -->      

    </div>
</div>


        
        
        
          
        

        

        <div class="tm-footer">
            <div class="uk-container uk-container-center uk-text-center">

                <ul class="uk-subnav uk-subnav-line uk-flex-center">
                    <li><a href="https://www.facebook.com/ClubGoApp/" target="_blank">Facebook</a></li>
                    <li><a href="https://www.instagram.com/clubgoapp/" target="_blank">Instagram</a></li>
                    <li><a href="https://twitter.com/ClubGoApp" target="_blank">Twitter</a></li>
                </ul>

                <div class="uk-panel">
                    <p>Copyright ClubGo - All rights reserved.</p>
                    <a href="index.php"><img src="docs/images/clugo.png" width="90" height="30" title="ClubGo" alt="ClubGo"></a>
                </div>

            </div>
        </div>

<div id="tm-offcanvas" class="uk-offcanvas">

	  <div class="uk-offcanvas-bar">

		<ul class="uk-nav uk-nav-offcanvas uk-nav-parent-icon" data-uk-nav="{ multiple: true }">
		  <li><a href="index.php">Home</a></li>
	  <li><a href="about.php">About</a></li>
		  <li><a href="events.php"> Event </a></li>
		  <li><a href="venues.php"> Venues </a></li>
<!--           <li><a href="offer.php"> Offers </a></li> -->
		  <li><a href="artist.php"> Artists </a></li>
		  <li><a href="login.php" id="ulog1"> Login/Signup </a></li>


		 <li style="display:none" id="uout1"><a href="logout.php">Logout</a></li>
		  <li><a href="download.php"> Download App </a></li>

		</ul>

	  </div>

	</div>

	<script type="text/javascript">

var us1='<?php echo $userid ?>';

if(us1){


$("#uout1").show();
$("#ulog1").hide();

}


	</script>
        
        <script>

            $.ajax({
                dataType : "jsonp",
                url      : "https://api.github.com/repos/uikit/uikit?callback=ukghapi&nocache="+Math.random(),
                success  : function(data){

                    if(!data) return;

                    if(data.data.watchers){
                        $("[data-uikit-stargazers]").html(data.data.watchers);
                    }

                    if(data.data.forks){
                        $("[data-uikit-forks]").html(data.data.forks);
                    }
                }
            });
        </script>
    </body>
</html>
 <?php ob_end_flush();
 ?>