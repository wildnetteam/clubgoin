<?php
include("config.php");
error_reporting(0);
ob_start();
session_start();

@$user=$_SESSION["name"];
@$userid=$_SESSION["userid"];
@$fbid=$_SESSION["fbid"];

?>

<!DOCTYPE html>
<html lang="en-gb" dir="ltr">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="keywords" content="" />
	<meta name="description" content="" />
        <title>ClubGo - Your Nightlife Conceirge</title>
        <link rel="shortcut icon" href="docs/images/clubgo-icon.png" type="image/x-icon">
        <link rel="apple-touch-icon-precomposed" href="docs/images/apple-touch-icon.png">
        <link id="data-uikit-theme" rel="stylesheet" href="docs/css/uikit.docs.min.css">
        <link rel="stylesheet" href="docs/css/docs.css">
        <link rel="stylesheet" href="docs/css/custom.css">
        <link rel="stylesheet" href="docs/css/makeweb.css">
        <link rel="stylesheet" href="docs/css/responsive.css">
        <link rel="stylesheet" href="docs/css/theme1.css">
        <link rel="stylesheet" href="vendor/highlight/highlight.css">
        <script src="vendor/jquery.js"></script>
        <script src="docs/js/uikit.min.js"></script>
        <script src="vendor/highlight/highlight.js"></script>
        <script src="docs/js/docs.js"></script>
        <script src="docs/js/slideshow.js"></script>
        <script src="docs/js/slideshow-fx.js"></script>
        <script src="docs/js/slideset.js"></script>
        <script src="docs/js/lightbox.js"></script>
        <script src="docs/js/sticky.js"></script>    
        </head>

    <body class="tm-background">

        <nav class="tm-navbar uk-navbar uk-navbar-attached" data-uk-sticky="{boundary:'#define-an-offset'}">
            <div class="uk-container uk-container-center">

                <div class="uk-animation-hover"><a class="uk-navbar-brand uk-hidden-small uk-animation-reverse uk-animation-scale" href="index.php"><img class="uk-margin uk-margin-remove" src="docs/images/clugo.png" width="120" height="40" title="Clubgo" alt="Clubgo"></a></div>

                <ul class="uk-navbar-nav uk-navbar-flip uk-hidden-small">
	                <li><a href="#" class="acti">About</a></li>
                    <li><a href="events.php">Events</a></li>
                    <li><a href="venues.php">Venues</a></li>
<!--                     <li><a href="offer.php">Offers</a></li> -->
                    <li><a href="artist.php">Artists</a></li>
                    <li><a href="login.php" id="ulog">Login/Signup</a></li>
                    <li class="download"><a href="download.php">Download App</a></li>
                    <?php
if($fbid){
?>
<li><div class="uk-button-dropdown" data-uk-dropdown="{mode:'click'}" aria-haspopup="true" aria-expanded="false"><img src="https://graph.facebook.com/<?php echo  $fbid ?>/picture?type=small" width="40" height="40" class="fb-img"><div class="uk-dropdown uk-dropdown-bottom" aria-hidden="true" style="top: 30px; left: 0px;" tabindex="" >
										<ul class="uk-nav uk-nav-dropdown">
										   <li><a href="logout.php">Logout</a></li>
										   <li><a href="history.php">Booked Tickets</a></li> 
										</ul>
									</div>
								</div></li>

<?php
}
if($userid and !$fbid){
?>
<li class="mail-img"><div class="uk-button-dropdown" data-uk-dropdown="{mode:'click'}" aria-haspopup="true" aria-expanded="false"><img src="docs/img/user-white.png" width="20" height="20"><div class="uk-dropdown uk-dropdown-bottom" aria-hidden="true" style="top: 30px; left: 0px;" tabindex="">
										<ul class="uk-nav uk-nav-dropdown" >
										   <li id="uout" ><a href="logout.php">Logout</a></li>
										   <li><a href="history.php">Booked Tickets</a></li> 
										</ul>
									</div>
								</div></li>
<?php
}
  ?>
                </ul>

                <a href="#tm-offcanvas" class="uk-navbar-toggle uk-visible-small" data-uk-offcanvas></a>

                <div class="uk-navbar-brand uk-navbar-center uk-visible-small"><img src="docs/images/clugo.png" width="90" height="30" title="Clubgo" alt="Clubgo"></div>

            </div>
        </nav>

                <script type="text/javascript">


var us='<?php echo $userid ?>';

if(us){


$("#uout").show();
$("#ulog").hide();

}


        </script>
        

        <div class="tm-section-color-2 tm-section-colored top_a">
            <div class="uk-container uk-container-center uk-text-center top_a">


             <p class="cg-about">ClubGo is a Nightlife Discovery and Booking Platform that enables nightlife junkies to know about the latest nightlife events in their city. These users can then book tickets or reserve tables at the hottest clubs, lounges and cafes in their town. 
The users are also able to get details about their favourite venues. The details includes Venue Locations, Cuisine, Music Types and Entry times. 
They are also able to get the entire details of an event at their fingerprints through our apps or website.</p> 
               
               
             
            </div>
        </div>

       
   <div class="tm-footer foot-fix">
            <div class="uk-container uk-container-center uk-text-center">

                <ul class="uk-subnav uk-subnav-line uk-flex-center">
                    <li><a href="https://www.facebook.com/ClubGoApp/" target="_blank">Facebook</a></li>
                    <li><a href="https://www.instagram.com/clubgoapp/" target="_blank">Instagram</a></li>
                    <li><a href="https://twitter.com/ClubGoApp" target="_blank">Twitter</a></li>
                </ul>

                <div class="uk-panel">
                    <p>Copyright ClubGo - All rights reserved.</p>
                    <a href="index.php"><img src="docs/images/clugo.png" width="90" height="30" title="ClubGo" alt="ClubGo"></a>
                </div>

            </div>
        </div>

<div id="tm-offcanvas" class="uk-offcanvas">

	  <div class="uk-offcanvas-bar">

		<ul class="uk-nav uk-nav-offcanvas uk-nav-parent-icon" data-uk-nav="{ multiple: true }">
		  <li><a href="index.php">Home</a></li>
	  <li><a href="about.php">About</a></li>
		  <li><a href="events.php"> Event </a></li>
		  <li><a href="venues.php"> Venues </a></li>
<!--           <li><a href="offer.php"> Offers </a></li> -->
		  <li><a href="artist.php"> Artists </a></li>
		  <li><a href="login.php" id="ulog1"> Login/Signup </a></li>


		 <li style="display:none" id="uout1"><a href="logout.php">Logout</a></li>
		  <li><a href="download.php"> Download App </a></li>

		</ul>

	  </div>

	</div>

	<script type="text/javascript">

var us1='<?php echo $userid ?>';

if(us1){


$("#uout1").show();
$("#ulog1").hide();

}


	</script>
        
        <script>

            $.ajax({
                dataType : "jsonp",
                url      : "https://api.github.com/repos/uikit/uikit?callback=ukghapi&nocache="+Math.random(),
                success  : function(data){

                    if(!data) return;

                    if(data.data.watchers){
                        $("[data-uikit-stargazers]").html(data.data.watchers);
                    }

                    if(data.data.forks){
                        $("[data-uikit-forks]").html(data.data.forks);
                    }
                }
            });
        </script>
    </body>
</html>
 <?php ob_end_flush();
 ?>