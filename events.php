<?php
include("config.php");
error_reporting(0);
ob_start();
session_start();
@$lati=$_SESSION["lati"];
@$longi=$_SESSION["longi"];

@$userid=$_SESSION["userid"];
@$user=$_SESSION["name"];
@$fbid=$_SESSION["fbid"];

?>

<!DOCTYPE html>
<html lang="en-gb" dir="ltr">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
                 <meta name="keywords" content="" />
	<meta name="description" content="" />
        <title>ClubGo - Your Nightlife Conceirge</title>
        <link rel="shortcut icon" href="docs/images/clubgo-icon.png" type="image/x-icon">
        <link rel="apple-touch-icon-precomposed" href="docs/images/apple-touch-icon.png">
        <link id="data-uikit-theme" rel="stylesheet" href="docs/css/uikit.docs.min.css">
        <link rel="stylesheet" href="docs/css/docs.css">
        <link rel="stylesheet" href="docs/css/custom.css">
        <link rel="stylesheet" href="docs/css/makeweb.css">
<!--         <link rel="stylesheet" href="docs/css/theme1.css"> -->
        <link rel="stylesheet" href="docs/css/responsive.css">
        <link rel="stylesheet" href="docs/css/grid.css">
        <link rel="stylesheet" href="vendor/highlight/highlight.css">
        <script src="vendor/jquery.js"></script>
        <script src="docs/js/uikit.min.js"></script>
        <script src="vendor/highlight/highlight.js"></script>
        <script src="docs/js/docs.js"></script>
        <script src="docs/js/slideshow.js"></script>
        <script src="docs/js/slideshow-fx.js"></script>
        <script src="docs/js/slideset.js"></script>   
        <script src="docs/js/sticky.js"></script>

    </head>
<style>

.drop-down{
float: left;
width: 32%;
background: #fff;
border: 1px solid #ddd;
border-right:0px;
height:50px;
}

.colr{
color:#000!important;
font-weight:300;
height:30px;
}

 .colr:hover{
 background:#eee!important;
 box-shadow:none!important;
 text-shadow:none!important;
 }
.bot-drop{width:100%;}

.uk-tab{margin:14px;border-bottom:none}

div.uk-width-1-3{margin:10px 0px;}
.evntbtn {
position: relative;
bottom: 10px;
text-align: center;
/* left: 40%; */
}
.tm-overlay-uniq .tm-panel-caption {
padding: 0;

}
.txt_anim_b {
width: 100%;
margin-top: -20px;
}
</style>
    <body class="tm-background">

       <nav class="tm-navbar uk-navbar uk-navbar-attached">
      <div class="uk-container uk-container-center">

        <div class="uk-animation-hover"><a class="uk-navbar-brand uk-hidden-small uk-animation-reverse uk-animation-scale" href="index.php"><img class="uk-margin uk-margin-remove" src="docs/images/clugo.png" width="120" height="40" title="Clubgo" alt="Clubgo"></a></div>

        <ul class="uk-navbar-nav uk-hidden-small uk-navbar-flip">
          <li><a href="about.php">About</a></li>
          <li><a href="events.php" class="acti">Events</a></li>
          <li><a href="venues.php">Venues</a></li>
<!--           <li><a href="offer.php">Offers</a></li> -->
          <li><a href="artist.php">Artists</a></li>
          <li><a href="login.php" id="ulog">Login/Signup</a></li>
<!--          <li style="display:none" id="uout"><a href="logout.php">Logout</a></li> -->
          <!-- <li><a href="#">Profile</a></li>
          <li><a href="#">Register</a></li> -->
          <li class="download"><a href="download.php">Download App</a></li>
          
          <?php
if($fbid){
?>
<li><div class="uk-button-dropdown" data-uk-dropdown="{mode:'click'}" aria-haspopup="true" aria-expanded="false"><img src="https://graph.facebook.com/<?php echo  $fbid ?>/picture?type=small" width="40" height="40" class="fb-img"><div class="uk-dropdown uk-dropdown-bottom" aria-hidden="true" style="top: 30px; left: 0px;" tabindex="" >
                                        <ul class="uk-nav uk-nav-dropdown">
                                           <li><a href="logout.php">Logout</a></li> 
                                           <li><a href="history.php">Booked Tickets</a></li>
                                        </ul>
                                    </div>
                                </div></li>

<?php
}
if($userid and !$fbid){
?>
<li class="mail-img"><div class="uk-button-dropdown" data-uk-dropdown="{mode:'click'}" aria-haspopup="true" aria-expanded="false"><img src="docs/img/user-white.png" width="20" height="20"><div class="uk-dropdown uk-dropdown-bottom" aria-hidden="true" style="top: 30px; left: 0px;" tabindex="">
                                        <ul class="uk-nav uk-nav-dropdown" >
                                           <li id="uout" ><a href="logout.php">Logout</a></li> 
                                           <li><a href="history.php">Booked Tickets</a></li>
                                        </ul>
                                    </div>
                                </div></li>
<?php
}
  ?>


<!--
<li><div class="uk-button-dropdown" data-uk-dropdown="{mode:'click'}" aria-haspopup="true" aria-expanded="false">
                                    <div class="uk-dropdown uk-dropdown-bottom" aria-hidden="true" style="top: 30px; left: 0px;" tabindex="">
                                        <ul class="uk-nav uk-nav-dropdown">
                                           <li style="display:none" id="uout"><a href="logout.php">Logout</a></li> 
                                        </ul>
                                    </div>
                                </div></li>
-->
        </ul>

        <a href="#tm-offcanvas" class="uk-navbar-toggle uk-visible-small" data-uk-offcanvas></a>

        <div class="uk-navbar-brand uk-navbar-center uk-visible-small"><img src="docs/images/clugo.png" width="120" height="50" title="Clubgo" alt="Clubgo"></div>

      </div>
    </nav>

        <script type="text/javascript">


var us='<?php echo $userid ?>';

if(us){


$("#uout").show();
$("#ulog").hide();

}


        </script>

        <div class="tm-section-color-2 tm-section-colored top_a" style="display: none;">
            <div class="uk-container uk-container-center uk-text-center top_a eventcontainer">


               <div class="uk-slidenav-position slidr" data-uk-slideshow="">
                                     
                   <ul class="uk-slideshow uk-overlay-active" data-uk-slideshow>

             <?php

                        

                        @$slide1=mysqli_query($conn,"SELECT banner_img1, banner_eventid  from banner where banner_type='Event'");
                        @$bann=mysqli_fetch_array($slide1);

                        @$slname=$bann['banner_eventid'];

                       // echo $slname;
                         @$slna=explode(",", $slname); 

                        //  @$sldesc=$bann['banner_desc'];
                        // // echo $sldesc;
                        // @$slndesc=explode(";", $sldesc);

                         @$slimg=$bann['banner_img1'];
                        
                        @$slnimg=explode(",", $slimg);
                        
                    


                        //@$a=count($fetch);
                       // echo $a;
                         foreach ($slna as $bannerkeyid ) {

                      if(strlen($bannerkeyid)>0){
                           
                           @$query=mysqli_query($conn,"SELECT e.e_id,e.e_name ,e.e_date,e.e_enddate,e.e_end,i.item_title from event e,items i where e.item_id=i.item_id and e.e_id='$bannerkeyid' ");
                                    @$fetch=mysqli_fetch_array($query);  

                                    @$splitdate=explode(",",$fetch['e_date']); //start date 

                                    @$endate=explode(",",$fetch['e_enddate']);


  @$endtime=explode(",",$fetch['e_end']);


@$cubanner=date("Y-m-d",time());

@$time= date("H:i",time()); //current time
@$checkbanner=false;            

          
                                   
                      for ($p=0;$p<count($splitdate);$p++ ) {





                      if($splitdate[$p]==$endate[$p]){



                              
                      
                        if ($splitdate[$p] >= $cubanner )
                         
                        {



                         @$findid=array_search($fetch['e_id'], $slna);
                      

        if(strlen($newdate)==0){

                          $newdate=$splitdate[$p];


                             }else{


                              $newdate=$newdate.",".$splitdate[$p];
                             }
            
                             if(strlen($newvar)==0){

                          $newvar=$fetch['e_id'];


                             }else{


                              $newvar=$newvar.",".$fetch['e_id'];
                             }
                          
                           
                                   
                         

                          ?>






                       <li class="ban">
                       <a href="event_detail.php?id=<?php echo $fetch['e_id'] ?>&date=<?php echo date(" l, dS F", strtotime($splitdate[$p]))?>">
                        <div>   
<span class="banmax"><img src="https://www.clubgo.in/cgsquad.in/backend/<?php echo $slnimg[$findid]; ?>" class="bansize" alt="image1"></span>


<div class="uk-overlay-panel uk-overlay-background uk-overlay-fade  uk-flex-center uk-flex-middle uk-text-center bannertextbox dead_txt" style="opacity:1;">
                               <div class="dead_top">
                                   <h2 style="line-height:10px;padding-left: 25px;"><?php

                                  
                                      echo $fetch['e_name'];
                       
                                     


                                   ?></h2>
                                     <h3 style="line-height:0px;padding-left: 25px;">

                                  <?php

                                  echo $fetch['item_title'];

?>

                                   </h3> 
                                   <!--<h4 style="line-height:0px;margin-top:0px;"><span><img src="http://neith.googlecode.com/hg/externals/icon-set/metrostation_by_yankoa-d312tty/PNG/Communications/White/MB_0021_clock2.png" class="clockimg"></span>&nbsp; 9 pm onwards, Tuesday, 15th march</h4>
                                    --><!-- <button class="uk-button uk-button-primary">Button</button> -->
                               </div>
                           </div>
                        </div>
                       </a>
                       </li>

<?php  } } if($splitdate[$p] < $cubanner && $endate[$p] >= $cubanner and $time<=$endtime[$p] ){



@$findid=array_search($fetch['e_id'], $slna);


        if(strlen($newdate)==0){

                          $newdate=$splitdate[$p];


                             }else{


                              $newdate=$newdate.",".$splitdate[$p];
                             }



                             if(strlen($newvar)==0){

                          $newvar=$fetch['e_id'];


                             }else{


                              $newvar=$newvar.",".$fetch['e_id'];
                             }

                            ?>




                       <li class="ban">
                       <a href="event_detail.php?id=<?php echo $fetch['e_id'] ?>&date=<?php echo date(" l, dS F", strtotime($splitdate[$p]))?>">
                        <div>   
<span class="banmax"><img src="https://www.clubgo.in/cgsquad.in/backend/<?php echo $slnimg[$findid]; ?>" class="bansize" alt="image1"></span>


<div class="uk-overlay-panel uk-overlay-background uk-overlay-fade  uk-flex-center uk-flex-middle uk-text-center bannertextbox dead_txt" style="opacity:1;">
                               <div class="dead_top">
                                   <h2 style="line-height:10px;padding-left: 25px;"><?php

                                  
                                     echo $fetch['e_name'];
                                    


                                   ?></h2>
                                     <h3 style="line-height:0px;padding-left: 25px;">

                              
                                                                    <?php

                                  echo $fetch['item_title'];

?>



                                   </h3> 
                                   <!--<h4 style="line-height:0px;margin-top:0px;"><span><img src="http://neith.googlecode.com/hg/externals/icon-set/metrostation_by_yankoa-d312tty/PNG/Communications/White/MB_0021_clock2.png" class="clockimg"></span>&nbsp; 9 pm onwards, Tuesday, 15th march</h4>
                                    --><!-- <button class="uk-button uk-button-primary">Button</button> -->
                               </div>
                           </div>
                        </div>
                       </a>
                       </li>



                       <?php







                           }if($splitdate[$p] >= $cubanner && $endate[$p] > $cubanner){



@$findid=array_search($fetch['e_id'], $slna);


                                if(strlen($newdate)==0){

                          $newdate=$splitdate[$p];


                             }else{


                              $newdate=$newdate.",".$splitdate[$p];
                             }


                             if(strlen($newvar)==0){

                          $newvar=$fetch['e_id'];


                             }else{


                              $newvar=$newvar.",".$fetch['e_id'];
                             }


                            ?>



                       <li class="ban">
                       <a href="event_detail.php?id=<?php echo $fetch['e_id'] ?>&date=<?php echo date(" l, dS F", strtotime($splitdate[$p]))?>">
                        <div>   
<span class="banmax"><img src="https://www.clubgo.in/cgsquad.in/backend/<?php echo $slnimg[$findid]; ?>" class="bansize" alt="image1"></span>


<div class="uk-overlay-panel uk-overlay-background uk-overlay-fade  uk-flex-center uk-flex-middle uk-text-center bannertextbox dead_txt" style="opacity:1;">
                               <div class="dead_top">
                                   <h2 style="line-height:10px;padding-left: 25px;"><?php

                                  
                                     echo $fetch['e_name'];
                                     


                                   ?></h2>
                                     <h3 style="line-height:0px;padding-left: 25px;">

                                  
                                    
                                <?php

                                  echo $fetch['item_title'];

?>


                                   </h3> 
                                   <!--<h4 style="line-height:0px;margin-top:0px;"><span><img src="http://neith.googlecode.com/hg/externals/icon-set/metrostation_by_yankoa-d312tty/PNG/Communications/White/MB_0021_clock2.png" class="clockimg"></span>&nbsp; 9 pm onwards, Tuesday, 15th march</h4>
                                    --><!-- <button class="uk-button uk-button-primary">Button</button> -->
                               </div>
                           </div>
                        </div>
                       </a>
                       </li>



               <?php
                           }




                         }   } } ?>



                             
                       
                     <!--   <li>
                                                  <img src="http://blog.wowtables.com/wp-content/uploads/2014/10/halloween-pune-header-1.jpg" width="800" height="300" alt="image1">
                                                  <div class="uk-overlay-panel uk-overlay-background uk-overlay-fade  uk-flex-center uk-flex-middle uk-text-center bannertextbox dead_txt">
                                                      <div class="dead_top">
                                                          <h2 style="line-height:10px;padding-left:25px">Hari and Sukhmani Live</h2>
                                                          <h3 style="line-height:0px;"><span><img src="http://fieldofgreenspc.com/assets/img/location.png" class="locimg"></span> Downtown, Gurgaon 2KM</h3>
                                                          <h4 style="line-height:0px;margin-top:0px;"><span><img src="http://neith.googlecode.com/hg/externals/icon-set/metrostation_by_yankoa-d312tty/PNG/Communications/White/MB_0021_clock2.png" class="clockimg"></span>&nbsp; 9 pm onwards, Tuesday, 15th march</h4> -->
                                                          <!-- <button class="uk-button uk-button-primary">Button</button> -->
                                                <!--       </div>
                                                  </div>
                                              </li> -->
       </ul>


       
                   <a href="#" class="uk-slidenav uk-slidenav-contrast uk-slidenav-previous pre_icon" data-uk-slideshow-item="previous" style="color: rgba(255,255,255,0.4)"></a>
                   <a href="#" class="uk-slidenav uk-slidenav-contrast uk-slidenav-next next_icon" data-uk-slideshow-item="next" style="color: rgba(255,255,255,0.4)"></a>
                   
                   
                   
                   <ul class="uk-dotnav uk-dotnav-contrast uk-position-bottom uk-flex-center nextbullate">

                                  <?php 



@$newitem= explode(",",$newvar);
                 
                 @$arritem=array_unique($newitem);
                
//print_r($newitem);
               $arritem1=count($arritem);

         @$newitemdate= explode(",",$newdate);

           

for($g=0;$g<$arritem1;$g++){





                    ?>

                  <li data-uk-slideshow-item="<?php echo $g; ?>" class=""><a href="#">Item 1</a></li>

                    <?php }?>
                       <!-- <li data-uk-slideshow-item="0" class=""><a href="#">Item 1</a></li>
                       <li data-uk-slideshow-item="1" class=""><a href="#">Item 2</a></li>
                         <li data-uk-slideshow-item="2" class=""><a href="#">Item 3</a></li>
                       <li data-uk-slideshow-item="3" class=""><a href="#">Item 4</a></li>
                       <li data-uk-slideshow-item="4" class=""><a href="#">Item 5</a></li> -->
                   </ul>
                   
                   
                   
               </div>
               
               
             
            </div>
        </div>

       

<div class="tm-section tm-section-color-white bgico" style="border:1px solid #ddd">
    <div class=" uk-container-center uk-text-center eventcontainer">


<section class="tm-top-c uk-grid" data-uk-grid-match="{target:'&gt; div &gt; .uk-panel'}" data-uk-grid-margin="">
<div class="uk-width-1-1 uk-row-first"><div class="uk-panel">
<div class="uk-panel">

        <div class="uk-panel-teaser uk-cover-background uk-position-relative">


<!--         <img class="uk-invisible" style="max-height: 70px;" src="http://bossfight.co/wp-content/uploads/2015/08/boss-fight-free-stock-photography-images-high-resolution-macbook-pro-iphone.jpg" alt=""> -->


        <div class="uk-position-bottom uk-margin-left uk-margin-right uk-contrast respdrop">

               



<ul class="uk-tab" data-uk-tab="{connect:'#wk-522',animation:'fade'}">
        <!--<li class="uk-active"><a href="/">Today</a></li>
        <li class=""><a href="/" class="colr">Tommorow</a></li>
        <li class=""><a href="/" class="colr">Soon</a></li>-->
        
         <div class="uk-button-dropdown drop-down" data-uk-dropdown aria-haspopup="true" aria-expanded="false">
         <div>
                                    <button  class="uk-button maddy"><?php

@$dy=$_GET['dy'];
@$dy1=strlen($dy);

if($dy1==0){

echo "WHEN";

}else{

echo "$dy";

}


?><i class="uk-icon-caret-down maddy-icon"></i></button>


         </div>
                                    <div class="uk-dropdown uk-dropdown-bottom bot-drop" style="top: 30px; left: 0px;">
                                        <ul class="uk-nav uk-nav-dropdown">
                                            <li><a href="events.php?dy=TODAY" class="colr">TODAY</a></li>
                                        <li ><a href="events.php?tom=1&dy=TOMMOROW" class="colr">TOMORROW</a></li>
                                        <li ><a href="events.php?tom=1&dy=WEEK" class="colr">THIS WEEK</a></li>
                                        <li ><a href="events.php?tom=1&dy=WEEKEND" class="colr">THIS WEEKEND</a></li>
                                        <li ><a href="events.php?later=1&dy=LATER" class="colr">LATER</a></li>
                                        </ul>  
                                        </div>
                                </div> 
                                
                              <!--  <select name="forma" onchange="location = this.value;">
     <option value="">when</option>
<option value="events.php">TODAY</option>
<option value="events.php?tom=1">TOMMOROW</option>
<option value="events.php?soon=1">SOON</option>
</select>-->
          
         <div class="uk-button-dropdown drop-down" data-uk-dropdown aria-haspopup="true" aria-expanded="false">
                                    <button class="uk-button maddy"><?php

@$price=$_GET['p'];
@$price1=strlen($price);

if($price1==0){

echo "PRICE";

}else{

echo "$price";

}


?><i class="uk-icon-caret-down maddy-icon"></i></button>


                                    <div class="uk-dropdown uk-dropdown-bottom bot-drop" style="top: 30px; left: 0px;">
                                        <ul class="uk-nav uk-nav-dropdown">
                                        <li><a href="events.php?pid=Free&p=Free" class="colr">FREE</a></li>

                                            <!-- <li ><a href="events.php?pid=1&p=&#8377;" class="colr">&#8377;</a></li> -->
                                            <li><a href="events.php?pid=2&p=&#8377;&#8377;" class="colr">&#8377;&#8377;</a></li>
                                      <li><a href="events.php?pid=3&p=&#8377;&#8377;&#8377;" class="colr">&#8377;&#8377;&#8377;</a></li>
                                             </ul>
                                    </div>
                                </div>
                                
        <div class="uk-button-dropdown drop-down" data-uk-dropdown aria-haspopup="true" aria-expanded="false" style="border-right:1px solid #ddd;">
                                    <button class="uk-button maddy">


        <?php

@$cate=$_GET['cate'];
@$cate1=strlen($cate);

if($cate1==0){

echo "CATEGORY";

}else{

echo "$cate";

}


?><i class="uk-icon-caret-down maddy-icon"></i></button>


                                    
                                    <div class="uk-dropdown uk-dropdown-bottom bot-drop" style="top: 30px; left: 0px;">
                                        <ul class="uk-nav uk-nav-dropdown">
                                                    <?php

                        
                        @$cate=mysqli_query($conn,"SELECT evecate_id  from event");
                         
                      
                       while(@$catefetch=mysqli_fetch_array($cate)){
                        
                             

                             if(strlen($var)==0){

                          $var=$catefetch['evecate_id'];


                             }else{


                              $var=$var.",".$catefetch['evecate_id'];
                             }
                           

                           ?>

                 
                 <?php      }
                
                @$catexp= explode(",",$var);
                 
                 @$arruni=array_unique($catexp);
                 

                 foreach ($arruni as $val) { ?>


                          <li><a href="events.php?cate=<?php echo $val;?>" class="colr"><?php echo $val;?></a></li>

                
                 <?php }
             


                        ?>
                                            
                                            <!-- <li><a href="#"></a></li> -->
                                        </ul>
                                    </div>
                                </div>                                               
                                
        
<li class="uk-tab-responsive uk-active uk-hidden" aria-haspopup="true"><a>Support</a><div class="uk-dropdown uk-dropdown-small"><ul class="uk-nav uk-nav-dropdown"></ul><div></div></div></li></ul>


        </div>

    </div>


<ul id="wk-522" class="uk-switcher uk-margin-top uk-text-left eventgrid" data-uk-check-display="">


    <li class="uk-active">

<div class="uk-grid">
<?php
@$tom=$_GET['tom'];
@$soon=$_GET['later'];
@$cate=$_GET['cate'];
@$prating=$_GET['pid'];

if($tom){

    @$next_date = date('Y-m-d', strtotime($current_date .' +1 day'));
    @$dis1=date(" l, dS F", strtotime(' +1 day'));
  
    @$sel4=mysqli_query($conn,"SELECT * from event e,items i,location loc where e.item_id=i.item_id and i.item_location=loc.loc_id and e.e_published=1 order by e.e_prior +0");
    
    while(@$fet4=mysqli_fetch_array($sel4))
    {
            @$spt1=explode(",",$fet4['e_date']); 
            @$id1= $fet4['e_id'];
            if (in_array($next_date,$spt1))
            {
              if(strlen($fet4['evecate_id']) >0)
              {
                @$tag1=explode(",",$fet4['evecate_id']);
                @$count1=count($tag1);
              }
    @$ki1= array_search($next_date,$spt1);
    @$sptstart1=explode(",",$fet4['e_start']);
?>
                            
                           


    <div class="uk-width-small-1-1 uk-width-medium-1-2 uk-width-large-1-3" style="padding-top:30px;">
        <div class="uk-panel card-width">
          <a href="event_detail.php?id=<?php echo $fet4['e_id']?>&date=<?php echo $dis1 ?>">
    <div class="uk-panel-badge uk-badge badgy1">
    <?php
      @$se9=mysqli_query($conn,"SELECT e_id,offer_title from offers where e_id='$id1'");
      while(@$fe9=mysqli_fetch_array($se9))
      {
        if($fe9['e_id'])
        {
          echo $fe9['offer_title'].'</br>'.'OFF';
      ?>
        <div class="uk-panel-badge uk-badge badgy" ></div>
      <?php 
        }
      }
    ?>
    </div>
  <div class="uk-panel-badge uk-badge badgy_left"><?php echo $fet4['e_type'];?></div>
    <figure class="uk-overlay tm-overlay-uniq" style="background-image:url('https://www.clubgo.in/cgsquad.in/backend/<?php echo $fet4['cover_image']?>');background-size:120% 350px;background-repeat:no-repeat;background-position: 60% 50%;">
      <img src="docs/images/overlay.png" class="uk-overlay-scale" alt="Wooden Chair" width="100%" style="height:250px;opacity:0.4;">
    </figure>
    
    <div class="" style="background: #fff;padding: 10px 20px;">
        <p class="uk-margin-bottom-remove tm-overlay-panel-title txt_anim_a">
          <?php echo $fet4['e_name']?>
        </p>
        <p class="daty uk-margin-bottom-remove tm-overlay-panel-title">
          <span><img src="docs/images/clock1.png" class="clockimg"></span>&nbsp; 
          <?php echo date('h:i a', strtotime($sptstart1[$ki1]));?><?php echo $dis1?>
        </p>
        <p class="center pad_card"><span><img src="docs/img/loc.png" class="locimg1"></span>&nbsp;
          <?php echo $fet4['item_title']?>, <?php echo $fet4['loc_title'].''?>
        </p> 
        <p style="color: rgba(61, 61, 61, 0.81);"><span><img src="docs/img/distance.png" class="distimg"></span> 
        <?php
        @$sel5=mysqli_query($conn,"SELECT  ( 6371 * acos ( cos ( radians($lati) ) * cos( radians( item_lat ) ) * cos( radians( item_lng ) - radians($longi) ) + sin ( radians($lati) ) * sin( radians( item_lat ) ) ) ) AS distance FROM items where item_id='".$fet4['item_id']."'");
        @$fet5=mysqli_fetch_array($sel5);
        echo substr($fet5['distance'], 0,4);
        ?>
        km</p>
<br/>
      <p class="evntbtn" style="float:left;">
      <?php for ($i=0; $i < $count1; $i++) { ?>
        <p class="uk-button tagbtn"><?php echo $tag1[$i]?></p> 
      <?php } ?>
        <div class="bookticky">Book Tickets</div> 
      </p>
    </div>
    </a>

</div></div>
<?php  } } }

else if($soon){

@$soon = date('Y-m-d', strtotime($current_date .' +2 day'));

  
@$sel6=mysqli_query($conn,"SELECT * from event e,items i,location loc where e.item_id=i.item_id and i.item_location=loc.loc_id  and e.e_published=1 order by e.e_prior +0");
                          while(@$fet6=mysqli_fetch_array($sel6)){
                        @$spt2=explode(",",$fet6['e_date']); 

                        @$id= $fet6['e_id'];
                       
                      foreach ($spt2 as $key ) {
                      
                        if ($key >= $soon)
                        
                        {
                       

if(strlen($fet6['evecate_id'])>0){

@$tag2=explode(",",$fet6['evecate_id']);


@$count2=count($tag2);

}
  
  @$ki2= array_search($key,$spt2);
  @$sptstart2=explode(",",$fet6['e_start']);
?>
<div class="uk-width-small-1-1 uk-width-medium-1-2 uk-width-large-1-3" style="padding-top:30px;">
        <div class="uk-panel card-width">
<a href="event_detail.php?id=<?php echo $fet6['e_id']?>&date=<?php echo date(" l, dS F", strtotime( $key)) ?>">
<div class="uk-panel-badge uk-badge badgy1"><?php



@$sel9=mysqli_query($conn,"SELECT e_id,offer_title from offers where e_id='$id'");
                          while(@$fet9=mysqli_fetch_array($sel9)){





if($fet9['e_id']){

echo $fet9['offer_title'].'</br>'.'OFF';

  ?>
 <div class="uk-panel-badge uk-badge badgy" ></div>


<?php 
}}?>


 
</div>

<div class="uk-panel-badge uk-badge badgy_left"><?php echo $fet6['e_type'];?></div>

    <figure class="uk-overlay tm-overlay-uniq" style="background-image:url('https://www.clubgo.in/cgsquad.in/backend/<?php echo $fet6['cover_image']?>');background-size:120% 350px;background-repeat:no-repeat;background-position: 60% 50%;">

       <!-- <img src="https://www.clubgo.in/cgsquad.in/backend/<?php echo $fet6['cover_image']?>" class="uk-overlay-scale brdr_red" alt="Wooden Chair" width="100%" height="1070" style=""> -->
<img src="docs/images/overlay.png" class="uk-overlay-scale" alt="Wooden Chair" width="100%" style="height:250px;" style="opacity:0.4">
 

          </figure>
            <div style="background: #fff;padding: 10px 20px;">

                                <p class="uk-margin-bottom-remove tm-overlay-panel-title txt_anim_a"><?php echo $fet6['e_name']?></p>
                                <p class="daty uk-margin-bottom-remove tm-overlay-panel-title"><span><img src="docs/images/clock1.png" class="clockimg"></span>&nbsp; <?php echo date('h:i a', strtotime($sptstart2[$ki2]));?><?php 



                                      echo date(" l, dS F", strtotime( $key));

                                      ?></p>

                                
                                    <p class="center pad_card"><span><img src="docs/img/loc.png" class="locimg1"></span>&nbsp;<?php echo $fet6['item_title']?>, <?php echo $fet6['loc_title'].''?></p>
                       <p style="color: rgba(61, 61, 61, 0.81);"><span><img src="docs/img/distance.png" class="distimg"></span>  <?php
    @$sel7=mysqli_query($conn,"SELECT  ( 6371 * acos ( cos ( radians($lati) ) * cos( radians( item_lat ) ) * cos( radians( item_lng ) - radians($longi) ) + sin ( radians($lati) ) * sin( radians( item_lat ) ) ) ) AS distance FROM items where item_id='".$fet6['item_id']."' 
 ");
                      @$fet7=mysqli_fetch_array($sel7);
                     
                      echo substr($fet7['distance'], 0,4);

                                ?>


km</p>
<br/>
                                     
<!--
                                    <h5 class="center pad_card"><?php echo date('h:i a', strtotime($sptstart2[$ki2]));?> onwards  <?php 



                                      echo date(" l, dS F", strtotime( $key));

                                      ?> </h5><br>
-->
                                    <p class="evntbtn" style="float:left;">
                                      <?php
                                 for ($i=0; $i < $count2; $i++) {?>

                            <p class="uk-button tagbtn"><?php echo $tag2[$i]?></p> 
                              
                           <?php } ?>
                            <div class="bookticky">Book Tickets</div> </p>
                                   
                                    </div>
        </a>


<!--                                                   <div class="uk-position-cover tm-panel-link icon-plus"  data-index="0" data-uk-modal=""></div></a> -->



</div></div>



                       <?php  } }

                          }


}else if($cate){

function closest($sp2, $findate)
{
    $newDates = array();

    foreach($sp2 as $date)
    {
        $newDates[] = strtotime($date);
    }

  /* echo "<pre>";
    print_r($newDates);
    echo "</pre>";*/

    sort($newDates);
    foreach ($newDates as $a)
    {
        if ($a >= strtotime($findate)) return $a;
    }
    return end($newDates);
}
  @$cudate=date("Y-m-d",time());
@$selcate=mysqli_query($conn,"SELECT * from event e,items i,location loc where e.item_id=i.item_id and i.item_location=loc.loc_id  and e.e_published=1 and e.evecate_id like '%".$cate."%' order by e.e_prior +0");
                          while(@$catefet=mysqli_fetch_array($selcate)){
                        @$sp2=explode(",",$catefet['e_date']);
                         
                        










$values = closest($sp2, date('Y-m-d',time()));
@$pdate= date('Y-m-d',$values);

if($pdate >=$cudate){

//print_r($sp2);
            



                        @$cateeid= $catefet['e_id'];
                       
                      
                       if(strlen($catefet['evecate_id'])>0){

                     @$tag12=explode(",",$catefet['evecate_id']);

                     @$count12=count($tag12);

                       }

                          

                            // @$ta12= array_shift(array_slice($tag12, 0, 1)); 
                            // @$tags12=array_splice($tag12, 0, 1);
                                //print_r($tag);
                            
                           // echo $count;
                               

                                 @$ki12= array_search($pdate,$sp2);
                            
                                     @$sptstart2=explode(",",$catefet['e_start']);
                                    @$amm= date("a",strtotime($sptstart2[$ki12]));
                                  //echo $amm;

                            ?>
                            
                           


    <div class="uk-width-small-1-1 uk-width-medium-1-2 uk-width-large-1-3" style="padding-top:30px;">
        <div class="uk-panel card-width">
<a href="event_detail.php?id=<?php echo $catefet['e_id']?>&date=<?php echo date(" l, dS F", strtotime( $pdate)) ?>">
<div class="uk-panel-badge uk-badge badgy1"><?php



@$sel19=mysqli_query($conn,"SELECT e_id,offer_title from offers where e_id='$cateeid'");
                          while(@$fet19=mysqli_fetch_array($sel19)){





if($fet19['e_id']){

echo $fet19['offer_title'].'</br>'.'OFF';

  ?>
 <div class="uk-panel-badge uk-badge badgy" ></div>


<?php 
}}?>


 
</div>

<div class="uk-panel-badge uk-badge badgy_left"><?php echo $catefet['e_type'];?></div>

    <figure class="uk-overlay tm-overlay-uniq" style="background-image:url('https://www.clubgo.in/cgsquad.in/backend/<?php echo $catefet['cover_image']?>');background-size:120% 350px;background-repeat:no-repeat;background-position: 60% 50%;">

       <!-- <img src="https://www.clubgo.in/cgsquad.in/backend/<?php echo $catefet['cover_image']?>" class="uk-overlay-scale brdr_red" alt="Wooden Chair" width="100%" height="1070" style=""> -->
<img src="docs/images/overlay.png" class="uk-overlay-scale" alt="Wooden Chair" width="100%" style="height:250px;opacity: 0.4;">
 </figure>

<!--         <div class="uk-overlay-panel uk-overlay-bottom"> -->
            <div style="background: #fff;padding: 10px 20px;">

                                <p class="uk-margin-bottom-remove tm-overlay-panel-title txt_anim_a"><?php echo $catefet['e_name']?></p>
                                <p class="daty uk-margin-bottom-remove tm-overlay-panel-title"><span><img src="docs/images/clock1.png" class="clockimg"></span>&nbsp; <?php echo date('h:i a', strtotime($sptstart2[$ki12]));?><?php 



                                      echo date(" l, dS F", strtotime( $pdate));

                                      ?></p>

                              
                                    <p class="center pad_card"><span><img src="docs/img/loc.png" class="locimg1"></span>&nbsp;<?php echo $catefet['item_title']?>, <?php echo $catefet['loc_title'].''?></p>
                        <p style="color: rgba(61, 61, 61, 0.81);"><span><img src="docs/img/distance.png" class="distimg"></span> <?php
    @$sel17=mysqli_query($conn,"SELECT  ( 6371 * acos ( cos ( radians($lati) ) * cos( radians( item_lat ) ) * cos( radians( item_lng ) - radians($longi) ) + sin ( radians($lati) ) * sin( radians( item_lat ) ) ) ) AS distance FROM items where item_id='".$catefet['item_id']."' 
 ");
                      @$fet17=mysqli_fetch_array($sel17);
                     
                      echo substr($fet17['distance'], 0,4);

                                ?>


km</p>
<br/>
                                     
<!--
                                    <h5 class="center pad_card"><?php echo date('h:i a', strtotime($sptstart2[$ki12]));?> onwards  <?php 



                                      echo date(" l, dS F", strtotime( $pdate));

                                      ?> </h5><br>
-->
                                    <p class="evntbtn" style="float:left;">
                                      <?php
                                 for ($i=0; $i < $count12; $i++) {?>

                            <p class="uk-button tagbtn"><?php echo $tag12[$i]?></p> 
                              
                           <?php } ?>
                            <div class="bookticky">Book Tickets</div> </p>
                                   
                                </div>
                                </a>


<!--                                                   <div class="uk-position-cover tm-panel-link icon-plus"  data-index="0" data-uk-modal=""></div></a> -->


</div></div>



                       <?php  

                        }  } 


}else if($prating){

function closest($sp21, $findate1)
{
    $newDates = array();

    foreach($sp21 as $date)
    {
        $newDates[] = strtotime($date);
    }

  /* echo "<pre>";
    print_r($newDates);
    echo "</pre>";*/

    sort($newDates);
    foreach ($newDates as $a)
    {
        if ($a >= strtotime($findate1)) return $a;
    }
    return end($newDates);
}
  @$cudate1=date("Y-m-d",time());

   if($prating =='Free'){

    @$selpri=mysqli_query($conn,"SELECT * from event e,items i,location loc where  e.item_id=i.item_id and i.item_location=loc.loc_id  and e.e_published=1 and i.item_rating='0' order by e.e_prior +0");
  }
  else{
@$selpri=mysqli_query($conn,"SELECT * from event e,items i,location loc where  e.item_id=i.item_id and i.item_location=loc.loc_id  and e.e_published=1 and i.item_rating='$prating' order by e.e_prior +0");
}
                          while(@$prifet=mysqli_fetch_array($selpri)){
                        @$sp21=explode(",",$prifet['e_date']); 

                      
                       
                      
  $values1 = closest($sp21, date('Y-m-d',time()));
@$pdate1= date('Y-m-d',$values1);
//echo $pdate1;

if($pdate1 >=$cudate1){                     
                           

                            @$prieid= $prifet['e_id'];


if(strlen($prifet['evecate_id'])>0){


@$tag13=explode(",",$prifet['evecate_id']);

@$count13=count($tag13);

}



                          

                            // @$ta12= array_shift(array_slice($tag12, 0, 1)); 
                            // @$tags12=array_splice($tag12, 0, 1);
                                //print_r($tag);
                            
                           // echo $count;
                               

                                 @$ki21= array_search($pdate1,$sp21);
                            
                                     @$sptstart21=explode(",",$prifet['e_start']);
                                   // @$pmm= date("a",strtotime($sptstart21[$ki21]));
                                              

                            ?>
                            
                           


    <div class="uk-width-small-1-1 uk-width-medium-1-2 uk-width-large-1-3" style="padding-top:30px;">
        <div class="uk-panel card-width">
<a href="event_detail.php?id=<?php echo $prifet['e_id']?>&date=<?php echo date(" l, dS F", strtotime( $pdate1)) ?>">
<div class="uk-panel-badge uk-badge badgy1"><?php



@$sel19=mysqli_query($conn,"SELECT e_id,offer_title from offers where e_id='$prieid'");
                          while(@$fet19=mysqli_fetch_array($sel19)){





if($fet19['e_id']){

echo $fet19['offer_title'].'</br>'.'OFF';

  ?>
 <div class="uk-panel-badge uk-badge badgy" ></div>


<?php 
}}?>


 
</div>

<div class="uk-panel-badge uk-badge badgy_left"><?php echo $prifet['e_type'];?></div>

    <figure class="uk-overlay tm-overlay-uniq" style="background-image:url('https://www.clubgo.in/cgsquad.in/backend/<?php echo $prifet['cover_image']?>');background-size:120% 350px;background-repeat:no-repeat;background-position: 60% 50%;">

      <!--  <img src="https://www.clubgo.in/cgsquad.in/backend/<?php echo $prifet['cover_image']?>" class="uk-overlay-scale brdr_red" alt="Wooden Chair" width="100%" height="1070" style=""> -->
        <img src="docs/images/overlay.png" class="uk-overlay-scale" alt="Wooden Chair" width="100%" style="height:250px;opacity: 0.4;">
 </figure>
<!--         <div class="uk-overlay-panel uk-overlay-bottom"> -->
            <div style="background: #fff;padding: 10px 20px;">

                                <p class="uk-margin-bottom-remove tm-overlay-panel-title txt_anim_a"><?php echo $prifet['e_name']?></p>
                                <p class="daty uk-margin-bottom-remove tm-overlay-panel-title"><span><img src="docs/images/clock1.png" class="clockimg"></span>&nbsp; <?php echo date('h:i a', strtotime($sptstart21[$ki21]));?><?php 



                                      echo date(" l, dS F", strtotime( $pdate1));

                                      ?></p>

                             
                                    <p class="center pad_card"><span><img src="docs/img/loc.png" class="locimg1"></span>&nbsp;<?php echo $prifet['item_title']?>, <?php echo $prifet['loc_title'].''?></p>
                                    <p style="color: rgba(61, 61, 61, 0.81);"><span><img src="docs/img/distance.png" class="distimg"></span>
                         <?php
    @$selp17=mysqli_query($conn,"SELECT  ( 6371 * acos ( cos ( radians($lati) ) * cos( radians( item_lat ) ) * cos( radians( item_lng ) - radians($longi) ) + sin ( radians($lati) ) * sin( radians( item_lat ) ) ) ) AS distance FROM items where item_id='".$prifet['item_id']."' 
 ");
                      @$fetp17=mysqli_fetch_array($selp17);
                     
                      echo substr($fetp17['distance'], 0,4);

                                ?>


km</p>
<br/>
                                     
<!--
                                    <h5 class="center pad_card"><?php echo date('h:i a', strtotime($sptstart21[$ki21]));?> onwards <?php 



                                      echo date(" l, dS F", strtotime( $pdate1));

                                      ?> </h5><br>
-->
                                    <p class="evntbtn" style="float:left;">
                                      <?php
                                 for ($i=0; $i < $count13; $i++) {?>

                            <p class="uk-button tagbtn"><?php echo $tag13[$i]?></p> 
                              
                           <?php } ?>
                            
                                 <div class="bookticky">Book Tickets</div> </p>  
                                    </div>
                             

           </a>

<!--                                                   <div class="uk-position-cover tm-panel-link icon-plus"  data-index="0" data-uk-modal=""></div></a> -->

    

</div></div>



                <?php 

                        }  }


}else{
       

  date_default_timezone_set("Asia/Kolkata");     
  @$current_date= date("Y-m-d",time());
  @$tim= date("H:i",time());
  //@$dis= date(" l, dS F",time());


//echo $current_date;
//echo $next_date;
@$sel3=mysqli_query($conn,"SELECT * from event e,items i,location loc where e.item_id=i.item_id  and i.item_location=loc.loc_id and e.e_published=1 order by e.e_prior +0");
                          while(@$fet3=mysqli_fetch_array($sel3)){
                         @$spt=explode(",",$fet3['e_date']);
                        @$edate=explode(",",$fet3['e_enddate']); 
                        @$etime=explode(",",$fet3['e_end']);




                                           @$taglen=strlen($fet3['evecate_id']);

                                 if($taglen!==0){


                                    @$tag=explode(",",$fet3['evecate_id']);


                                     @$counttt=count($tag);
                              }else{

                                @$counttt=0;


                              }
                                 


                            // @$ta= array_shift(array_slice($tag, 0, 1)); 
                            // @$tags=array_splice($tag, 0, 1);
                                //print_r($tag);


                            


                           
                                     @$sptstart=explode(",",$fet3['e_start']); 
                              

                                @$id2= $fet3['e_id'];

                      for($i=0;$i<count($spt);$i++)
                                          {  
                          

                             

                          


                        if($spt[$i]==$edate[$i])
                          {



                        if ($current_date==$edate[$i] and $tim<=$etime[$i]) 
                        
                              {                    

                        
                      
                               
                                  

                            ?>
                            
                           


    <div class="uk-width-small-1-1 uk-width-medium-1-2 uk-width-large-1-3" style="padding-top:30px;">
        <div class="uk-panel card-width">
          <a href="event_detail.php?id=<?php echo $fet3['e_id']?>&date=<?php echo date(" l, dS F", strtotime( $spt[$i])) ?>">
<div class="uk-panel-badge uk-badge badgy1"><?php



@$st9=mysqli_query($conn,"SELECT e_id,offer_title from offers where e_id='$id2'");
                          while(@$ft9=mysqli_fetch_array($st9)){





if($ft9['e_id']){

echo $ft9['offer_title'].'</br>'.'OFF';

  ?>
 <div class="uk-panel-badge uk-badge badgy" ></div>


<?php 
}}?>


 
</div>
<div class="uk-panel-badge uk-badge badgy_left"><?php echo $fet3['e_type'];?></div>

    <figure class="uk-overlay tm-overlay-uniq" style="background-image:url('https://www.clubgo.in/cgsquad.in/backend/<?php echo $fet3['cover_image']?>');background-size:120% 350px;background-repeat:no-repeat;background-position: 60% 50%;">

       <!-- <img src="https://www.clubgo.in/cgsquad.in/backend/<?php echo $fet3['cover_image']?>" class="uk-overlay-scale brdr_red" alt="Wooden Chair" width="100%" height="1070" style=""> -->
<img src="docs/images/overlay.png" class="uk-overlay-scale" alt="Wooden Chair" width="100%" style="height:250px; opacity: 0.4;">
    </figure>
        
            <div style="background: #fff;padding: 10px 20px;">

                                <p class="uk-margin-bottom-remove tm-overlay-panel-title txt_anim_a"><?php echo $fet3['e_name']?></p>
                                <p class="daty uk-margin-bottom-remove tm-overlay-panel-title"><span><img src="docs/images/clock1.png" class="clockimg"></span>&nbsp; <?php echo date('h:i a', strtotime($sptstart[$i])); ?><?php echo date(" l, dS F", strtotime( $spt[$i])) ?></p>

                                
                                    <p class="center pad_card"><span><img src="docs/img/loc.png" class="locimg1"></span>&nbsp;<?php echo $fet3['item_title']?>, <?php echo $fet3['loc_title'].''?></p>
                                    
                                    <p style="color: rgba(61, 61, 61, 0.81);"><span><img src="docs/img/distance.png" class="distimg"></span>
                         <?php
   /* @$sel2=mysqli_query($conn,"SELECT  ( 6371 * acos ( cos ( radians($lati) ) * cos( radians( item_lat ) ) * cos( radians( item_lng ) - radians($longi) ) + sin ( radians($lati) ) * sin( radians( item_lat ) ) ) ) AS distance FROM items where item_id='".$fet3['item_id']."' 
 ");
                      @$fet2=mysqli_fetch_array($sel2);
                     
                      echo substr($fet2['distance'], 0,4);*/

$details = 'https://maps.googleapis.com/maps/api/distancematrix/json?origins='.$lati.','.$longi.'&destinations='.$fet3['loc_lat'].','.$fet3['loc_lng'].'&mode=driving&sensor=false';




    $json = file_get_contents($details);

    $details = json_decode($json, TRUE);

    //echo "<pre>"; print_r($details); echo "</pre>";

       echo $dist = $details['rows'][0]['elements'][0]['distance']['text'];
//echo substr($dist, 0,4);
                      
                                ?>


</p>
<br/>

                                     
<!--                                     <h5 class="center pad_card"><?php echo date('h:i a', strtotime($sptstart[$i])); ?> onwards <?php echo date(" l, dS F", strtotime( $spt[$i])) ?> </h5><br> -->
                                    <p class="evntbtn" style="float:left;">
                                      <?php
                                 for ($e=0; $e < $counttt; $e++) {?>

                            <p class="uk-button tagbtn"><?php echo $tag[$e]?></p> 
                              
                           <?php } ?>
                            
                                     <!-- <p class="uk-button">Nightlife</p> -->
<div class="bookticky">Book Tickets</div></p>
                                </div>

          </a>

<!--                                                   <div class="uk-position-cover tm-panel-link icon-plus"  data-index="0" data-uk-modal=""></div></a> -->


</div></div>



                       <?php  }

                          } else{

if ($current_date==$spt[$i])
                        
                              {

                               // if(strlen($fet3['e_tag'])>0){

                               //      @$tag=explode(",",$fet3['e_tag']);


                               //      @$count=count($tag);

                               //    }
                      
                                 ?>




    <div class="uk-width-small-1-1 uk-width-medium-1-2 uk-width-large-1-3" style="padding-top:30px;">
        <div class="uk-panel card-width">
          <a href="event_detail.php?id=<?php echo $fet3['e_id']?>&date=<?php echo date(" l, dS F", strtotime( $spt[$i])) ?>">
<div class="uk-panel-badge uk-badge badgy1"><?php



@$st9=mysqli_query($conn,"SELECT e_id,offer_title from offers where e_id='$id2'");
                          while(@$ft9=mysqli_fetch_array($st9)){





if($ft9['e_id']){

echo $ft9['offer_title'].'</br>'.'OFF';

  ?>
 <div class="uk-panel-badge uk-badge badgy" ></div>


<?php 
}}?>


 
</div>
<div class="uk-panel-badge uk-badge badgy_left"><?php echo $fet3['e_type'];?></div>

    <figure class="uk-overlay tm-overlay-uniq" style="background-image:url('https://www.clubgo.in/cgsquad.in/backend/<?php echo $fet3['cover_image']?>');background-size:120% 350px;background-repeat:no-repeat;background-position: 60% 50%;">

       <!-- <img src="https://www.clubgo.in/cgsquad.in/backend/<?php echo $fet3['cover_image']?>" class="uk-overlay-scale brdr_red" alt="Wooden Chair" width="100%" height="1070" style=""> -->
<img src="docs/images/overlay.png" class="uk-overlay-scale" alt="Wooden Chair" width="100%" style="height:250px;opacity: 0.4;">
    </figure>
<!--         <div class="uk-overlay-panel uk-overlay-bottom"> -->
            <div style="background: #fff;padding: 10px 20px;">

                                <p class="uk-margin-bottom-remove tm-overlay-panel-title txt_anim_a"><?php echo $fet3['e_name']?></p>
                                <p class="daty uk-margin-bottom-remove tm-overlay-panel-title"><span><img src="docs/images/clock1.png" class="clockimg"></span>&nbsp; <?php echo date('h:i a', strtotime($sptstart[$i])); ?><?php echo date(" l, dS F", strtotime( $spt[$i])) ?></p>

                               
                                    <p class="center pad_card"><span><img src="docs/img/loc.png" class="locimg1"></span>&nbsp;<?php echo $fet3['item_title']?>, <?php echo $fet3['loc_title'].''?></p>
                                     <p style="color: rgba(61, 61, 61, 0.81);"><span><img src="docs/img/distance.png" class="distimg"></span>
                         <?php
   /* @$sel2=mysqli_query($conn,"SELECT  ( 6371 * acos ( cos ( radians($lati) ) * cos( radians( item_lat ) ) * cos( radians( item_lng ) - radians($longi) ) + sin ( radians($lati) ) * sin( radians( item_lat ) ) ) ) AS distance FROM items where item_id='".$fet3['item_id']."' 
 ");
                      @$fet2=mysqli_fetch_array($sel2);
                     
                      echo substr($fet2['distance'], 0,4);*/

$details = 'https://maps.googleapis.com/maps/api/distancematrix/json?origins='.$lati.','.$longi.'&destinations='.$fet3['loc_lat'].','.$fet3['loc_lng'].'&mode=driving&sensor=false';




    $json = file_get_contents($details);

    $details = json_decode($json, TRUE);

    //echo "<pre>"; print_r($details); echo "</pre>";

       echo $dist = $details['rows'][0]['elements'][0]['distance']['text'];


                                ?>


</p>
<br/>

                                     
<!--                                     <h5 class="center pad_card"><?php echo date('h:i a', strtotime($sptstart[$i])); ?> onwards  <?php echo date(" l, dS F", strtotime( $spt[$i])) ?> </h5><br> -->
                                    <p class="evntbtn" style="float:left;">
                                      <?php
                                 for ($e=0; $e < $counttt; $e++) {?>

                            <p class="uk-button tagbtn"><?php echo $tag[$e]?></p> 
                              
                           <?php } ?>
                            
                                     <!-- <p class="uk-button">Nightlife</p> -->
                            <div class="bookticky">Book Tickets</div>
                                    </p>
                                </div>

          </a>



</div></div>



                       <?php 





}



    if ($current_date==$edate[$i] and $tim<=$etime[$i])
                        
                              {

                                // if(strlen($fet3['e_tag'])>0){

                                //     @$tag=explode(",",$fet3['e_tag']);

                                //     @$count=count($tag);

                                //   }
                      
                                 ?>


<div class="uk-width-small-1-1 uk-width-medium-1-2 uk-width-large-1-3" style="padding-top:30px;">
        <div class="uk-panel card-width">
           <a href="event_detail.php?id=<?php echo $fet3['e_id']?>&date=<?php echo date(" l, dS F", strtotime( $spt[$i])) ?>">
<div class="uk-panel-badge uk-badge badgy1"><?php



@$st9=mysqli_query($conn,"SELECT e_id,offer_title from offers where e_id='$id2'");
                          while(@$ft9=mysqli_fetch_array($st9)){





if($ft9['e_id']){

echo $ft9['offer_title'].'</br>'.'OFF';

  ?>
 <div class="uk-panel-badge uk-badge badgy" ></div>


<?php 
}}?>


 
</div>
<div class="uk-panel-badge uk-badge badgy_left"><?php echo $fet3['e_type'];?></div>

    <figure class="uk-overlay tm-overlay-uniq" style="background-image:url('https://www.clubgo.in/cgsquad.in/backend/<?php echo $fet3['cover_image']?>');background-size:120% 350px;background-repeat:no-repeat;background-position: 60% 50%;">

       <!-- <img src="https://www.clubgo.in/cgsquad.in/backend/<?php echo $fet3['cover_image']?>" class="uk-overlay-scale brdr_red" alt="Wooden Chair" width="100%" height="1070" style=""> -->
<img src="docs/images/overlay.png" class="uk-overlay-scale" alt="Wooden Chair" width="100%" style="height:250px;opacity: 0.4;">
    </figure>
       
            <div style="background: #fff;padding: 10px 20px;">

                                <p class="uk-margin-bottom-remove tm-overlay-panel-title txt_anim_a"><?php echo $fet3['e_name']?></p>
                                <p class="daty uk-margin-bottom-remove tm-overlay-panel-title"><span><img src="docs/images/clock1.png" class="clockimg"></span>&nbsp; <?php echo date('h:i a', strtotime($sptstart[$i])); ?><?php echo date(" l, dS F", strtotime( $spt[$i])) ?></p>

                                    <p class="center pad_card"><span><img src="docs/img/loc.png" class="locimg1"></span>&nbsp;<?php echo $fet3['item_title']?>, <?php echo $fet3['loc_title'].''?></p>
                                    
                                    <p style="color: rgba(61, 61, 61, 0.81);"><span><img src="docs/img/distance.png" class="distimg"></span> 
                         <?php
    /*@$sel2=mysqli_query($conn,"SELECT  ( 6371 * acos ( cos ( radians($lati) ) * cos( radians( item_lat ) ) * cos( radians( item_lng ) - radians($longi) ) + sin ( radians($lati) ) * sin( radians( item_lat ) ) ) ) AS distance FROM items where item_id='".$fet3['item_id']."' 
 ");
                      @$fet2=mysqli_fetch_array($sel2);
                     
                      echo substr($fet2['distance'], 0,4);*/

$details = 'http://maps.googleapis.com/maps/api/distancematrix/json?origins='.$lati.','.$longi.'&destinations='.$fet3['loc_lat'].','.$fet3['loc_lng'].'&mode=driving&sensor=false';




    $json = file_get_contents($details);

    $details = json_decode($json, TRUE);

    //echo "<pre>"; print_r($details); echo "</pre>";

       echo $dist = $details['rows'][0]['elements'][0]['distance']['text'];

                                ?>


km</p>
<br/>
                                     
<!--                                     <h5 class="center pad_card"><?php echo date('h:i a', strtotime($sptstart[$i])); ?> onwards <?php echo date(" l, dS F", strtotime( $spt[$i])) ?> </h5><br> -->
                                    <p class="evntbtn" style="float:left;">
                                      <?php
                                 for ($e=0; $e < $counttt; $e++) {?>

                            <p class="uk-button tagbtn"><?php echo $tag[$e]?></p> 
                              
                           <?php } ?>
                            
                                     <!-- <p class="uk-button">Nightlife</p> -->
<div class="bookticky">Book Tickets</div>
</p>
                                </div>

          
                                                 </a>



</div></div>



                       <?php 




                              }

 if ($current_date>$spt[$i]  and $current_date<$edate[$i])
                        
                              {

                               // if(strlen($fet3['e_tag'])>0){

                               //      @$tag=explode(",",$fet3['e_tag']);

                               //       @$count=count($tag);

                               //    }
                      
                               ?>


<div class="uk-width-small-1-1 uk-width-medium-1-2 uk-width-large-1-3" style="padding-top:30px;">
        <div class="uk-panel card-width">
          <a href="event_detail.php?id=<?php echo $fet3['e_id']?>&date=<?php echo date(" l, dS F", strtotime( $spt[$i])) ?>">
<div class="uk-panel-badge uk-badge badgy1"><?php



@$st9=mysqli_query($conn,"SELECT e_id,offer_title from offers where e_id='$id2'");
                          while(@$ft9=mysqli_fetch_array($st9)){





if($ft9['e_id']){

echo $ft9['offer_title'].'</br>'.'OFF';

  ?>
 <div class="uk-panel-badge uk-badge badgy" ></div>


<?php 
}}?>


 
</div>
<div class="uk-panel-badge uk-badge badgy_left"><?php echo $fet3['e_type'];?></div>

    <figure class="uk-overlay tm-overlay-uniq" style="background-image:url('https://www.clubgo.in/cgsquad.in/backend/<?php echo $fet3['cover_image']?>');background-size:120% 350px;background-repeat:no-repeat;background-position: 60% 50%;">

       <!-- <img src="https://www.clubgo.in/cgsquad.in/backend/<?php echo $fet3['cover_image']?>" class="uk-overlay-scale brdr_red" alt="Wooden Chair" width="100%" height="1070" style=""> -->
<img src="docs/images/overlay.png" class="uk-overlay-scale" alt="Wooden Chair" width="100%" style="height:250px;opacity: 0.4;">
 
    </figure>  
            <div style="background: #fff;padding: 10px 20px;">

                                <p class="uk-margin-bottom-remove tm-overlay-panel-title txt_anim_a"><?php echo $fet3['e_name']?></p>
                                <p class="daty uk-margin-bottom-remove tm-overlay-panel-title"><span><img src="docs/images/clock1.png" class="clockimg"></span>&nbsp; <?php echo date('h:i a', strtotime($sptstart[$i])); ?><?php echo date(" l, dS F", strtotime( $spt[$i])) ?></p>

                               
                                    <p class="center pad_card"><span><img src="docs/img/loc.png" class="locimg1"></span>&nbsp;<?php echo $fet3['item_title']?>, <?php echo $fet3['loc_title'].''?></p>
                         <p style="color: rgba(61, 61, 61, 0.81);"><span><img src="docs/img/distance.png" class="distimg"></span>
                           <?php
    /*@$sel2=mysqli_query($conn,"SELECT  ( 6371 * acos ( cos ( radians($lati) ) * cos( radians( item_lat ) ) * cos( radians( item_lng ) - radians($longi) ) + sin ( radians($lati) ) * sin( radians( item_lat ) ) ) ) AS distance FROM items where item_id='".$fet3['item_id']."' 
 ");
                      @$fet2=mysqli_fetch_array($sel2);
                     
                      echo substr($fet2['distance'], 0,4);*/

$details = 'https://maps.googleapis.com/maps/api/distancematrix/json?origins='.$lati.','.$longi.'&destinations='.$fet3['loc_lat'].','.$fet3['loc_lng'].'&mode=driving&sensor=false';




    $json = file_get_contents($details);

    $details = json_decode($json, TRUE);

    //echo "<pre>"; print_r($details); echo "</pre>";

       echo $dist = $details['rows'][0]['elements'][0]['distance']['text'];

                                ?>


km</p><br/>

                                     
<!--                                     <h5 class="center pad_card"><?php echo date('h:i a', strtotime($sptstart[$i])); ?> onwards  <?php echo date(" l, dS F", strtotime( $spt[$i])) ?> </h5><br> -->
                                    <p class="evntbtn" style="float:left;">
                                      <?php
                                 for ($e=0; $e < $counttt; $e++) {?>

                            <p class="uk-button tagbtn"><?php echo $tag[$e]?></p> 
                              
                           <?php } ?>
                            
                                     <!-- <p class="uk-button">Nightlife</p> -->
                                <div class="bookticky">Book Tickets</div>
                                     </p>
                                </div>

</a>



</div></div>



                       <?php 




                              }


                           } } } }


?>






<!-- <div class="uk-width-1-3"><div class="uk-panel uk-overlay-hover uk-text-center uk-scrollspy-init-inview uk-scrollspy-inview uk-animation-fade">

<div class="uk-panel-badge uk-badge badgy"></div>
<div class="uk-panel-badge uk-badge badgy1">20% off</div>
 <div class="uk-panel-badge uk-badge badgy_left">Trending</div>
    <figure class="uk-overlay tm-overlay-uniq ">

        <img src="http://52.74.78.106/blog/wp-content/uploads/2015/07/Third-Eye-nightclub-.jpg" class="uk-overlay-scale brdr_red" alt="Wooden Chair" width="1400" height="1070" style="">
        <div class="uk-overlay-panel uk-overlay-bottom">
            <div>

                                <h3 class="uk-h2 uk-margin-bottom-remove tm-overlay-panel-title txt_anim_a">Hari and Sukhmani Live</h3>

                                <div class="tm-panel-caption txt_anim_b">
                                    <h4 class="center pad_card">Strike, Gurgaon 4km</h4>
                                    <h5 class="center pad_card">8:30pm onwards Tuesday, 15th March</h5><br>
                                    <div class="" style="float:left;">
                                     <p class="uk-button">Nightlife</p>
                                     <p class="uk-button">Nightlife</p>
                                    </div>
                                </div>

            </div>
        </div>

                                                    <a href="event_detail.html"><div class="uk-position-cover tm-panel-link icon-plus"  data-index="0" data-uk-modal=""></div></a>

    </figure>

</div></div>

<div class="uk-width-1-3"><div class="uk-panel uk-overlay-hover uk-text-center uk-scrollspy-init-inview uk-scrollspy-inview uk-animation-fade">
<div class="uk-panel-badge uk-badge badgy"></div>
<div class="uk-panel-badge uk-badge badgy1">20% off</div>
<div class="uk-panel-badge uk-badge badgy_left">Trending</div>

    <figure class="uk-overlay tm-overlay-uniq ">

        <img src="http://images.locanto.in/1096382497/Bali-Nightlife-Tour-Packages_1.jpg" class="uk-overlay-scale brdr_red" alt="Wooden Chair" width="1400" height="1070" style="">

        <div class="uk-overlay-panel uk-overlay-bottom">
            <div>

                                <h3 class="uk-h2 uk-margin-bottom-remove tm-overlay-panel-title txt_anim_a">Hari and Sukhmani Live</h3>

                                <div class="tm-panel-caption txt_anim_b">
                                    <h4 class="center pad_card">Strike, Gurgaon 4km</h4>
                                    <h5 class="center pad_card">8:30pm onwards Tuesday, 15th March</h5><br>
                                    <div class="" style="float:left;">
                                     <p class="uk-button">Nightlife</p>
                                     <p class="uk-button">Nightlife</p>
                                    </div>
                                </div>

            </div>
        </div>

                                                   <a href="event_detail.html"><div class="uk-position-cover tm-panel-link icon-plus"  data-index="0" data-uk-modal=""></div></a>

    </figure>

</div></div>

</div>


<div class="uk-grid">
    <div class="uk-width-1-3">
        <div class="uk-panel uk-overlay-hover uk-text-center uk-scrollspy-init-inview uk-scrollspy-inview uk-animation-fade">
<div class="uk-panel-badge uk-badge badgy"></div>
<div class="uk-panel-badge uk-badge badgy1">20% off</div>
<div class="uk-panel-badge uk-badge badgy_left">Trending</div>

    <figure class="uk-overlay tm-overlay-uniq ">

        <img src="http://timesofindia.indiatimes.com/photo/49830598.cms" class="uk-overlay-scale brdr_red" alt="Wooden Chair" width="1400" height="1070" style="">
        <div class="uk-overlay-panel uk-overlay-bottom">
            <div>

                                <h3 class="uk-h2 uk-margin-bottom-remove tm-overlay-panel-title txt_anim_a">Hari and Sukhmani Live</h3>

                                <div class="tm-panel-caption txt_anim_b">
                                    <h4 class="center pad_card">Strike, Gurgaon 4km</h4>
                                    <h5 class="center pad_card">8:30pm onwards Tuesday, 15th March</h5><br>
                                    <div class="" style="float:left;">
                                     <p class="uk-button">Nightlife</p>
                                     <p class="uk-button">Nightlife</p>
                                    </div>
                                </div>

            </div>
        </div>

                                                   <a href="event_detail.html"><div class="uk-position-cover tm-panel-link icon-plus"  data-index="0" data-uk-modal=""></div></a>

    </figure>

</div></div>



<div class="uk-width-1-3"><div class="uk-panel uk-overlay-hover uk-text-center uk-scrollspy-init-inview uk-scrollspy-inview uk-animation-fade">
<div class="uk-panel-badge uk-badge badgy"></div>
<div class="uk-panel-badge uk-badge badgy1">20% off</div>
<div class="uk-panel-badge uk-badge badgy_left">Trending</div>

    <figure class="uk-overlay tm-overlay-uniq ">

        <img src="http://forbesindia.com/blog/wp-content/uploads/2013/08/panaea.jpg" class="uk-overlay-scale brdr_red" alt="Wooden Chair" width="1400" height="1070" style="">
        <div class="uk-overlay-panel uk-overlay-bottom">
            <div>

                                <h3 class="uk-h2 uk-margin-bottom-remove tm-overlay-panel-title txt_anim_a">Hari and Sukhmani Live</h3>

                                <div class="tm-panel-caption txt_anim_b">
                                    <h4 class="center pad_card">Strike, Gurgaon 4km</h4>
                                    <h5 class="center pad_card">8:30pm onwards Tuesday, 15th March</h5><br>
                                    <div class="" style="float:left;">
                                     <p class="uk-button">Nightlife</p>
                                     <p class="uk-button">Nightlife</p>
                                    </div>
                                </div>

            </div>
        </div>

                                                    <a href="event_detail.html"><div class="uk-position-cover tm-panel-link icon-plus"  data-index="0" data-uk-modal=""></div></a>

    </figure>

</div></div>

<div class="uk-width-1-3"><div class="uk-panel uk-overlay-hover uk-text-center uk-scrollspy-init-inview uk-scrollspy-inview uk-animation-fade">
<div class="uk-panel-badge uk-badge badgy"></div>
<div class="uk-panel-badge uk-badge badgy1">20% off</div>
<div class="uk-panel-badge uk-badge badgy_left">Trending</div>

    <figure class="uk-overlay tm-overlay-uniq ">

        <img src="http://timesofindia.indiatimes.com/photo/50297508.cms" class="uk-overlay-scale brdr_red" alt="Wooden Chair" width="1400" height="1070" style="">
        <div class="uk-overlay-panel uk-overlay-bottom">
            <div>

                                <h3 class="uk-h2 uk-margin-bottom-remove tm-overlay-panel-title txt_anim_a">Hari and Sukhmani Live</h3>
                                <div class="tm-panel-caption txt_anim_b">
                                    <h4 class="center pad_card">Strike, Gurgaon 4km</h4>
                                    <h5 class="center pad_card">8:30pm onwards Tuesday, 15th March</h5><br>
                                    <div class="" style="float:left;">
                                     <p class="uk-button">Nightlife</p>
                                     <p class="uk-button">Nightlife</p>
                                    </div>
                                </div>

            </div>
        </div>

                                                    <a href="event_detail.html"><div class="uk-position-cover tm-panel-link icon-plus"  data-index="0" data-uk-modal=""></div></a>

    </figure>

</div></div> -->

</div>

 </li>



    <li class="" >

<div class="uk-grid">



<?php




?>















<!--     <div class="uk-width-1-3">
        <div class="uk-panel uk-overlay-hover uk-text-center uk-scrollspy-init-inview uk-scrollspy-inview uk-animation-fade">
<div class="uk-panel-badge uk-badge badgy"></div>
<div class="uk-panel-badge uk-badge badgy1">20% off</div>
<div class="uk-panel-badge uk-badge badgy_left">Trending</div>

    <figure class="uk-overlay tm-overlay-uniq ">

        <img src="http://timesofindia.indiatimes.com/photo/50809064.cms" class="uk-overlay-scale brdr_red" alt="Wooden Chair" width="1400" height="1070" style="">
        <div class="uk-overlay-panel uk-overlay-bottom">
            <div>

                                <h3 class="uk-h2 uk-margin-bottom-remove tm-overlay-panel-title txt_anim_a">Hari and Sukhmani Live</h3>

                                <div class="tm-panel-caption txt_anim_b">
                                    <h4 class="center pad_card">Strike, Gurgaon 4km</h4>
                                    <h5 class="center pad_card">8:30pm onwards Tuesday, 15th March</h5><br>
                                    <div class="" style="float:left;">
                                     <p class="uk-button">Nightlife</p>
                                     <p class="uk-button">Nightlife</p>
                                    </div>
                                </div>

            </div>
        </div>

                                                  <a href="event_detail.html"><div class="uk-position-cover tm-panel-link icon-plus"  data-index="0" data-uk-modal=""></div></a>

    </figure>

</div></div>



<div class="uk-width-1-3"><div class="uk-panel uk-overlay-hover uk-text-center uk-scrollspy-init-inview uk-scrollspy-inview uk-animation-fade">

<div class="uk-panel-badge uk-badge badgy"></div>
<div class="uk-panel-badge uk-badge badgy1">20% off</div>
 <div class="uk-panel-badge uk-badge badgy_left">Trending</div>
    <figure class="uk-overlay tm-overlay-uniq ">

        <img src="http://52.74.78.106/blog/wp-content/uploads/2015/07/Third-Eye-nightclub-.jpg" class="uk-overlay-scale brdr_red" alt="Wooden Chair" width="1400" height="1070" style="">
        <div class="uk-overlay-panel uk-overlay-bottom">
            <div>

                                <h3 class="uk-h2 uk-margin-bottom-remove tm-overlay-panel-title txt_anim_a">Hari and Sukhmani Live</h3>

                                <div class="tm-panel-caption txt_anim_b">
                                    <h4 class="center pad_card">Strike, Gurgaon 4km</h4>
                                    <h5 class="center pad_card">8:30pm onwards Tuesday, 15th March</h5><br>
                                    <div class="" style="float:left;">
                                     <p class="uk-button">Nightlife</p>
                                     <p class="uk-button">Nightlife</p>
                                    </div>
                                </div>

            </div>
        </div>

                                                    <a href="event_detail.html"><div class="uk-position-cover tm-panel-link icon-plus"  data-index="0" data-uk-modal=""></div></a>

    </figure>

</div></div>

<div class="uk-width-1-3"><div class="uk-panel uk-overlay-hover uk-text-center uk-scrollspy-init-inview uk-scrollspy-inview uk-animation-fade">
<div class="uk-panel-badge uk-badge badgy"></div>
<div class="uk-panel-badge uk-badge badgy1">20% off</div>
<div class="uk-panel-badge uk-badge badgy_left">Trending</div>

    <figure class="uk-overlay tm-overlay-uniq ">

        <img src="http://images.locanto.in/1096382497/Bali-Nightlife-Tour-Packages_1.jpg" class="uk-overlay-scale brdr_red" alt="Wooden Chair" width="1400" height="1070" style="">
        <div class="uk-overlay-panel uk-overlay-bottom">
            <div>

                                <h3 class="uk-h2 uk-margin-bottom-remove tm-overlay-panel-title txt_anim_a">Hari and Sukhmani Live</h3>

                                <div class="tm-panel-caption txt_anim_b">
                                    <h4 class="center pad_card">Strike, Gurgaon 4km</h4>
                                    <h5 class="center pad_card">8:30pm onwards Tuesday, 15th March</h5><br>
                                    <div class="" style="float:left;">
                                     <p class="uk-button">Nightlife</p>
                                     <p class="uk-button">Nightlife</p>
                                    </div>
                                </div>

            </div>
        </div>

                                                   <a href="event_detail.html"><div class="uk-position-cover tm-panel-link icon-plus"  data-index="0" data-uk-modal=""></div></a>

    </figure>

</div></div>

</div>


<div class="uk-grid">
    <div class="uk-width-1-3">
        <div class="uk-panel uk-overlay-hover uk-text-center uk-scrollspy-init-inview uk-scrollspy-inview uk-animation-fade">
<div class="uk-panel-badge uk-badge badgy"></div>
<div class="uk-panel-badge uk-badge badgy1">20% off</div>
<div class="uk-panel-badge uk-badge badgy_left">Trending</div>

    <figure class="uk-overlay tm-overlay-uniq ">

        <img src="http://timesofindia.indiatimes.com/photo/49830598.cms" class="uk-overlay-scale brdr_red" alt="Wooden Chair" width="1400" height="1070" style="">
        <div class="uk-overlay-panel uk-overlay-bottom">
            <div>

                                <h3 class="uk-h2 uk-margin-bottom-remove tm-overlay-panel-title txt_anim_a">Hari and Sukhmani Live</h3>

                                <div class="tm-panel-caption txt_anim_b">
                                    <h4 class="center pad_card">Strike, Gurgaon 4km</h4>
                                    <h5 class="center pad_card">8:30pm onwards Tuesday, 15th March</h5><br>
                                    <div class="" style="float:left;">
                                     <p class="uk-button">Nightlife</p>
                                     <p class="uk-button">Nightlife</p>
                                    </div>
                                </div>

            </div>
        </div>

                                                   <a href="event_detail.html"><div class="uk-position-cover tm-panel-link icon-plus"  data-index="0" data-uk-modal=""></div></a>

    </figure>

</div></div>



<div class="uk-width-1-3"><div class="uk-panel uk-overlay-hover uk-text-center uk-scrollspy-init-inview uk-scrollspy-inview uk-animation-fade">
<div class="uk-panel-badge uk-badge badgy"></div>
<div class="uk-panel-badge uk-badge badgy1">20% off</div>
<div class="uk-panel-badge uk-badge badgy_left">Trending</div>

    <figure class="uk-overlay tm-overlay-uniq ">

        <img src="http://forbesindia.com/blog/wp-content/uploads/2013/08/panaea.jpg" class="uk-overlay-scale brdr_red" alt="Wooden Chair" width="1400" height="1070" style="">
        <div class="uk-overlay-panel uk-overlay-bottom">
            <div>

                                <h3 class="uk-h2 uk-margin-bottom-remove tm-overlay-panel-title txt_anim_a">Hari and Sukhmani Live</h3>

                                <div class="tm-panel-caption txt_anim_b">
                                    <h4 class="center pad_card">Strike, Gurgaon 4km</h4>
                                    <h5 class="center pad_card">8:30pm onwards Tuesday, 15th March</h5><br>
                                    <div class="" style="float:left;">
                                     <p class="uk-button">Nightlife</p>
                                     <p class="uk-button">Nightlife</p>
                                    </div>
                                </div>

            </div>
        </div>

                                                    <a href="event_detail.html"><div class="uk-position-cover tm-panel-link icon-plus"  data-index="0" data-uk-modal=""></div></a>

    </figure>

</div></div>

<div class="uk-width-1-3"><div class="uk-panel uk-overlay-hover uk-text-center uk-scrollspy-init-inview uk-scrollspy-inview uk-animation-fade">
<div class="uk-panel-badge uk-badge badgy"></div>
<div class="uk-panel-badge uk-badge badgy1">20% off</div>
<div class="uk-panel-badge uk-badge badgy_left">Trending</div>

    <figure class="uk-overlay tm-overlay-uniq ">

        <img src="http://timesofindia.indiatimes.com/photo/50297508.cms" class="uk-overlay-scale brdr_red" alt="Wooden Chair" width="1400" height="1070" style="">
        <div class="uk-overlay-panel uk-overlay-bottom">
            <div>

                                <h3 class="uk-h2 uk-margin-bottom-remove tm-overlay-panel-title txt_anim_a">Hari and Sukhmani Live</h3>
                                <div class="tm-panel-caption txt_anim_b">
                                    <h4 class="center pad_card">Strike, Gurgaon 4km</h4>
                                    <h5 class="center pad_card">8:30pm onwards Tuesday, 15th March</h5><br>
                                    <div class="" style="float:left;">
                                     <p class="uk-button">Nightlife</p>
                                     <p class="uk-button">Nightlife</p>
                                    </div>
                                </div>

            </div>
        </div>

                                                    <a href="event_detail.html"><div class="uk-position-cover tm-panel-link icon-plus"  data-index="0" data-uk-modal=""></div></a>

    </figure>

</div></div> -->

</div>

    </li>



    <li class="">

<div class="uk-grid">




<?php

    


?>












    <!-- <div class="uk-width-1-3">
        <div class="uk-panel uk-overlay-hover uk-text-center uk-scrollspy-init-inview uk-scrollspy-inview uk-animation-fade">
<div class="uk-panel-badge uk-badge badgy"></div>
<div class="uk-panel-badge uk-badge badgy1">20% off</div>
<div class="uk-panel-badge uk-badge badgy_left">Trending</div>

    <figure class="uk-overlay tm-overlay-uniq ">

        <img src="http://timesofindia.indiatimes.com/photo/50809064.cms" class="uk-overlay-scale brdr_red" alt="Wooden Chair" width="1400" height="1070" style="">
        <div class="uk-overlay-panel uk-overlay-bottom">
            <div>

                                <h3 class="uk-h2 uk-margin-bottom-remove tm-overlay-panel-title txt_anim_a">Hari and Sukhmani Live</h3>

                                <div class="tm-panel-caption txt_anim_b">
                                    <h4 class="center pad_card">Strike, Gurgaon 4km</h4>
                                    <h5 class="center pad_card">8:30pm onwards Tuesday, 15th March</h5><br>
                                    <div class="" style="float:left;">
                                     <p class="uk-button">Nightlife</p>
                                     <p class="uk-button">Nightlife</p>
                                    </div>
                                </div>

            </div>
        </div>

                                                  <a href="event_detail.html"><div class="uk-position-cover tm-panel-link icon-plus"  data-index="0" data-uk-modal=""></div></a>

    </figure>

</div></div>



<div class="uk-width-1-3"><div class="uk-panel uk-overlay-hover uk-text-center uk-scrollspy-init-inview uk-scrollspy-inview uk-animation-fade">

<div class="uk-panel-badge uk-badge badgy"></div>
<div class="uk-panel-badge uk-badge badgy1">20% off</div>
 <div class="uk-panel-badge uk-badge badgy_left">Trending</div>
    <figure class="uk-overlay tm-overlay-uniq ">

        <img src="http://52.74.78.106/blog/wp-content/uploads/2015/07/Third-Eye-nightclub-.jpg" class="uk-overlay-scale brdr_red" alt="Wooden Chair" width="1400" height="1070" style="">
        <div class="uk-overlay-panel uk-overlay-bottom">
            <div>

                                <h3 class="uk-h2 uk-margin-bottom-remove tm-overlay-panel-title txt_anim_a">Hari and Sukhmani Live</h3>

                                <div class="tm-panel-caption txt_anim_b">
                                    <h4 class="center pad_card">Strike, Gurgaon 4km</h4>
                                    <h5 class="center pad_card">8:30pm onwards Tuesday, 15th March</h5><br>
                                    <div class="" style="float:left;">
                                     <p class="uk-button">Nightlife</p>
                                     <p class="uk-button">Nightlife</p>
                                    </div>
                                </div>

            </div>
        </div>

                                                    <a href="event_detail.html"><div class="uk-position-cover tm-panel-link icon-plus"  data-index="0" data-uk-modal=""></div></a>

    </figure>

</div></div>

<div class="uk-width-1-3"><div class="uk-panel uk-overlay-hover uk-text-center uk-scrollspy-init-inview uk-scrollspy-inview uk-animation-fade">
<div class="uk-panel-badge uk-badge badgy"></div>
<div class="uk-panel-badge uk-badge badgy1">20% off</div>
<div class="uk-panel-badge uk-badge badgy_left">Trending</div>

    <figure class="uk-overlay tm-overlay-uniq ">

        <img src="http://images.locanto.in/1096382497/Bali-Nightlife-Tour-Packages_1.jpg" class="uk-overlay-scale brdr_red" alt="Wooden Chair" width="1400" height="1070" style="">
        <div class="uk-overlay-panel uk-overlay-bottom">
            <div>

                                <h3 class="uk-h2 uk-margin-bottom-remove tm-overlay-panel-title txt_anim_a">Hari and Sukhmani Live</h3>

                                <div class="tm-panel-caption txt_anim_b">
                                    <h4 class="center pad_card">Strike, Gurgaon 4km</h4>
                                    <h5 class="center pad_card">8:30pm onwards Tuesday, 15th March</h5><br>
                                    <div class="" style="float:left;">
                                     <p class="uk-button">Nightlife</p>
                                     <p class="uk-button">Nightlife</p>
                                    </div>
                                </div>

            </div>
        </div>

                                                   <a href="event_detail.html"><div class="uk-position-cover tm-panel-link icon-plus"  data-index="0" data-uk-modal=""></div></a>

    </figure>

</div></div>

</div>


<div class="uk-grid">
    <div class="uk-width-1-3">
        <div class="uk-panel uk-overlay-hover uk-text-center uk-scrollspy-init-inview uk-scrollspy-inview uk-animation-fade">
<div class="uk-panel-badge uk-badge badgy"></div>
<div class="uk-panel-badge uk-badge badgy1">20% off</div>
<div class="uk-panel-badge uk-badge badgy_left">Trending</div>

    <figure class="uk-overlay tm-overlay-uniq ">

        <img src="http://timesofindia.indiatimes.com/photo/49830598.cms" class="uk-overlay-scale brdr_red" alt="Wooden Chair" width="1400" height="1070" style="">
        <div class="uk-overlay-panel uk-overlay-bottom">
            <div>

                                <h3 class="uk-h2 uk-margin-bottom-remove tm-overlay-panel-title txt_anim_a">Hari and Sukhmani Live</h3>

                                <div class="tm-panel-caption txt_anim_b">
                                    <h4 class="center pad_card">Strike, Gurgaon 4km</h4>
                                    <h5 class="center pad_card">8:30pm onwards Tuesday, 15th March</h5><br>
                                    <div class="" style="float:left;">
                                     <p class="uk-button">Nightlife</p>
                                     <p class="uk-button">Nightlife</p>
                                    </div>
                                </div>

            </div>
        </div>

                                                   <a href="event_detail.html"><div class="uk-position-cover tm-panel-link icon-plus"  data-index="0" data-uk-modal=""></div></a>

    </figure>

</div></div>



<div class="uk-width-1-3"><div class="uk-panel uk-overlay-hover uk-text-center uk-scrollspy-init-inview uk-scrollspy-inview uk-animation-fade">
<div class="uk-panel-badge uk-badge badgy"></div>
<div class="uk-panel-badge uk-badge badgy1">20% off</div>
<div class="uk-panel-badge uk-badge badgy_left">Trending</div>

    <figure class="uk-overlay tm-overlay-uniq ">

        <img src="http://forbesindia.com/blog/wp-content/uploads/2013/08/panaea.jpg" class="uk-overlay-scale brdr_red" alt="Wooden Chair" width="1400" height="1070" style="">
        <div class="uk-overlay-panel uk-overlay-bottom">
            <div>

                                <h3 class="uk-h2 uk-margin-bottom-remove tm-overlay-panel-title txt_anim_a">Hari and Sukhmani Live</h3>

                                <div class="tm-panel-caption txt_anim_b">
                                    <h4 class="center pad_card">Strike, Gurgaon 4km</h4>
                                    <h5 class="center pad_card">8:30pm onwards Tuesday, 15th March</h5><br>
                                    <div class="" style="float:left;">
                                     <p class="uk-button">Nightlife</p>
                                     <p class="uk-button">Nightlife</p>
                                    </div>
                                </div>

            </div>
        </div>

                                                    <a href="event_detail.html"><div class="uk-position-cover tm-panel-link icon-plus"  data-index="0" data-uk-modal=""></div></a>

    </figure>

</div></div>

<div class="uk-width-1-3"><div class="uk-panel uk-overlay-hover uk-text-center uk-scrollspy-init-inview uk-scrollspy-inview uk-animation-fade">
<div class="uk-panel-badge uk-badge badgy"></div>
<div class="uk-panel-badge uk-badge badgy1">20% off</div>
<div class="uk-panel-badge uk-badge badgy_left">Trending</div>

    <figure class="uk-overlay tm-overlay-uniq ">

        <img src="http://timesofindia.indiatimes.com/photo/50297508.cms" class="uk-overlay-scale brdr_red" alt="Wooden Chair" width="1400" height="1070" style="">
        <div class="uk-overlay-panel uk-overlay-bottom">
            <div>

                                <h3 class="uk-h2 uk-margin-bottom-remove tm-overlay-panel-title txt_anim_a">Hari and Sukhmani Live</h3>
                                <div class="tm-panel-caption txt_anim_b">
                                    <h4 class="center pad_card">Strike, Gurgaon 4km</h4>
                                    <h5 class="center pad_card">8:30pm onwards Tuesday, 15th March</h5><br>
                                    <div class="" style="float:left;">
                                     <p class="uk-button">Nightlife</p>
                                     <p class="uk-button">Nightlife</p>
                                    </div>
                                </div>

            </div>
        </div>

                                                    <a href="event_detail.html"><div class="uk-position-cover tm-panel-link icon-plus"  data-index="0" data-uk-modal=""></div></a>

    </figure>

</div></div>
 -->
</div>

    </li>

</ul>

</div>
</div></div>
</section>

    
    </div>
</div>


        
        
        
          
        

        

        <div class="tm-footer">
            <div class="uk-container uk-container-center uk-text-center">

                <ul class="uk-subnav uk-subnav-line uk-flex-center">
                    <li><a href="#">Facebook</a></li>
                    <li><a href="#">Instagram</a></li>
                    <li><a href="#">Twitter</a></li>
                </ul>

                <div class="uk-panel">
                    <p>Copyright ClubGo - All rights reserved.</p>
                    <a href="index.php"><img src="docs/images/clugo.png" width="90" height="30" title="Clubgo" alt="Clubgo"></a>
                </div>

            </div>
        </div>


<div id="tm-offcanvas" class="uk-offcanvas">

	  <div class="uk-offcanvas-bar">

		<ul class="uk-nav uk-nav-offcanvas uk-nav-parent-icon" data-uk-nav="{ multiple: true }">
		  <li><a href="index.php">Home</a></li>
	  <li><a href="about.php">About</a></li>
		  <li><a href="events.php"> Event </a></li>
		  <li><a href="venues.php"> Venues </a></li>
<!--           <li><a href="offer.php"> Offers </a></li> -->
		  <li><a href="artist.php"> Artists </a></li>
		  <li><a href="login.php" id="ulog1"> Login/Signup </a></li>


		 <li style="display:none" id="uout1"><a href="logout.php">Logout</a></li>
		  <li><a href="download.php"> Download App </a></li>

		</ul>

	  </div>

	</div>

	<script type="text/javascript">

var us1='<?php echo $userid ?>';

if(us1){


$("#uout1").show();
$("#ulog1").hide();

}


	</script>
        
        <script>

            $.ajax({
                dataType : "jsonp",
                url      : "https://api.github.com/repos/uikit/uikit?callback=ukghapi&nocache="+Math.random(),
                success  : function(data){

                    if(!data) return;

                    if(data.data.watchers){
                        $("[data-uikit-stargazers]").html(data.data.watchers);
                    }

                    if(data.data.forks){
                        $("[data-uikit-forks]").html(data.data.forks);
                    }
                }
            });
        </script>
    </body>
</html>
 <?php ob_end_flush();
 ?>