<?php
include("config.php");
ob_start();
session_start();
@$userid=$_SESSION["userid"];
@$user=$_SESSION["name"];
 @$fbid=$_SESSION["fbid"];
?>

<!DOCTYPE html>
<html lang="en-gb" dir="ltr">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="keywords" content="" />
	<meta name="description" content="" />
        <title>ClubGo - Your Nightlife Conceirge</title>
        <link rel="shortcut icon" href="docs/images/clubgo-icon.png" type="image/x-icon">
        <link rel="apple-touch-icon-precomposed" href="docs/images/apple-touch-icon.png">
        <link id="data-uikit-theme" rel="stylesheet" href="docs/css/uikit.docs.min.css">
        <link rel="stylesheet" href="docs/css/docs.css">
        <link rel="stylesheet" href="docs/css/custom.css">
        <link rel="stylesheet" href="docs/css/makeweb.css">
        <link rel="stylesheet" href="docs/css/responsive.css">
        <link rel="stylesheet" href="vendor/highlight/highlight.css">
        <script src="vendor/jquery.js"></script>
        <script src="docs/js/uikit.min.js"></script>
        <script src="vendor/highlight/highlight.js"></script>
        <script src="docs/js/docs.js"></script>
        <script src="docs/js/slideshow.js"></script>
        <script src="docs/js/slideshow-fx.js"></script>
        <script src="docs/js/slideset.js"></script> 
         <script src="docs/js/loader.js"></script> 

 <script type="text/javascript" src="js/jquery.js"></script>
    </head>
<style>
html{
    background: url(docs/images/cl-bg.jpg) no-repeat center center fixed;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
}
</style>
    <body class="">

        <nav class="tm-navbar uk-navbar uk-navbar-attached">
            <div class="uk-container uk-container-center">

                <div class="uk-animation-hover"><a class="uk-navbar-brand uk-hidden-small uk-animation-reverse uk-animation-scale" href="index.php"><img class="uk-margin uk-margin-remove" src="docs/images/clugo.png" width="120" height="40" title="Clubgo" alt="Clubgo"></a></div>

                <ul class="uk-navbar-nav uk-hidden-small uk-navbar-flip">
	                <li><a href="about.php">About</a></li>
                    <li><a href="events.php">Events</a></li>
                    <li><a href="venues.php">Venues</a></li>
<!--                     <li><a href="offer.php">Offers</a></li> -->
                    <li><a href="artist.php">Artists</a></li>
                    <li><a href="login.php" id="ulog">Login/Signup</a></li>
                    <li class="download"><a href="download.php">Download App</a></li>
                    <?php
if($fbid){
?>
<li><div class="uk-button-dropdown" data-uk-dropdown="{mode:'click'}" aria-haspopup="true" aria-expanded="false"><img src="https://graph.facebook.com/<?php echo  $fbid ?>/picture?type=small" width="40" height="40" class="fb-img"><div class="uk-dropdown uk-dropdown-bottom" aria-hidden="true" style="top: 30px; left: 0px;" tabindex="" >
                                        <ul class="uk-nav uk-nav-dropdown">
                                           <li><a href="logout.php">Logout</a></li> 
                                           <li><a href="history.php">Booked Tickets</a></li>
                                        </ul>
                                    </div>
                                </div></li>

<?php
}
if($userid and !$fbid){
?>
<li class="mail-img"><div class="uk-button-dropdown" data-uk-dropdown="{mode:'click'}" aria-haspopup="true" aria-expanded="false"><img src="docs/img/user-white.png" width="20" height="20"><div class="uk-dropdown uk-dropdown-bottom" aria-hidden="true" style="top: 30px; left: 0px;" tabindex="">
                                        <ul class="uk-nav uk-nav-dropdown" >
                                           <li id="uout" ><a href="logout.php">Logout</a></li> 
                                           <li><a href="history.php">Booked Tickets</a></li>
                                        </ul>
                                    </div>
                                </div></li>
<?php
}
  ?>
                </ul>

                <a href="#tm-offcanvas" class="uk-navbar-toggle uk-visible-small" data-uk-offcanvas></a>

                <div class="uk-navbar-brand uk-navbar-center uk-visible-small"><img src="docs/images/clugo.png" width="120" height="50" title="Clubgo" alt="Clubgo"></div>

            </div>
        </nav>

 <script type="text/javascript">


var us='<?php echo $userid ?>';

if(us){


$("#uout").show();
$("#ulog").hide();

}


        </script>

        <div class="tm-section tm-section-color-1 tm-section-colored">
            <div class="uk-container uk-container-center uk-text-center">
	            
	            <div class="uk-grid">
		            <div class="uk-width-medium-1-2 uk-width-small-1-1">
			            
			            <img class="tm-logo" src="docs/images/clubgo-icon.png" width="150" height="200" title="ClubGo" alt="ClubGO">

                <p class="uk-text-large" style="font-weight: 500">Your Nightlife Conceirge</p>
<span class="" style="margin-top:-5px;"><img src="docs/img/android.png" width="200" height="45"></span>
                
                <span class="" style="margin-top:-5px;"><img src="docs/img/apple.png" width="200" height="25"></span>

<!--
                <ul class="tm-subnav uk-subnav uk-flex-center">
                    <li><a href="#">Read More</a></li>
                    <li><a href="#">Get Started</a></li>
                    <li data-uikit-version></li>
                </ul>
-->
<div class="uk-text-margin" style="margin-top: 20px;">
<!--
                <ul class="tm-subnav uk-subnav uk-flex-center">
                    <li><a href="#"><i class="uk-icon-star"></i> <span data-uikit-stargazers style="font-weight: 500;font-size: 16px;">6514</span><span style="font-weight: 500;font-size: 16px;">Events</span></a></li>
                    <li><a href="#"><i class="uk-icon-code-fork"></i> <span data-uikit-forks style="font-weight: 500;font-size: 16px;">1287</span><span style="font-weight: 500;font-size: 16px;"> Venues </span></a></li>
                </ul>
-->
                <ul class="tm-subnav uk-subnav uk-flex-center">
                     <li><a href="https://www.facebook.com/ClubGoApp/" target="_blank"><span style="font-weight: 500;font-size: 16px;"> @facebook </span></a></li>
                    <li><a href="https://twitter.com/ClubGoApp" target="_blank"></i> <span style="font-weight: 500;font-size: 16px;"> @twitter </span></a></li>
                    <li><a href="https://www.instagram.com/clubgoapp/" target="_blank"></i> <span style="font-weight: 500;font-size: 16px;"> #instagram </span></a></li>
                   
                </ul>
</div>          
		            </div>
		            <div class="uk-width-medium-1-2 uk-width-small-1-1">
			            <p><img src="docs/img/mockup.png" style="width:600px;"</p>
		            </div>
	            </div>

<!--
                <img class="tm-logo" src="docs/images/clubgo-icon.png" width="200" height="200" title="ClubGo" alt="ClubGO">

                <p class="uk-text-large">Your Nightlife Conceirge</p>
<p class="uk-text-large" style="margin-bottom:0px;">Coming Soon for Android <span class="" style="margin-top:-5px;"><img src="docs/img/android.png" width="50" height="45"></span></p>
                
                <p class="uk-text-large">Coming Soon on App Store &nbsp;<span class="" style="margin-top:-5px;"><img src="docs/img/apple.png" width="25" height="25"></span></p>

                <ul class="tm-subnav uk-subnav uk-flex-center">
                    <li><a href="#">Read More</a></li>
                    <li><a href="#">Get Started</a></li>
                    <li data-uikit-version></li>
                </ul>

                <ul class="tm-subnav uk-subnav uk-flex-center">
                    <li><a href="#"><i class="uk-icon-star"></i> <span data-uikit-stargazers>6514</span> Events</a></li>
                    <li><a href="#"><i class="uk-icon-code-fork"></i> <span data-uikit-forks>1287</span> Venues</a></li>
                    <li><a href="#"><i class="uk-icon-twitter"></i> @clubgo</a></li>
                    <li><a href="#"><i class="uk-icon-comments-o"></i> Fb.com/Clubgo</a></li>
                </ul>
-->

            </div>
        </div>

        

        <div id="tm-offcanvas" class="uk-offcanvas">

	  <div class="uk-offcanvas-bar">

		<ul class="uk-nav uk-nav-offcanvas uk-nav-parent-icon" data-uk-nav="{ multiple: true }">
		  <li><a href="index.php">Home</a></li>
	  <li><a href="about.php">About</a></li>
		  <li><a href="events.php"> Event </a></li>
		  <li><a href="venues.php"> Venues </a></li>
<!--           <li><a href="offer.php"> Offers </a></li> -->
		  <li><a href="artist.php"> Artists </a></li>
		  <li><a href="login.php" id="ulog1"> Login/Signup </a></li>


		 <li style="display:none" id="uout1"><a href="logout.php">Logout</a></li>
		  <li><a href="download.php"> Download App </a></li>

		</ul>

	  </div>

	</div>

	<script type="text/javascript">

var us1='<?php echo $userid ?>';

if(us1){


$("#uout1").show();
$("#ulog1").hide();

}


	</script>

        

        

        
        <script>

            $.ajax({
                dataType : "jsonp",
                url      : "https://api.github.com/repos/uikit/uikit?callback=ukghapi&nocache="+Math.random(),
                success  : function(data){

                    if(!data) return;

                    if(data.data.watchers){
                        $("[data-uikit-stargazers]").html(data.data.watchers);
                    }

                    if(data.data.forks){
                        $("[data-uikit-forks]").html(data.data.forks);
                    }
                }
            });
        </script>
    </body>
</html>
<?php ob_end_flush();
 ?>