<?php
include("config.php");
error_reporting(0);
ob_start();
session_start();

@$user=$_SESSION["name"];
@$userid=$_SESSION["userid"];
@$fbid=$_SESSION["fbid"];

?>

<!DOCTYPE html>
<html lang="en-gb" dir="ltr">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="keywords" content="" />
    <meta name="description" content="" />
        <title>ClubGo - Your Nightlife Conceirge</title>
        <link rel="shortcut icon" href="docs/images/clubgo-icon.png" type="image/x-icon">
        <link rel="apple-touch-icon-precomposed" href="docs/images/apple-touch-icon.png">
        <link id="data-uikit-theme" rel="stylesheet" href="docs/css/uikit.docs.min.css">
        <link rel="stylesheet" href="docs/css/docs.css">
        <link rel="stylesheet" href="docs/css/custom.css">
        <link rel="stylesheet" href="docs/css/makeweb.css">
        <link rel="stylesheet" href="docs/css/responsive.css">
        <link rel="stylesheet" href="docs/css/theme1.css">
        <link rel="stylesheet" href="vendor/highlight/highlight.css">
        <script src="vendor/jquery.js"></script>
        <script src="docs/js/uikit.min.js"></script>
        <script src="vendor/highlight/highlight.js"></script>
        <script src="docs/js/docs.js"></script>
        <script src="docs/js/slideshow.js"></script>
        <script src="docs/js/slideshow-fx.js"></script>
        <script src="docs/js/slideset.js"></script>
        <script src="docs/js/lightbox.js"></script>
        <script src="docs/js/sticky.js"></script>    
        </head>

    <body class="tm-background">

        

                <script type="text/javascript">


var us='<?php echo $userid ?>';

if(us){


$("#uout").show();
$("#ulog").hide();

}


        </script>
        

        <div class="tm-section-color-2 tm-section-colored top_a">
            <div class="uk-container uk-container-center uk-text-center top_a">
             <br>   
             <h1 style="color: #000 !important;">Terms & Conditions</h1>   
                <div class="uk-margin spacein spacein1" data-uk-slideset="{small: 2, medium: 3, large: 4}">
                    <div class="uk-slidenav-position uk-margin" style="margin-top: 60px;color:#000">
                        <ol style="text-align: left; font-size: 13px;font-weight: 500;word-spacing:4px;">
                            <li><h1 style="color: #000 !important; text-align: left; font-size: 20px;font-weight: 500">The Basics</h1></li>
                            <ul>
                                <li>To use the App and make any Purchases, you must be 18 or over. By continuing to use the App and agreeing to these Terms you are representing to us that you are 18 and we can rely on this representation.
                                </li>
                                <li>
                                The App and the Events offered on the App are only directed at people who access the App and make Purchases in the india. We do not represent that any Events are suitable outside the india and you should be careful to satisfy yourself of a Event's suitability to your circumstances (such as considering your travel arrangements to/from the event because unless this is expressly stated as part of the Event, it is not included as part of the Purchase). If you choose to use the App or make a Purchase outside the india you do so at your own initiative.
                                </li>
                                <li>
                                We grant you a limited licence to use the App to access and use our services for your own personal and non-commercial use, provided that you comply fully with these Terms (e.g. you must not re-sell the Tickets).                                    
                                </li>
                                <li>
                                   We reserve the right to prevent you using the App or making Purchases, however we will honour Purchases already made, unless you have not complied with these Terms or we are unable to because of circumstances outside our control. For more on this, please read below.
                                </li>
                            </ul>

                            <li><h1 style="color: #000 !important; text-align: left; font-size: 20px;font-weight: 500">Access to the App</h1></li>
                            <ul>
                                <li>We cannot guarantee the continuous, uninterrupted or error-free operability of the App. There may be times when certain features, parts or content of the App, or the entire App, become unavailable (whether on a scheduled or unscheduled basis) or are modified, suspended or withdrawn by us, in our sole discretion, without notice to you. You agree that we will not be liable to you or to any third party for any unavailability, modification, suspension or withdrawal of the App, or any features, parts or content of the App.
                                </li>
                                <li> We may change the format and content of the App from time to time. We reserve the right to modify, change, substitute or withdraw any Events or other information on our App. You agree that your use of the App is on an 'as is' and 'as available' basis and at your sole risk.
                                </li>
                            </ul>
                            
                            <li><h1 style="color: #000 !important; text-align: left; font-size: 20px;font-weight: 500">Registering  ClubGo Account</h1></li>
                            <ul>
                                <li>Why register? To put simply, if you don't register an account, you won't be able to make any Purchases. This is because you need an account in order to view your Purchased Tickets, redeem them (if required), view your past Purchases, store your financial details and modify your preference  ClubGo Entertainment Private Limited to name a few. We reserve the right to decline a new registration or cancel your account at any time (including if you don't provide us with a valid e-mail address that is personal to you, or if you don't provide us with a correct name and surname).
                                </li>

                                <li>Why register? To put simply, if you don't register an account, you won't be able to make any Purchases. This is because you need an account in order to view your Purchased Tickets, redeem them (if required), view your past Purchases, store your financial details and modify your preference  ClubGo Entertainment Private Limited to name a few. We reserve the right to decline a new registration or cancel your account at any time (including if you don't provide us with a valid e-mail address that is personal to you, or if you don't provide us with a correct name and surname).
                                </li>
                                <li>
                                    To register an account you will need to fill out a form on the mobile app or online. You warrant that the information you input is true, complete and accurate. You will promptly inform us of any changes to such information (you can do this by updating your account details at any time). The personal information you give us is governed by our privacy policy.
                                </li>
                                <li>
                                     In order to register and access your account, you will need to choose a password. It is important you keep this password confidential because you are entirely responsible if you do not maintain the confidentiality of your password to a reasonably expected standard and Purchases are made via your account. Let us know as soon as you become aware there has been a breach of security and we will suspend your account. You agree that any person who you directly or implicitly permit to use your password (implicitly, for example, because you do not log out of your account and another person uses your device to make Purchases or you write your password on a noticeboard in your home and a family member then uses it to gain access to your account) is authorised to act as your agent to use the App and make Purchases via your account. You will not be responsible to pay for Purchases made after you have notified us of a password change or that security has been compromised.
                                </li>
                                <li>
                                    One individual user is allowed to register only one account on the App. You warrant that you will not create fraudulent accounts. You also warrant that you will not use any fraudulent activities to generate unfair Reward Credits in the App to be applied to you (for instance, you will not redeem your own invitation code with a different account of yours).
                                </li>
                            </ul>

                            <li><h1 style="color: #000 !important; text-align: left; font-size: 20px;font-weight: 500">Intellectual Property Rights</h1></li>
                            <ul>
                                <li>Why register? To put simply, if you don't register an account, you won't be able to make any Purchases. This is because you need an account in order to view your Purchased Tickets, redeem them (if required), view your past Purchases, store your financial details and modify your preference  ClubGo Entertainment Private Limited to name a few. We reserve the right to decline a new registration or cancel your account at any time (including if you don't provide us with a valid e-mail address that is personal to you, or if you don't provide us with a correct name and surname).
                                </li>

                                <li>
                                     All intellectual property rights (which includes rights such as copyright, and rights in trade marks) in any content of the App (including text, graphics, software, photographs and other images, videos, sound, trade marks and logos) are owned by us and/or our licensors. Except as expressly set out in these Terms, nothing in these Terms gives you any rights in respect of any intellectual property owned by us or our licensors and you acknowledge that you do not acquire any ownership rights by downloading content from the App. If you print off, copy or store pages from the App (only as permitted by these terms and conditions), you must ensure that any copyright, trade mark or other intellectual property right noticeces contained in the original content are reproduced.
                                </li>
                                <li>
                                    You agree that by submitting any posting, images or other content for publication on the App (Your Content), you retain any copyright you may have in Your Content, however you grant us and our affiliates a perpetual, irrevocable, worldwide, non-exclusive, royalty-free and fully sub-licensable right and licence to use, reproduce, edit, modify, adapt, publish, translate, create derivative works from, distribute, perform and display such content (in whole or in part) and/or to incorporate it into other works in any form, media or technology, whether for commercial or non-commercial purposes. You waive any moral rights you may have in, or to be identified as the author, of Your Content.

                                </li>
                                <li>
                                    You are solely responsible for Your Content (including content you share with other sites, such as social networking sites) and we do not endorse Your Content or any opinion, recommendation, or advice expressed therein, and we expressly disclaim any and all liability in connection with Your Content.

                                </li>
                                
                            </ul>

                            <li><h1 style="color: #000 !important; text-align: left; font-size: 20px;font-weight: 500">Review Forums</h1></li>
                            <ul>
                                <li>
                                    The App may, from time to time, make message boards, news groups and/or other public forums (collectively Review Forums) available to you so that you can feedback on Events offered on the App and read the feedback submitted by other users of the App. We do not control the material submitted to Review Forums (collectively Postings), nor are Review Forums moderated. You are solely responsible for the content of your Postings as submitted by you and acknowledge that all Postings express the views of their respective authors, and not our views.
<ul><li>If you participate in any Review Forum, you must:</li>

<li> keep all Postings relevant to the purpose of the Review Forum and the nature of any topic;</li>
<li>  not submit any Posting that is unlawful, threatening, abusive, libellous, pornographic, obscene, vulgar, indecent, offensive or which infringes on the intellectual property rights or other rights of any person;</li>
<li>   not submit any Posting that contains any viruses and/or other code that has contaminating or destructive elements;
<li>   not submit any Posting containing any form of advertising; </li>
<li>   not impersonate, or misrepresent an affiliation with, any person or entity.</li>
</ul>
You agree that, by submitting any Posting, you grant us and our affiliates a perpetual, irrevocable, worldwide, non-exclusive, royalty-free and fully sub-licensable right and licence to use, reproduce, modify, adapt, publish, translate, create derivative works from, distribute, perform and display such Posting (in whole or part) and/or to incorporate it in other works in any form, media or technology, and you waive any moral rights you may have in, or to be identified as the author, of such Posting.</br>
Whilst we do not screen or monitor Postings, we reserve the right, in our sole discretion, to delete, edit or modify any Posting submitted by you and/or to close any topic, at any time without notice to you.
Complaints about the content of any Posting must be sent to hello@ClubGo.com and must contain details of the specific Posting giving rise to the complaint.
                                </li>
                                
                            </ul>

                            <li><h1 style="color: #000 !important; text-align: left; font-size: 20px;font-weight: 500">External Links</h1></li>
                            <ul>
                                <li>The App may, from time to time, include links to external sites. We have included links to these sites to provide you with access to information and services that you may find useful or interesting. We are not responsible for the content of these sites or for anything provided by them and do not guarantee that they will be continuously available. The fact that we include links to such external sites does not imply any endorsement of or association with their operators.
                                </li>
                            </ul>

                            <li> <h1 style="color: #000 !important; text-align: left; font-size: 20px;font-weight: 500">Your Personal Information</h1></li>
                            <ul>
                                <li>Use of your personal information submitted via the App is governed by our privacy policy.
                                </li>
                            </ul>

                            <li>Price and Repayment</li>
                            <ul>
                                <li>The price of Tickets is as quoted on the App from time to time. Prices include VAT, if applicable.
Prices are liable to change at any time, but changes will not affect Purchases in respect of which we have already sent you an email confirmation.
                                </li>
                                <li>Payment for all orders must be made by credit or debit card by entering such information in the Settings part in the App or as part of the booking process.
                                </li>
                                <li>You should be aware that online payment transactions are subject to validation checks by your card issuer and we are not responsible if your card issuer declines to authorise payment for any reason. Please note, it is possible that your card issuer may charge you an online handling fee or processing fee. We are not responsible for this.
                                </li>
                            </ul>

                            <li><h1 style="color: #000 !important; text-align: left; font-size: 20px;font-weight: 500">Refund Policy</h1></li>
                            <ul>
                                <li>Purchases for Tickets cannot be exchanged or refunded, except in the following circumstances:
                                (a) If we discover an error in the price of the Tickets you have ordered, we will use reasonable endeavours to inform you as soon as possible and allow you to either reconfirm your order at the correct price (crediting or debiting your account as applicable), or cancel your order. If you choose to cancel, you will receive a full refund from us if you've already paid. If we are unable to contact you, this will be treated as a cancellation.
(b) If an Event is cancelled or there is a material change to the subject matter of the Ticket contract (that is what you have contracted to see) we will give you a full refund of the face value price paid or, if the face value has been reduced by the Event Partner, the discounted face value. A material change is one which, in our reasonable opinion, makes the Event materially different to what a purchaser of the Event, taken generally, could reasonably expect. For example, the use of understudies in a theatre performance does not constitute a material change. If an event is re-scheduled (and you cannot attend the re-scheduled date), we will coordinate with the Event Partner on how your ticket(s) will be treated however a refund cannot be guaranteed.
                                </li>
                                <li>Paying refunds
                                    We will usually refund any money received from you using the same method originally used by you to pay for your purchase.
                                    You may reject a credit or refund within 30 days of receiving the credit or refund, but if you do not reject the credit or refund by notifying us within that timeframe you will be deemed to have accepted the credit or refund in full and final settlement of any and all claims, actions, demands and proceedings you may have against ClubGo Entertainment Private Limited arising out of the Ticket or Purchase for which you received the credit or refund.

                                </li>
                                
                            </ol>

               <br><br>&nbsp;
             
            </div>
        </div>

       
   <div class="tm-footer foot-fix">
            <div class="uk-container uk-container-center uk-text-center">

                <ul class="uk-subnav uk-subnav-line uk-flex-center">
                    <li><a href="https://www.facebook.com/ClubGoApp/" target="_blank">Facebook</a></li>
                    <li><a href="https://www.instagram.com/clubgoapp/" target="_blank">Instagram</a></li>
                    <li><a href="https://twitter.com/ClubGoApp" target="_blank">Twitter</a></li>
                </ul>

                <div class="uk-panel">
                    <p>Copyright ClubGo - All rights reserved.</p>
                    <a href="index.php"><img src="docs/images/clugo.png" width="90" height="30" title="ClubGo" alt="ClubGo"></a>
                </div>

            </div>
        </div>

<div id="tm-offcanvas" class="uk-offcanvas">

      <div class="uk-offcanvas-bar">

        <ul class="uk-nav uk-nav-offcanvas uk-nav-parent-icon" data-uk-nav="{ multiple: true }">
          <li><a href="index.php">Home</a></li>
      <li><a href="about.php">About</a></li>
          <li><a href="events.php"> Event </a></li>
          <li><a href="venues.php"> Venues </a></li>
<!--           <li><a href="offer.php"> Offers </a></li> -->
          <li><a href="artist.php"> Artists </a></li>
          <li><a href="login.php" id="ulog1"> Login/Signup </a></li>


         <li style="display:none" id="uout1"><a href="logout.php">Logout</a></li>
          <li><a href="download.php"> Download App </a></li>

        </ul>

      </div>

    </div>

    <script type="text/javascript">

var us1='<?php echo $userid ?>';

if(us1){


$("#uout1").show();
$("#ulog1").hide();

}


    </script>
        
        <script>

            $.ajax({
                dataType : "jsonp",
                url      : "https://api.github.com/repos/uikit/uikit?callback=ukghapi&nocache="+Math.random(),
                success  : function(data){

                    if(!data) return;

                    if(data.data.watchers){
                        $("[data-uikit-stargazers]").html(data.data.watchers);
                    }

                    if(data.data.forks){
                        $("[data-uikit-forks]").html(data.data.forks);
                    }
                }
            });
        </script>
    </body>
</html>
 <?php ob_end_flush();
 ?>