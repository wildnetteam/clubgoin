<?php
include("config.php");
error_reporting(0);
ob_start();
session_start();

@$user=$_SESSION["name"];
@$userid=$_SESSION["userid"];
@$fbid=$_SESSION["fbid"];

if(!$userid){


header("location:login.php");

}



?>

<!DOCTYPE html>
<html lang="en-gb" dir="ltr">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>ClubGo - Your Nightlife Conceirge</title>
        <link rel="shortcut icon" href="docs/images/clubgo-icon.png" type="image/x-icon">
        <link rel="apple-touch-icon-precomposed" href="docs/images/apple-touch-icon.png">
        <link id="data-uikit-theme" rel="stylesheet" href="docs/css/uikit.docs.min.css">
        <link rel="stylesheet" href="docs/css/docs.css">
        <link rel="stylesheet" href="docs/css/custom.css">
        <link rel="stylesheet" href="docs/css/makeweb.css">
        <link rel="stylesheet" href="docs/css/responsive.css">
        <link rel="stylesheet" href="docs/css/theme1.css">
        <link rel="stylesheet" href="vendor/highlight/highlight.css">
        <script src="vendor/jquery.js"></script>
        <script src="docs/js/uikit.min.js"></script>
        <script src="vendor/highlight/highlight.js"></script>
        <script src="docs/js/docs.js"></script>
        <script src="docs/js/slideshow.js"></script>
        <script src="docs/js/slideshow-fx.js"></script>
        <script src="docs/js/slideset.js"></script>
        <script src="docs/js/lightbox.js"></script>
        <script src="docs/js/sticky.js"></script>    
        </head>

    <body class="tm-background">

        <nav class="tm-navbar uk-navbar uk-navbar-attached" data-uk-sticky="{boundary:'#define-an-offset'}">
            <div class="uk-container uk-container-center">

                <div class="uk-animation-hover"><a class="uk-navbar-brand uk-hidden-small uk-animation-reverse uk-animation-scale" href="index.php"><img class="uk-margin uk-margin-remove" src="docs/images/clugo.png" width="120" height="40" title="Clubgo" alt="Clubgo"></a></div>

                <ul class="uk-navbar-nav uk-navbar-flip uk-hidden-small">
                  <li><a href="about.php">About</a></li>
                    <li><a href="events.php">Events</a></li>
                    <li><a href="venues.php">Venues</a></li>
<!--                     <li><a href="offer.php">Offers</a></li> -->
                    <li><a href="artist.php">Artists</a></li>
                    <li><a href="login.php" id="ulog">Login/Signup</a></li>
                    <li class="download"><a href="download.php">Download App</a></li>
                    <?php
if($fbid){
?>
<li><div class="uk-button-dropdown" data-uk-dropdown="{mode:'click'}" aria-haspopup="true" aria-expanded="false"><img src="https://graph.facebook.com/<?php echo  $fbid ?>/picture?type=small" width="40" height="40" class="fb-img"><div class="uk-dropdown uk-dropdown-bottom" aria-hidden="true" style="top: 30px; left: 0px;" tabindex="" >
                                        <ul class="uk-nav uk-nav-dropdown">
                                           <li><a href="logout.php">Logout</a></li>
                                           <li><a href="#">Booked Tickets</a></li> 
                                        </ul>
                                    </div>
                                </div></li>

<?php
}
if($userid and !$fbid){
?>
<li class="mail-img"><div class="uk-button-dropdown" data-uk-dropdown="{mode:'click'}" aria-haspopup="true" aria-expanded="false"><img src="docs/img/user-white.png" width="20" height="20"><div class="uk-dropdown uk-dropdown-bottom" aria-hidden="true" style="top: 30px; left: 0px;" tabindex="">
                                        <ul class="uk-nav uk-nav-dropdown" >
                                           <li id="uout" ><a href="logout.php">Logout</a></li>
                                           <li><a href="#">Booked Tickets</a></li> 
                                        </ul>
                                    </div>
                                </div></li>
<?php
}
  ?>
                </ul>

                <a href="#tm-offcanvas" class="uk-navbar-toggle uk-visible-small" data-uk-offcanvas></a>

                <div class="uk-navbar-brand uk-navbar-center uk-visible-small"><img src="docs/images/clugo.png" width="90" height="30" title="Clubgo" alt="Clubgo"></div>

            </div>
        </nav>

                <script type="text/javascript">


var us='<?php echo $userid ?>';

if(us){


$("#uout").show();
$("#ulog").hide();

}


        </script>
        

        <div class="tm-section-color-2 tm-section-colored top_a">
            <div class="uk-container uk-container-center uk-text-center top_a">

<!--
    <div class="uk-grid">
            <div class="uk-width-1-2 live1">
              <h2>Live Tickets</h2>
              <div class="uk-grid history-brdr">
             <div class="uk-width-1-2 history-1"><img src="https://www.clubgo.in/cgsquad.in/backend/images/event/small801800919.jpg" width="80%" height="auto"/></div>
             <div class="uk-width-1-2 history-2">
               <h2 class="history-detail" style="color:#000!important;">DJ Shaan Live</h2>
               <h4>Hype, Nehru Place</h4>
               <h4>11:00 pm onwords friday, 09 september</h4>
               <h4>Last Entry Time Before - 11:00 pm </h4>
               <h4>DEL-0709-123426435</h4>
               <div class="uk-grid">
                 <div class="uk-width-1-3">Couple 4</div>
                 <div class="uk-width-1-3">Male 1</div>
                 <div class="uk-width-1-3">Female 0</div>
               </div>
             </div>   
              </div>
        </div>
            <div class="uk-width-1-2 past1">Past Tickets</div>
    </div>
-->
            
              
<ul class="uk-subnav uk-subnav-pill uk-grid book-pill" data-uk-switcher="{connect:'#subnav-pill-content-1'}">
                                <li class="uk-width-1-2 uk-active live" aria-expanded="tue"><a href="#" class="live1">Live Tickets</a></li>
                                <li aria-expanded="false" class="uk-width-1-2 past"><a href="#" class="past1">Past Tickets</a></li>
                            </ul>
                            
                            <ul id="subnav-pill-content-1" class="uk-switcher history-pill">
                                <li class="uk-active" aria-hidden="false">
            
<?php

@$query=mysqli_query($conn,"SELECT * from booking b, event e,items i, ticket t, ticket_type tt where b.event_id=e.e_id and b.ticket_id=t.ticket_id  and t.ticket_type=tt.type_id and e.item_id=i.item_id  and  b.userid='$userid' order by b.user_booking_id desc");



while(@$fetch=mysqli_fetch_array($query))

{
@$currentdate=date("Y-m-d",time());


          @$datetim=date("Y-m-d",strtotime($fetch['book_posted']));


              $ti= $fetch["item_title"];
             $loc= $fetch["item_address"];
             $viewlocation=$ti.','.$loc;

           $beforeaftertime=$fetch['book_time'];
           $userbookdate=$fetch['book_date'];

           @$estartdate=explode(",",$fetch['e_date']);
           @$eenddate=explode(",",$fetch['e_enddate']);
           @$estarttime=explode(",",$fetch['e_start']);
           @$eendtime=explode(",",$fetch['e_end']);


    if($datetim >= $currentdate){

    
           
 

        ?>
        <div class="uk-grid grid-history">
                                  <div class="uk-width-medium-1-2 uk-width-small-1-1 history-resp">


                             <img src="https://www.clubgo.in/cgsquad.in/backend/<?php echo str_replace('small','thumb' ,$fetch['cover_image'])?>" width="100%" height="auto"/>
            </div>
                                  <div class="uk-width-medium-1-2 uk-width-small-1-1 history-resp1">


              <p class="book-price"><b>&#8377; <?php echo $fetch['booked_price'];?></b></p>
                                        <p class="history-detail"><b>Event</b> : <?php

                                         echo $fetch['e_name'];?></p>
                     <p class="history-detail1"><b>Venue</b> : <?php echo $viewlocation;?></p>
                     <p class="history-detail1"><b>D&T</b> :  <?php  for ($i=0; $i <count($estartdate) ; $i++) { 

                    if($estartdate[$i]>=$userbookdate || $eenddate <= $userbookdate){
                        
                        echo date('h:i a',strtotime($estarttime[$i])) .' onwards'.date(" l, dS F", strtotime($estartdate[$i])) ;

                        $endtime=$eendtime[$i];

                        @$ctime=strtotime($endtime); //convert time
                    @$lesstime=$ctime-(60*10); //10 min minus from event time 
                    @$format=date("h:i a", $lesstime);
                     $format='Before -'.ltrim($format, '0');
                        break;

                    }
                   }?></p>
                     <p class="history-detail1"><b>Last Entry Time</b> : <?php


      if($beforeaftertime=='0'){

            @$entrytime='Before -'.date('h:i a', strtotime($fetch['bf_time']));

            echo $entrytime;


           }else{

echo $format;



           } 


                      ?> </p>
                     <p class="history-detail1"><b>Ticket#</b> :<?php echo $fetch['user_booking_id'];?></p>
                     <div class="uk-grid">
                         <div class="uk-width-1-3 history-detail1"><b>Couple</b> <span class="book-count"><?php echo $fetch['couple_ticket'];?></span> </div>
                         <div class="uk-width-1-3 history-detail1"><b>Female</b> <span class="book-count"><?php echo $fetch['female_ticket'];?></span> </div>
                         <div class="uk-width-1-3 history-detail1"><b>Male</b> <span class="book-count"><?php echo $fetch['male_ticket'];?></span> </div>
                     </div>
                     </div>
                       </div>

<?php } }

?>


                      <!--               <p class="book-price"><b>&#8377; 5000</b></p>
                                    <p class="history-detail"><b>Event</b> : DJ Shaan Live</p>
               <p class="history-detail1"><b>Venue</b> : Hype, Nehru Place</p>
               <p class="history-detail1"><b>D&T</b> : 11:00 pm onwords friday, 09 september</p>
               <p class="history-detail1"><b>Last Entry Time</b> : Before - 11:00 pm </p>
               <p class="history-detail1"><b>Ticket#</b> : DEL-0709-123426435</p>
               <div class="uk-grid">
                 <div class="uk-width-1-3 history-detail1"><b>Couple</b> <span class="book-count">4</span> </div>
                 <div class="uk-width-1-3 history-detail1"><b>Female</b> <span class="book-count">0</span> </div>
                 <div class="uk-width-1-3 history-detail1"><b>Male</b> <span class="book-count">1</span> </div>
               </div> -->
                
                                  
                                 
                              
            
                                </li>
                                <li aria-hidden="true" class="">


<?php

@$query1=mysqli_query($conn,"SELECT * from booking b, event e,items i, ticket t, ticket_type tt where b.event_id=e.e_id and b.ticket_id=t.ticket_id  and t.ticket_type=tt.type_id and e.item_id=i.item_id  and  b.userid='$userid'");

while(@$fetch1=mysqli_fetch_array($query1))

{


@$currentdate1=date("Y-m-d",time());

          @$datetim1=date("Y-m-d",strtotime($fetch1['book_posted']));

          //echo $datetim1;

              $ti1= $fetch1["item_title"];
             $loc1= $fetch1["item_address"];
             $viewlocation1=$ti1.','.$loc1;

           //$beforeaftertime=$fetch['book_time'];
           $userbookdate1=$fetch1['book_date'];

           @$estartdate1=explode(",",$fetch1['e_date']);
           @$eenddate1=explode(",",$fetch1['e_enddate']);
           // @$estarttime=explode(",",$fetch['e_start']);
           // @$eendtime=explode(",",$fetch['e_end']);


    if($datetim1 < $currentdate1){

  

       

 

        ?>
        <div class="uk-grid grid-history">
                                  <div class="uk-width-medium-1-2 uk-width-small-1-1 history-resp">


                             <img src="https://www.clubgo.in/cgsquad.in/backend/<?php echo str_replace('small','thumb' ,$fetch1['cover_image'])?>" width="100%" height="auto"/>
            </div>
                                  <div class="uk-width-medium-1-2 uk-width-small-1-1 history-resp1">


              <p class="book-price"><b>&#8377; <?php echo $fetch1['booked_price'];?></b></p>
                                        <p class="history-detail"><b>Event</b> : <?php

                                         echo $fetch1['e_name'];?></p>
                     <p class="history-detail1"><b>Venue</b> : <?php echo $viewlocation1;?></p>
                     <p class="history-detail1"><b>D&T</b> :  <?php  
                        echo date(" l, dS F", strtotime($userbookdate1));

                    
                   ?></p>

                     <p class="history-detail1"><b>Ticket#</b> :<?php echo $fetch1['user_booking_id'];?></p>
                     <div class="uk-grid">
                         <div class="uk-width-1-3 history-detail1"><b>Couple</b> <span class="book-count"><?php echo $fetch1['couple_ticket'];?></span> </div>
                         <div class="uk-width-1-3 history-detail1"><b>Female</b> <span class="book-count"><?php echo $fetch1['female_ticket'];?></span> </div>
                         <div class="uk-width-1-3 history-detail1"><b>Male</b> <span class="book-count"><?php echo $fetch1['male_ticket'];?></span> </div>
                     </div>
                     </div>
                       </div>

<?php } }




 ?>


                                <!-- <div class="uk-grid grid-history">
                                  <div class="uk-width-medium-1-2 uk-width-small-1-1 history-resp">
                             <img src="https://www.clubgo.in/cgsquad.in/backend/images/event/small801800919.jpg" width="100%" height="auto"/>
            </div>
                                  <div class="uk-width-medium-1-2 uk-width-small-1-1 history-resp1">
                                    <p class="book-price"><b>&#8377; 5000</b></p>
                                    <p class="history-detail"><b>Event</b> : DJ Shaan Live</p>
               <p class="history-detail1"><b>Venue</b> : Hype, Nehru Place</p>
               <p class="history-detail1"><b>D&T</b> : 11:00 pm onwords friday, 09 september</p>
               <p class="history-detail1"><b>Last Entry Time</b> : Before - 11:00 pm </p>
               <p class="history-detail1"><b>Ticket#</b> : DEL-0709-123426435</p>
               <div class="uk-grid">
                 <div class="uk-width-1-3 history-detail1"><b>Couple</b> <span class="book-count">4</span> </div>
                 <div class="uk-width-1-3 history-detail1"><b>Female</b> <span class="book-count">0</span> </div>
                 <div class="uk-width-1-3 history-detail1"><b>Male</b> <span class="book-count">1</span> </div>
               </div>
                
                                  </div>
                                 
                                </div> -->

                                
                                </li>
                            </ul>               
               
             
            </div>
        </div>

       
   <div class="tm-footer">
            <div class="uk-container uk-container-center uk-text-center">

                <ul class="uk-subnav uk-subnav-line uk-flex-center">
                    <li><a href="https://www.facebook.com/ClubGoApp/" target="_blank">Facebook</a></li>
                    <li><a href="https://www.instagram.com/clubgoapp/" target="_blank">Instagram</a></li>
                    <li><a href="https://twitter.com/ClubGoApp" target="_blank">Twitter</a></li>
                </ul>

                <div class="uk-panel">
                    <p>Copyright ClubGo - All rights reserved.</p>
                    <a href="index.php"><img src="docs/images/clugo.png" width="90" height="30" title="ClubGo" alt="ClubGo"></a>
                </div>

            </div>
        </div>

<div id="tm-offcanvas" class="uk-offcanvas">

	  <div class="uk-offcanvas-bar">

		<ul class="uk-nav uk-nav-offcanvas uk-nav-parent-icon" data-uk-nav="{ multiple: true }">
		  <li><a href="index.php">Home</a></li>
	  <li><a href="about.php">About</a></li>
		  <li><a href="events.php"> Event </a></li>
		  <li><a href="venues.php"> Venues </a></li>
<!--           <li><a href="offer.php"> Offers </a></li> -->
		  <li><a href="artist.php"> Artists </a></li>
		  <li><a href="login.php" id="ulog1"> Login/Signup </a></li>


		 <li style="display:none" id="uout1"><a href="logout.php">Logout</a></li>
		  <li><a href="download.php"> Download App </a></li>

		</ul>

	  </div>

	</div>

	<script type="text/javascript">

var us1='<?php echo $userid ?>';

if(us1){


$("#uout1").show();
$("#ulog1").hide();

}


	</script>
        
        <script>

            $.ajax({
                dataType : "jsonp",
                url      : "https://api.github.com/repos/uikit/uikit?callback=ukghapi&nocache="+Math.random(),
                success  : function(data){

                    if(!data) return;

                    if(data.data.watchers){
                        $("[data-uikit-stargazers]").html(data.data.watchers);
                    }

                    if(data.data.forks){
                        $("[data-uikit-forks]").html(data.data.forks);
                    }
                }
            });
        </script>
    </body>
</html>
 <?php ob_end_flush();
 ?>