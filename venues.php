<?php
include("config.php");
error_reporting(0);
ob_start();
session_start();

@$user=$_SESSION["name"];
@$userid=$_SESSION["userid"];
@$fbid=$_SESSION["fbid"];


?>

<!DOCTYPE html>
<html lang="en-gb" dir="ltr">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="keywords" content="" />
	<meta name="description" content="" />
        <title>ClubGo - Your Nightlife Conceirge</title>
        <link rel="shortcut icon" href="docs/images/clubgo-icon.png" type="image/x-icon">
        <link rel="apple-touch-icon-precomposed" href="docs/images/apple-touch-icon.png">
        <link id="data-uikit-theme" rel="stylesheet" href="docs/css/uikit.docs.min.css">
        <link rel="stylesheet" href="docs/css/docs.css">
        <link rel="stylesheet" href="docs/css/custom.css">
        <link rel="stylesheet" href="docs/css/makeweb.css">
        <link rel="stylesheet" href="docs/css/responsive.css">
        <link rel="stylesheet" href="docs/css/theme1.css">
        <link rel="stylesheet" href="vendor/highlight/highlight.css">
        <script src="vendor/jquery.js"></script>
        <script src="docs/js/uikit.min.js"></script>
        <script src="vendor/highlight/highlight.js"></script>
        <script src="docs/js/docs.js"></script>
        <script src="docs/js/slideshow.js"></script>
        <script src="docs/js/slideshow-fx.js"></script>
        <script src="docs/js/slideset.js"></script>
        <script src="docs/js/sticky.js"></script>
        
</head>

    <body class="tm-background">

        <nav class="tm-navbar uk-navbar uk-navbar-attached" data-uk-sticky="{boundary:'#define-an-offset'}">
            <div class="uk-container uk-container-center">

                <div class="uk-animation-hover"><a class="uk-navbar-brand uk-hidden-small uk-animation-reverse uk-animation-scale" href="index.php"><img class="uk-margin uk-margin-remove" src="docs/images/clugo.png" width="120" height="40" title="Clubgo" alt="Clubgo"></a></div>

                <ul class="uk-navbar-nav uk-navbar-flip uk-hidden-small">
	               <li><a href="about.php">About</a></li>
                    <li><a href="events.php">Events</a></li>
                    <li><a href="#" class="acti">Venues</a></li>
<!--                     <li><a href="offer.php">Offers</a></li> -->
                    <li><a href="artist.php">Artists</a></li>
                     <li><a href="login.php" id="ulog">Login/Signup</a></li>
                    <li class="download"><a href="download.php">Download App</a></li>
                    <?php
if($fbid){
?>
<li><div class="uk-button-dropdown" data-uk-dropdown="{mode:'click'}" aria-haspopup="true" aria-expanded="false"><img src="https://graph.facebook.com/<?php echo  $fbid ?>/picture?type=small" width="40" height="40" class="fb-img"><div class="uk-dropdown uk-dropdown-bottom" aria-hidden="true" style="top: 30px; left: 0px;" tabindex="" >
                                        <ul class="uk-nav uk-nav-dropdown">
                                           <li><a href="logout.php">Logout</a></li> 
                                           <li><a href="history.php">Booked Tickets</a></li>
                                        </ul>
                                    </div>
                                </div></li>

<?php
}
if($userid and !$fbid){
?>
<li class="mail-img"><div class="uk-button-dropdown" data-uk-dropdown="{mode:'click'}" aria-haspopup="true" aria-expanded="false"><img src="docs/img/user-white.png" width="20" height="20"><div class="uk-dropdown uk-dropdown-bottom" aria-hidden="true" style="top: 30px; left: 0px;" tabindex="">
                                        <ul class="uk-nav uk-nav-dropdown" >
                                           <li id="uout" ><a href="logout.php">Logout</a></li> 
                                           <li><a href="history.php">Booked Tickets</a></li>
                                        </ul>
                                    </div>
                                </div></li>
<?php
}
  ?>
                </ul>

                <a href="#tm-offcanvas" class="uk-navbar-toggle uk-visible-small" data-uk-offcanvas></a>

                <div class="uk-navbar-brand uk-navbar-center uk-visible-small"><img src="docs/images/clugo.png" width="90" height="30" title="Clubgo" alt="Clubgo"></div>

            </div>
        </nav>
        
        <script type="text/javascript">


var us='<?php echo $userid ?>';

if(us){


$("#uout").show();
$("#ulog").hide();

}


        </script>

        <div class="tm-section-color-2 tm-section-colored top_a" style="display: none;">
            <div class="uk-container uk-container-center uk-text-center top_a">


               <div class="uk-slidenav-position slider" data-uk-slideshow="{autoplay:true}">
                                     
                   <ul class="uk-slideshow uk-overlay-active slider-size" data-uk-slideshow>


                                <?php

                        
                        @$slide1=mysqli_query($conn,"SELECT banner_img1 , banner_eventid  from banner where banner_type='Venue'");
                        @$bann=mysqli_fetch_array($slide1);

                       @$slname=$bann['banner_eventid'];

                       // echo $slname;
                         @$slna=explode(",", $slname); 

                       

                         @$slimg=$bann['banner_img1'];
                        
                        @$slnimg=explode(",", $slimg);
                        
                    


                        //@$a=count($fetch);
                       // echo $a;
                         for ($i=0; $i <count($slnimg); $i++) { 
                           
                                   @$query=mysqli_query($conn,"SELECT item_title ,item_address from items where item_id='$slna[$i]' ");
                                    @$fetch=mysqli_fetch_array($query); 
                         

                          ?>






                       <li class="ban">
                           <a href="venue_detail.php?id=<?php echo $slna[$i];?>">
                             <div>
<p class="maxban"><img src="https://www.clubgo.in/cgsquad.in/backend/<?php echo $slnimg[$i]?>" class="bansize" alt="image1"></p>


<div class="uk-overlay-panel uk-overlay-background uk-overlay-fade  uk-flex-center uk-flex-middle uk-text-center bannertextbox dead_txt" style="opacity:1;">
                               <div class="dead_top">
                                   <h2 style="padding-left:25px;margin: 0px;" class="h2-dead"><?php

                                  
                                   echo $fetch['item_title'];


                                   ?></h2>
                                     <h3 style="padding-left:25px;margin: 0px;" class="h3-dead">
<?php  


 echo $fetch['item_address'];


?>
                                  
                                    



                                   </h3> 
                                   <!--<h4 style="line-height:0px;margin-top:0px;"><span><img src="http://neith.googlecode.com/hg/externals/icon-set/metrostation_by_yankoa-d312tty/PNG/Communications/White/MB_0021_clock2.png" class="clockimg"></span>&nbsp; 9 pm onwards, Tuesday, 15th march</h4>
                                    --><!-- <button class="uk-button uk-button-primary">Button</button> -->
                               </div>
                           </div>
                             </div>
                           </a>
                       </li>


                           <?php }?>                      <!--  <li>
                                                  <img src="http://scout-site.com/taste-of-india/wp-content/uploads/sites/1744/2015/12/Taste-of-India-Cover-Photo.jpg" width="800" height="400" alt="image1">
                                                  <div class="uk-overlay-panel uk-overlay-background uk-overlay-fade  uk-flex-center uk-flex-middle uk-text-center bannertextbox dead_txt">
                                                      <div class="dead_top">
                                                          <h2 style="line-height:10px;padding-left:25px">Kitty Su</h2>
                                                          <h3 style="line-height:0px;"><span><img src="http://fieldofgreenspc.com/assets/img/location.png" class="locimg"></span> Cannaught Place</h3>
                                                          <h4 style="line-height:0px;margin-top:0px;"><span><img src="http://neith.googlecode.com/hg/externals/icon-set/metrostation_by_yankoa-d312tty/PNG/Communications/White/MB_0021_clock2.png" class="clockimg"></span>&nbsp; 9 pm onwards, Tuesday, 15th march</h4> -->
                                                          <!-- <button class="uk-button uk-button-primary">Button</button> -->
                                             <!--          </div>
                                                  </div>
                                              </li> -->
                   </ul>
                   
                  <a href="#" class="uk-slidenav uk-slidenav-contrast uk-slidenav-previous pre_icon" data-uk-slideshow-item="previous" style="color: rgba(255,255,255,0.4)"></a>
                   <a href="#" class="uk-slidenav uk-slidenav-contrast uk-slidenav-next next_icon" data-uk-slideshow-item="next" style="color: rgba(255,255,255,0.4)"></a> 
                   
                   <ul class="uk-dotnav uk-dotnav-contrast uk-position-bottom uk-flex-center nextbullate">

                   <?php 

                    for ($i=0; $i <count($slnimg); $i++) {?>

                  <li data-uk-slideshow-item="<?php echo $i ?>" class=""><a href="#">Item 1</a></li>

                    <?php }?>

                       <!-- <li data-uk-slideshow-item="0" class=""><a href="#">Item 1</a></li>
                       <li data-uk-slideshow-item="1" class=""><a href="#">Item 2</a></li>
                        <li data-uk-slideshow-item="2" class=""><a href="#">Item 3</a></li>
                       <li data-uk-slideshow-item="3" class=""><a href="#">Item 4</a></li>
                       <li data-uk-slideshow-item="4" class=""><a href="#">Item 5</a></li> -->
                   </ul>
                   
                   
               </div>
               
               
             
            </div>
        </div>

       

<div class="tm-section tm-section-color-white bgico">
    <div class=" uk-container-center uk-text-center">

        <h1 class="tm-margin-large-bottom"><strong>VENUES</strong></h1>

<div class="vengrid">

       <div class="uk-grid">


                                                 <?php

                          @$sel3=mysqli_query($conn,"SELECT * from items i,location loc where  i.item_location=loc.loc_id and i.item_published=1 group by i.item_id order by i.item_prior");
                          while(@$fet3=mysqli_fetch_array($sel3)){

                                @$cost=explode(",",$fet3['cost']);

                           // @$ta= array_shift(array_slice($tag, 0, 1)); 
                                
                            ?>

 <div class="uk-width-small-1-1 uk-width-medium-1-2 uk-width-large-1-3" style="padding-top:30px;">
               <div class="uk-panel uk-overlay-hover uk-text-center uk-scrollspy-init-inview uk-scrollspy-inview uk-animation-fade">
<!--        <div class="uk-panel-badge uk-badge badgy_left"></div> -->


           <figure class="uk-overlay tm-overlay-uniq ">
       
               <img src="https://www.clubgo.in/cgsquad.in/backend/<?php echo $fet3['cover_img']?>" class="uk-overlay-scale brdr_red" alt="Wooden Chair" style="min-width:400px;height:270px;">
               <div class="uk-overlay-panel uk-overlay-bottom">
                   <div>                                                     
     <h3 class="uk-h2 uk-margin-bottom-remove tm-overlay-panel-title txt_anim_a"><?php echo $fet3['item_title']?></h3>

      <div class="tm-panel-caption txt_anim_b pad_card">
      
      
                                           <div class="uk-grid" style="padding-left: 20px;">
                                           <div class="uk-width-1-2"style="width:20%;text-align: center;"><img src="docs/img/locb.png" style="width:18px;"></div>
                                           <div class="uk-width-1-2" style="width: 70%;text-align: left;"><?php echo $fet3['loc_title']?></div>
                                           </div>
                                           <?php 
                                             @$kfor=strlen($fet3['known_for']);
                                                 if(!$kfor==0){?>

                                                 <div class="uk-grid" style="margin-top:10px;padding-left: 20px;">
                                           <div class="uk-width-1-2"style="width:20%;text-align: center;"><img src="docs/img/couple.png" style="width:22px;"></div>
                                           <div class="uk-width-1-2" style="width: 80%;text-align: left;"><?php echo $fet3['known_for'] ?></div>
                                           </div>


                                                <?php } 

                                                @$cuis=strlen($fet3['cuisines']);

                                                if(!$cuis==0){?>

                                                <div class="uk-grid" style="margin-top:10px;padding-left: 20px;">
                                           <div class="uk-width-1-2"style="width:20%;text-align: center;"><img src="docs/img/cuisi.png" style="width:22px;"></div>
                                           <div class="uk-width-1-2" style="width: 80%;text-align: left;"><?php echo $fet3['cuisines'] ?></div>
                                           </div>
                                 

                                                <?php } 

                                                @$co=strlen($cost[0]);

                                                if(!$co==0){?>

                                                     <div class="uk-grid" style="margin-top:10px;padding-left: 20px;">
                                           <div class="uk-width-1-2"style="width:20%;text-align: center;"><img src="docs/img/rupeeb.png" style="width:13px;"></div>
                                           <div class="uk-width-1-2" style="width: 80%;text-align: left;"><?php echo $cost[0] ?></div>
                                           </div>

                                                <?php }
                                            
                                             ?>
                                           
                                           
                                           
                                           
                                            
                                           
                                           <!-- <p style="font-size:16px;"><?php echo $fet3['known_for'] ?></p>
                                           <p style="font-size:16px;"><?php echo $fet3['cuisines'] ?></p>
                                           <p style="font-size:16px;"><img src="docs/img/rupeeb.png" style="width:10px;">  <?php echo $fet3['cost'] ?></p> -->
                                           
                                           
                                           
                                            <!--<div class="" style="float:left;"><?php echo $fet3['e_evetag']?></div>--> 
                                       </div>

                    </div>
               </div>



                                  <a href="venue_detail.php?id=<?php echo $fet3['item_id']?>"><div class="uk-position-cover tm-panel-link icon-plus"  data-index="0" data-uk-modal=""></div></a>
       
           </figure>
       
       </div></div>

                         <?php }



                           ?>






           
       
       
                                      
       
                                      
       
                 
       
            
       
       
       
     <!--   <div class="uk-width-1-3"><div class="uk-panel uk-overlay-hover uk-text-center uk-scrollspy-init-inview uk-scrollspy-inview uk-animation-fade">
       
       <div class="uk-panel-badge uk-badge badgy_left">Trending</div>
           <figure class="uk-overlay tm-overlay-uniq ">
       
               <img src="http://52.74.78.106/blog/wp-content/uploads/2015/07/Third-Eye-nightclub-.jpg" class="uk-overlay-scale brdr_red" alt="Wooden Chair" width="1400" height="1070" style="">
               <div class="uk-overlay-panel uk-overlay-bottom">
                   <div>
       
                                       <h3 class="uk-h2 uk-margin-bottom-remove tm-overlay-panel-title txt_anim_a">Kittu Su</h3>
       
                                       <div class="tm-panel-caption txt_anim_b">
                                           <h4 class="center pad_card">Donut with Kohli Akash</h4>
                                           <h5 class="center pad_card">Freeze Lounge, Gurgaon</h5><br>
                                           <div class="" style="float:left;">An oasis of Good Eating, Drinking and Friends.</div>
                                       </div>
       
                   </div>
               </div>
       
                                                           <a href="#"><div class="uk-position-cover tm-panel-link icon-plus"  data-index="0" data-uk-modal=""></div></a>
       
           </figure>
       
       </div></div>
       
       <div class="uk-width-1-3"><div class="uk-panel uk-overlay-hover uk-text-center uk-scrollspy-init-inview uk-scrollspy-inview uk-animation-fade">
       <div class="uk-panel-badge uk-badge badgy_left">Trending</div>
       
           <figure class="uk-overlay tm-overlay-uniq ">
       
               <img src="http://images.locanto.in/1096382497/Bali-Nightlife-Tour-Packages_1.jpg" class="uk-overlay-scale brdr_red" alt="Wooden Chair" width="1400" height="1070" style="">
               <div class="uk-overlay-panel uk-overlay-bottom">
                   <div>
       
                                       <h3 class="uk-h2 uk-margin-bottom-remove tm-overlay-panel-title txt_anim_a">Kittu Su</h3>
       
                                       <div class="tm-panel-caption txt_anim_b">
                                           <h4 class="center pad_card">Donut with Kohli Akash</h4>
                                           <h5 class="center pad_card">Freeze Lounge, Gurgaon</h5><br>
                                           <div class="" style="float:left;">An oasis of Good Eating, Drinking and Friends.</div>
                                       </div>
       
                   </div>
               </div>
       
                                                           <a href="#"><div class="uk-position-cover tm-panel-link icon-plus"  data-index="0" data-uk-modal=""></div></a>
       
           </figure>
       
       </div></div> -->
       
       </div>
       
       
    <!--    <div class="uk-grid">
           <div class="uk-width-1-3">
               <div class="uk-panel uk-overlay-hover uk-text-center uk-scrollspy-init-inview uk-scrollspy-inview uk-animation-fade">
       <div class="uk-panel-badge uk-badge badgy_left">Trending</div>
       
           <figure class="uk-overlay tm-overlay-uniq ">
       
               <img src="http://timesofindia.indiatimes.com/photo/49830598.cms" class="uk-overlay-scale brdr_red" alt="Wooden Chair" width="1400" height="1070" style="">
               <div class="uk-overlay-panel uk-overlay-bottom">
                   <div>
       
                                       <h3 class="uk-h2 uk-margin-bottom-remove tm-overlay-panel-title txt_anim_a">Kittu Su</h3>
       
                                       <div class="tm-panel-caption txt_anim_b">
                                           <h4 class="center pad_card">Donut with Kohli Akash</h4>
                                           <h5 class="center pad_card">Freeze Lounge, Gurgaon</h5><br>
                                           <div class="" style="float:left;">An oasis of Good Eating, Drinking and Friends.</div>
                                       </div>
       
                   </div>
               </div>
       
                                                           <a href="#"><div class="uk-position-cover tm-panel-link icon-plus"  data-index="0" data-uk-modal=""></div></a>
       
           </figure>
       
       </div></div>
       
       
       
       <div class="uk-width-1-3"><div class="uk-panel uk-overlay-hover uk-text-center uk-scrollspy-init-inview uk-scrollspy-inview uk-animation-fade">
       
       <div class="uk-panel-badge uk-badge badgy_left">Trending</div>
           <figure class="uk-overlay tm-overlay-uniq ">
       
               <img src="http://forbesindia.com/blog/wp-content/uploads/2013/08/panaea.jpg" class="uk-overlay-scale brdr_red" alt="Wooden Chair" width="1400" height="1070" style="">
               <div class="uk-overlay-panel uk-overlay-bottom">
                   <div>
       
                                       <h3 class="uk-h2 uk-margin-bottom-remove tm-overlay-panel-title txt_anim_a">Kittu Su</h3>
       
                                       <div class="tm-panel-caption txt_anim_b">
                                           <h4 class="center pad_card">Donut with Kohli Akash</h4>
                                           <h5 class="center pad_card">Freeze Lounge, Gurgaon</h5><br>
                                           <div class="" style="float:left;">An oasis of Good Eating, Drinking and Friends.</div>
                                       </div>
       
                   </div>
               </div>
       
                                                           <a href="#"><div class="uk-position-cover tm-panel-link icon-plus"  data-index="0" data-uk-modal=""></div></a>
       
           </figure>
       
       </div></div>
       
       <div class="uk-width-1-3"><div class="uk-panel uk-overlay-hover uk-text-center uk-scrollspy-init-inview uk-scrollspy-inview uk-animation-fade">
       <div class="uk-panel-badge uk-badge badgy_left">Trending</div>
       
           <figure class="uk-overlay tm-overlay-uniq ">
       
               <img src="http://timesofindia.indiatimes.com/photo/50297508.cms" class="uk-overlay-scale brdr_red" alt="Wooden Chair" width="1400" height="1070" style="">
               <div class="uk-overlay-panel uk-overlay-bottom">
                   <div>
       
                                       <h3 class="uk-h2 uk-margin-bottom-remove tm-overlay-panel-title txt_anim_a">Kittu Su</h3>
       
                                       <div class="tm-panel-caption txt_anim_b">
                                           <h4 class="center pad_card">Donut with Kohli Akash</h4>
                                           <h5 class="center pad_card">Freeze Lounge, Gurgaon</h5><br>
                                           <div class="" style="float:left;">An oasis of Good Eating, Drinking and Friends.</div>
                                       </div>
       
                   </div>
               </div>
       
                                                          <a href="#"><div class="uk-position-cover tm-panel-link icon-plus"  data-index="0" data-uk-modal=""></div></a>
       
           </figure>
       
       </div></div>
       
       </div> -->
</div>

    </div>
</div>


        
        
        
          
        

        

        <div class="tm-footer">
            <div class="uk-container uk-container-center uk-text-center">

                <ul class="uk-subnav uk-subnav-line uk-flex-center">
                    <li><a href="https://www.facebook.com/ClubGoApp/" target="_blank">Facebook</a></li>
                    <li><a href="https://www.instagram.com/clubgoapp/" target="_blank">Instagram</a></li>
                    <li><a href="https://twitter.com/ClubGoApp" target="_blank">Twitter</a></li>
                </ul>

                <div class="uk-panel">
                    <p>Copyright ClubGo - All rights reserved.</p>
                    <a href="index.php"><img src="docs/images/clugo.png" width="90" height="30" title="Clubgo" alt="Clubgo"></a>
                </div>

            </div>
        </div>


<div id="tm-offcanvas" class="uk-offcanvas">

	  <div class="uk-offcanvas-bar">

		<ul class="uk-nav uk-nav-offcanvas uk-nav-parent-icon" data-uk-nav="{ multiple: true }">
		  <li><a href="index.php">Home</a></li>
	  <li><a href="about.php">About</a></li>
		  <li><a href="events.php"> Event </a></li>
		  <li><a href="venues.php"> Venues </a></li>
<!--           <li><a href="offer.php"> Offers </a></li> -->
		  <li><a href="artist.php"> Artists </a></li>
		  <li><a href="login.php" id="ulog1"> Login/Signup </a></li>


		 <li style="display:none" id="uout1"><a href="logout.php">Logout</a></li>
		  <li><a href="download.php"> Download App </a></li>

		</ul>

	  </div>

	</div>

	<script type="text/javascript">

var us1='<?php echo $userid ?>';

if(us1){


$("#uout1").show();
$("#ulog1").hide();

}


	</script>
        
        <script>

            $.ajax({
                dataType : "jsonp",
                url      : "https://api.github.com/repos/uikit/uikit?callback=ukghapi&nocache="+Math.random(),
                success  : function(data){

                    if(!data) return;

                    if(data.data.watchers){
                        $("[data-uikit-stargazers]").html(data.data.watchers);
                    }

                    if(data.data.forks){
                        $("[data-uikit-forks]").html(data.data.forks);
                    }
                }
            });
        </script>
    </body>
</html>
<?php ob_end_flush();
 ?>